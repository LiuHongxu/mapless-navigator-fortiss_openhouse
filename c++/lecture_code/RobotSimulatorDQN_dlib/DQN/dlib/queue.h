#ifndef QUEUE_H
#define QUEUE_H

#include <deque>
#include <cstddef>

/* Important resize before using add() */
template<typename T>
class Queue : public std::deque<T>
{
public:
    Queue(size_t size = 0) {
        this->resize(size);
    }
    /* filo buffer behaviour */
    void add(const T& val) {
        this->push_front(val);
        this->pop_back();
    }
};

#endif // QUEUE_H
