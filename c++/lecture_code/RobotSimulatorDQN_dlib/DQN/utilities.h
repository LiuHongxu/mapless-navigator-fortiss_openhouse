#ifndef UTILITIES_H
#define UTILITIES_H

#include "replay.h"
#include "dqn.h"
#include <iostream>

/*
 * Function to store qlearning to binary file
 * overload << , >> operator for class qlearn
 */
namespace dqn {

    /* write std vector to stream */
    template<typename T>
    inline std::ostream& operator<< (std::ostream& out, const std::vector<T>& vec)
    {
        size_t size = vec.size();

        out.write((char*)(&size), sizeof(size_t));
        out.write((char*)vec.data(), size * sizeof(T));
        return out;
    }
    /* read std vector form stream */
    template<typename T>
    std::istream& operator>> (std::istream& in, std::vector<T>& vec)
    {
        size_t size = 0;
        in.read((char*)(&size), sizeof(size_t));
        vec.resize(size);
        in.read((char*)vec.data(), size*sizeof(T));
        return in;
    }

    /* write MiniDNN::Network weights to stream */
    inline std::ostream& operator<< (std::ostream &out, const Replay& replay)
    {
        size_t buffer_size=0, state_size=0, filled=0, cur_index=0;
        const Memory& memory = replay.get_memory();
        buffer_size = memory.get_buffer_size();
        state_size = memory.get_state_size();
        filled = replay.is_filled();
        cur_index = replay.get_current_index();

        out.write((char*)(&buffer_size), sizeof(size_t));
        out.write((char*)(&state_size), sizeof(size_t));
        out.write((char*)(&filled), sizeof(size_t));
        out.write((char*)(&cur_index), sizeof(size_t));

        dlib::serialize(memory.states, out);

        out << memory.actions;
        out << memory.rewards;
        out << memory.td_errors;
        out << memory.isfinished;

        dlib::serialize(memory.next_states, out);

        return out;
    }

    /* read MiniDNN::Network weights form stream */
    inline std::istream& operator>> (std::istream &in, Replay& replay)
    {
        size_t buffer_size=0, state_size=0, filled=0, cur_index=0;
        Memory memory;

        in.read((char*)(&buffer_size), sizeof(size_t));
        in.read((char*)(&state_size), sizeof(size_t));
        in.read((char*)(&filled), sizeof(size_t));
        in.read((char*)(&cur_index), sizeof(size_t));
        memory.resize(buffer_size, state_size);

        dlib::deserialize(memory.states, in);

        in >> memory.actions;
        in >> memory.rewards;
        in >> memory.td_errors;
        in >> memory.isfinished;

        dlib::deserialize(memory.next_states, in);

        replay.initialize(memory, cur_index, filled);
        return in;
    }

    inline std::ostream& operator<< (std::ostream &out, const DQN& dqn)
    {
        dlib::serialize( dqn.get_net(), out);
        return out;
    }
    inline std::istream& operator>> (std::istream &in, DQN& dqn)
    {
        net_type net;
        dlib::deserialize( net, in);
        dqn.set_net(net);
        return in;
    }

    inline std::ostream& operator<< (std::ostream &out, const DDQN& ddqn)
    {
        dlib::serialize( ddqn.get_net(), out);
        dlib::serialize( ddqn.get_target_net(), out);
        return out;
    }
    inline std::istream& operator>> (std::istream &in, DDQN& ddqn)
    {
        net_type target_net, net;
        dlib::deserialize( net, in);
        dlib::deserialize( target_net, in);
        ddqn.set_net(net);
        ddqn.set_target_net(target_net);
        return in;
    }
}

#endif // UTILITIES_H
