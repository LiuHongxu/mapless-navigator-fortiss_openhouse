#ifndef DQNENVIRONMENT_H
#define DQNENVIRONMENT_H

#include "Simulator/agentenvironment.h"
#include "dlib/queue.h"

/*
 * Decribes the Environment as perceived form the perspective of a individual agent
 */
class DQNEnvironment : public AgentEnvironment
{
public:
    DQNEnvironment(Environment *env, ag::Agent* agent, const b2Vec2& goal_position);
    virtual ~DQNEnvironment() { }

    /* set a new goal position */
    void set_goal_position(const b2Vec2& goal_position );
    /* perform one step (apply action) */
    void execute(action_type action);
    /* returns the new observations */
    bool observe(EnvState& env_state);
    /* do random reset, keep area around goal position free */
    void reset(bool random = false, b2AABB* clear_area = nullptr);

private:
    // robot state settings
    b2Vec2 goal_position;                   // goal position
    b2AABB clear_goal_area;                 // free area around gool position
    float min_goal_dist;                    // min distance to goal     (stop simulation)
    float min_obstical_dist;                // min distance to obsticle (stop simulation)
    int range_dim;                          // number of rays for state
    float max_range;                        // maximum range of laser range finder

    // robot state
    action_type prev_action;
    float distance, prev_distance;          // robot world position
    float theta;                            // robto world angle
    std::vector<float> normalized_ranges;   // trimmed down ranges

    // prev values
    size_t hist_len;
    Queue<float> hist_distance;
    Queue<float> hist_theta;
    Queue<float> hist_action;

    // setup variables
    void setup();
    /* callback modelstates: compute distance + angle to goal */
    bool modelStatesCallback(const ag::ModelStates& modelstate, float min_range);
    /* callback lidar data: discretize ranges */
    bool scanCallback(const ag::SensorReading& ranges, float min_range);

    /* signed rotation angle form src to dst */
    static float rotation_angle(float src, float dst);
    /* compute a potential reward to objects */
    static float potential_distance( std::vector<float> d, float d_min, float tau );
    /* add new element to queue, remove the last one */
};

#endif // DQNENVIRONMENT_H
