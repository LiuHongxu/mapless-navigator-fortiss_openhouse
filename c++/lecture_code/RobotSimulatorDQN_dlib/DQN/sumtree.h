#ifndef SUMTREE_H
#define SUMTREE_H

#include <vector>
#include <random>

class SumTree
{
public:
    SumTree(int capacity);

    void set(int dataIndex, float priority);

    int retrieve(float value, int validLen);

    void sample(int batchSize, std::vector<int>& indexes, int validLen, float beta, std::vector<float> &weights);

private:
    std::vector<float> data;
    int n;

    std::random_device rd;      //  (seed) engine
    std::mt19937 rng;           // random-number engine
    std::uniform_real_distribution<double> real_uni;
};

#endif // SUMTREE_H
