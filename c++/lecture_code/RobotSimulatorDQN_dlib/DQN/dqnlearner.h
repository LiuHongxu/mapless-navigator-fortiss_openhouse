#ifndef DQNLEARNER_H
#define DQNLEARNER_H

#include "Simulator/agentenvironment.h"
#include "Simulator/environment.h"
#include <boost/filesystem.hpp>
#include <iostream>

class DQNLearner
{
public:
    enum Mode { TRAIN, EXEC };

    DQNLearner(Environment* env, const std::string& log_dir = "", const std::string& name = "", size_t cnt_save_steps = 2000)
        : env(env), cnt_save_steps(cnt_save_steps), cnt_episodes(0), log_dir_path(log_dir + name), mode(TRAIN)
    {
    }

    virtual ~DQNLearner()
    {
    }

    void set_mode(Mode mode) {
        this->mode = mode;
    }

    /* initalize everything */
    void setup() {
        // 1. create new directory
        boost::filesystem::path dir(log_dir_path);
        if(!(boost::filesystem::exists(log_dir_path))) {
            if (!boost::filesystem::create_directory(dir)) {
                std::cout << "error creating dir" << std::endl;
            }
        }

        // 2. setup the world
        setup_environment(env);
    }

    /* start training session */
    void start(size_t total_episodes) {
        if(!is_started) {
            this->total_episodes = total_episodes;

            // call startup method
            on_start(total_episodes);

            // call load method
            load(log_dir_path);

            is_started = true;
        }
    }

    /* update loop call this function as fast as possible */
    int update() {
        if(!is_started )
            return -1;  // not started

        // training
        if(mode == TRAIN) {
            if(cnt_episodes) {
                if(cnt_episodes % cnt_save_steps == 0 ) {
                    save(log_dir_path, false, cnt_episodes);
                }
            }
            if(cnt_episodes > total_episodes) {
                save(log_dir_path, true);
                is_started = false;
                return 1;   // finished
            }
            train(cnt_episodes);
        }
        // execution
        else {
            execute();
        }
        return 0;
    }

    /* save all data */
    int save_all() {
        save(log_dir_path, true);
    }

protected:
    /* add objects to envrionment */
    virtual void setup_environment(Environment* env) = 0;

    /* gets called after setup is finished */
    virtual void on_start(size_t total_episodes) = 0;

    /* run neuronal net training here */
    virtual void train(size_t& cnt_episodes) = 0;

    /* execture learned policy here */
    virtual void execute() = 0;

    /* save all data */
    virtual int save(const std::string& dirpath, bool all, size_t cnt_episodes = 0) = 0;

    /* load all data */
    virtual int load(const std::string& dirpath) = 0;

private:
    Environment* env;                           // environments of world

    bool is_started;
    size_t total_episodes;
    size_t cnt_save_steps;
    size_t cnt_episodes;

    std::string log_dir_path;
    Mode mode;
};

#endif // DQNLEARNER_H
