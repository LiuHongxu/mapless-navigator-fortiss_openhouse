#include "sumtree.h"
#include <assert.h>

SumTree::SumTree(int capacity)
{
    data.resize(2*capacity-1, 0.0);
    n = capacity;

    rng = std::mt19937(rd());
    real_uni = std::uniform_real_distribution<double>(0, 1);
}

void SumTree::set(int dataIndex, float value)
{
    int index = dataIndex + n-1;
    data.at(index)=value;
    while(true)
    {
        int parent=(index-1)/2;
        data.at(parent)=data.at(2*parent+1)+data.at(2*parent+2);
        if(parent == 0) {
            break;
        }
        index = parent;
    }
}

int SumTree::retrieve(float value_, int validLen)
{
    float value = value_;
    assert(value<data.at(0));
    int index=0;
    while(true)
    {
        if(value > data.at(index))
        {
            assert(false);
            value = data.at(index);
        }

        if(index>=n-1)
        {
            break;
        }
        int left=2*index+1;
        double leftValue=data.at(left);
        if(value<=leftValue)
        {
            index=left;
        }
        else
        {
            value-=leftValue;
            index=left+1;
        }
    }
    int dataIndex=index-n+1;
    assert(dataIndex < validLen);
    assert(dataIndex < n);
    return dataIndex;
}

void SumTree::sample(int batchSize, std::vector<int>& dataIndexes, int validLen, float beta, std::vector<float>& weights)
{
    double total=data.at(0);
    double d = total/batchSize;
    weights.clear();
    float maxW = 0;
    for(int i=0; i<batchSize; i++)
    {
        int dataIndex=retrieve( real_uni(rng) * d, validLen);
        float dataVal=data.at(dataIndex+n-1);
        float tw=pow(dataVal, -beta);
        weights.push_back(tw);
        maxW=std::max(maxW, tw);
        dataIndexes.push_back(dataIndex);
    }

    for(int i=0; i<weights.size(); i++) {
        weights.at(i) /= maxW;
    }
}
