#include "dqn.h"
#include <fstream>
#include "utilities.h"

#include <iostream>

using namespace dqn;
using namespace dlib;

DQN::DQN(Replay &replay, size_t state_size, size_t action_size, float epsilon, float alpha, float gamma )
    : trainer(net, adam(0.0005f, 0.9f, 0.999f)), replay(replay), state_size(state_size), action_size(action_size),
      epsilon(epsilon), alpha(alpha), gamma(gamma)
{
    rng = std::mt19937(rd());
    int_uni = std::uniform_int_distribution<int>(0, action_size-1);
    real_uni = std::uniform_real_distribution<double>(0, 1);

    trainer.be_verbose();
    trainer.set_learning_rate_shrink_factor(1.0);
    trainer.set_learning_rate( alpha );
    trainer.set_mini_batch_size( replay.get_batch_size() );
    std::cout << net << std::endl;
}

DQN::~DQN()
{
}

float DQN::learn(const std::vector<scalar_t> &state, const action_t& action, const scalar_t& reward, const std::vector<scalar_t> &next_state, bool isfinished)
{
    Matrix state_ = mat(state);
    Matrix next_state_ = mat(next_state);

    // remember transition
    replay.push_back(state_, action, reward, next_state_, 0.0f, isfinished);
    if(replay.is_filled())
        return memory_replay(); // learning !!!
    return 0.0f;
}

action_t DQN::policy(const std::vector<scalar_t>& state)
{
    Matrix state_ = mat(state);
    Matrix qValues = net(state_);
    return index_of_max( qValues );
}

scalar_t DQN::getQ(const std::vector<scalar_t>& state, int action)
{
    Matrix state_ = mat(state);
    Matrix qValues = net(state_);
    return qValues(action);
}

std::vector<scalar_t> DQN::getQs(const std::vector<scalar_t> &state)
{
    Matrix state_ = mat(state);
    Matrix qValues = net(state_);
    return std::vector<scalar_t>(qValues.begin(), qValues.end());
}

float DQN::memory_replay()
{
    // 1. get samples form history buffer
    const Memory& samples = replay.sample();

    // 2. evaluate neural net at all states / next states
    auto qValues = net( samples.states );                     // qValues of state
    auto next_qValues = net( samples.next_states );            // qValues of next state

    // 3. compute target values
    auto targets = qtarget(samples.rewards, qValues, next_qValues, samples.actions, samples.isfinished, gamma);

    // 4. update network
    trainer.train_one_step(samples.states, targets);

    return (float)trainer.get_average_loss();
}

Batch DQN::qtarget(const std::vector<float> &rewards, const Batch &qValues, const Batch &next_qValues, const std::vector<action_t> &actions, const std::vector<uint8_t>& isfinished, float gamma)
{
    Batch targets = qValues;
    float max_value;

    // Q(s,a) = r + gamma * argmax_a Q(s', a)
    for(unsigned int i = 0; i < rewards.size(); ++i ) {
        if(!isfinished[i]) {
            max_value = max( next_qValues[i] );
            targets[i](actions[i]) = rewards[i] + gamma * max_value;
        }
        else
            targets[i](actions[i]) = rewards[i];
    }
    return targets;
}

action_t DQN::generate_action(const std::vector<scalar_t> &state)
{
    action_t action;

    // choose eps greedy action
    if( real_uni(rng) > epsilon ) {
        Matrix state_ = mat(state);
        Matrix qValues = net( state_ );
        action = index_of_max( qValues );
    } else {
        // select complety random
        action = int_uni(rng);
    }
    return action;
}

DDQN::DDQN(Replay& replay, size_t state_size, size_t action_size, int swap_cnt, float epsilon, float alpha, float gamma )
    : DQN(replay, state_size, action_size, epsilon, alpha, gamma ), swap_cnt(swap_cnt), cnt(0)
{
    target_net.subnet() = net.subnet();
}

DDQN::~DDQN()
{
}

float DDQN::memory_replay()
{
    // 1. get samples form history buffer
    const Memory& samples = replay.sample();

    // Alternate between network and target network
    if(cnt == swap_cnt) {
        target_net.subnet() = net.subnet();
        cnt = 0;
    }    
    cnt++;

    // 2. evaluate neural net at all states / next states
    auto qValues = net( samples.states );                       // qValues of state
    auto next_qValues = net( samples.next_states );             // qValues of next state
    auto next_qValues_t = target_net( samples.next_states );    // qValues of next state based on target network

    // 3. compute target values
    auto targets = qtarget(samples.rewards, qValues, next_qValues, next_qValues_t, samples.actions, samples.isfinished, gamma);

    // 4. update network
    trainer.train_one_step(samples.states, targets); // seed rng number
    return (float)trainer.get_average_loss();
}

Batch DDQN::qtarget(const std::vector<float> &rewards, const Batch &qValues, const Batch &next_qValues, const Batch &next_qValues_t, const std::vector<action_t> &actions, const std::vector<uint8_t>& isfinished, float gamma)
{
    Batch targets = qValues;
    int max_inx;

    // Q(s,a) = r + gamma * argmax_a Q(s', a)
    for(unsigned int i = 0; i < rewards.size(); ++i ) {
        if(!isfinished[i]) {
            max_inx = index_of_max( next_qValues[i] );
            targets[i](actions[i]) = rewards[i] + gamma * next_qValues_t[i](max_inx);
        }
        else
            targets[i](actions[i]) = rewards[i];
    }
    return targets;
}
