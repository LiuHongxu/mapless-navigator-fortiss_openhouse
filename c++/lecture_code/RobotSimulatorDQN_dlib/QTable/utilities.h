#ifndef UTILITIES_CPP
#define UTILITIES_CPP

#include "qlearn.h"

/*
 * Function to store qlearning to binary file
 * overload << , >> operator for class qlearn
 */
namespace ql {

    /* write std vector to stream */
    template<typename T>
    inline std::ostream& operator<< (std::ostream& out, const std::vector<T>& vec)
    {
        size_t size = vec.size();

        out.write((char*)(&size), sizeof(size_t));
        out.write((char*)vec.data(), size * sizeof(T));
        return out;
    }
    /* read std vector form stream */
    template<typename T>
    std::istream& operator>> (std::istream& in, std::vector<T>& vec)
    {
        size_t size = 0;
        in.read((char*)(&size), sizeof(size_t));
        vec.resize(size);
        in.read((char*)vec.data(), size*sizeof(T));
        return in;
    }

    template< typename T_S, typename T_V>
    inline std::ostream& operator<< (std::ostream &out, const QTable<T_S, T_V>& qtable)
    {
        size_t size = qtable.size();
        out.write((char*)(&size), sizeof(size_t));

       for ( auto it = qtable.begin(); it != qtable.end(); ++it ) {
            const T_S& state = it->first;
            const T_V& Qs = it->second;
            out << state << Qs;
        }
        return out;
    }

    template< typename T_S, typename T_V>
    inline std::istream& operator>> (std::istream &in, QTable<T_S, T_V>& qtable)
    {
        size_t size = 0;
        T_S state;
        T_V Qs;

        in.read((char*)(&size), sizeof(size_t));
        for( size_t i = 0; i < size; i++ ) {
            in >> state;
            in >> Qs;
            qtable[state] = Qs;
        }
        return in;
    }

    template< typename T_S, typename T_A, typename T_V>
    inline std::ostream& operator<< (std::ostream &out, const QLearn<T_S, T_A, T_V>& qlearn)
    {
        double epsilon = qlearn.getEpsilon();
        out << epsilon;

        out << qlearn.getQTable();
        return out;
    }

    template< typename T_S, typename T_A, typename T_V>
    inline std::istream& operator>> (std::istream &in, QLearn<T_S, T_A, T_V>& qlearn)
    {
        double epsilon = 1.0;
        QTable<std::vector<T_S>, std::vector<T_V>> qtable;
        in >> epsilon;
        in >> qtable;
        qlearn.setEpsilon( epsilon );
        qlearn.setQTable( qtable );
        return in;
    }

    template< typename T_S, typename T_A, typename T_V>
    inline void save_readable(std::ostream &out, QLearn<T_S, T_A, T_V>& qlearn)
    {
        const QTable<std::vector<T_S>, std::vector<T_V>>& qtable = qlearn.getQTable();

        for ( auto it = qtable.begin(); it != qtable.end(); ++it ) {
            const std::vector<T_S>& state = it->first;
            const std::vector<T_V>& Qs = it->second;

            for(auto it = state.begin(); it != state.end(); ++it) {
                out << *it << ", ";
            }
            for(auto it = Qs.begin(); it != Qs.end(); ++it ) {
                out << *it << ", ";
            }
            out << std::endl;
        }
    }
}


#endif // UTILITIES_CPP
