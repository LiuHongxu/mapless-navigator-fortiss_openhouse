#ifndef QLEARNER_H
#define QLEARNER_H

#include "Simulator/matstruct.h"
#include "qlearnenvironment.h"
#include "qlearn.h"

class Qlearner
{
public:
    typedef int state_type; // QTable uses disretized state values

public:
    Qlearner(AgentEnvironment* agentenv);
    ~Qlearner();

    /* enable user controll */
    void set_user_controlled(bool is_user_controlled) { this->is_user_controlled = is_user_controlled; }

    /* start training session */
    void start(int total_episodes, int max_steps);
    /* main update loop call this function as fast as possible */
    int update(action_type user_action = -1);

private:
    AgentEnvironment* agentenv;
    matio::MatStruct monitor;

    bool is_started, is_after_reset, is_user_controlled;
    int total_episodes, max_steps;
    int cnt_episodes, cnt_steps;

    double epsilon;
    double epsilon_i;
    double epsilon_f;
    double alpha;
    double gamma;
    double epsilon_discount;
    ql::QLearn<state_type, action_type, reward_type> qlearn;

    action_type action;
    TypedEnvState<state_type> env_state;
    std::vector<state_type> state;
    std::vector<state_type> env_inital_state;

    /* monitored variables */
    float cumulated_reward;
    float highest_reward;
    std::vector<float> rewards;
    std::vector<float> epsilons;
    std::vector<int> num_of_steps;
    std::vector<int> visited_states;
    std::vector<int> selected_actions;
};

#endif // TESTQLEARNER_H
