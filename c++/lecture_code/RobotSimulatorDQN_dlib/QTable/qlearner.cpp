#include "qlearner.h"

#include <iostream>
#include <fstream>
#include "utilities.h"

using namespace ql;

Qlearner::Qlearner(AgentEnvironment *agentenv)
    : agentenv(agentenv), is_user_controlled(false)
{
    alpha = 1.0;
    gamma = 0.99;

    epsilon = 1.0;
    epsilon_i = 1.0;
    epsilon_f = 0.05;
    epsilon_discount = 0.9997; // 20000 -> 2 %

    qlearn.initialze(agentenv->get_n_actions(), epsilon, alpha, gamma);

    // read data from last session if available
    std::ifstream ifs("qtable.dat", std::ifstream::in);
    if(ifs.is_open()) {
        ifs >> qlearn; // will override epsilon & qtable !!!
        ifs.close();
    }
}

Qlearner::~Qlearner()
{
    // 0. show q Table
    qlearn.print();

    // 1. write q table content to binaray file
    std::ofstream ofs("qtable.dat", std::ofstream::out | std::ostream::binary);
    ofs << qlearn;
    ofs.close();

    // 2. store envrionment information in mat file
    monitor.add("cumulated_reward", rewards);
    monitor.add("num_of_steps", num_of_steps);
    monitor.add("epsilons", epsilons);
    monitor.add("visited_states", visited_states);
    monitor.add("selected_actions", selected_actions);
    monitor.add("highest_reward", highest_reward);
    monitor.write("environment_log.mat", "env_struct");

    // 3. write readable q table
    ofs.open("qtable.csv", std::ofstream::out );
    ql::save_readable(ofs, qlearn);
    ofs.close();
}

/* start training session */
void Qlearner::start(int total_episodes, int max_steps)
{
    this->total_episodes = total_episodes;
    this->max_steps = max_steps;
    rewards.reserve(total_episodes);
    epsilons.reserve(total_episodes);
    visited_states.reserve(total_episodes);
    selected_actions.reserve(total_episodes);

    cnt_episodes = 0;
    cnt_steps = 0;
    cumulated_reward = 0;
    highest_reward = 0;
    is_started = true;

    agentenv->reset();
    is_after_reset = true;
}

/* main update loop call this function as fast as possible */
int Qlearner::update(action_type user_action)
{
    agentenv->observe(env_state);

    // simulation started ?
    if(!is_started )
        return -1;  // not started

    // controlled by user and no action -> do nothing
    if(user_action < 0 && is_user_controlled)
        return 0;

    // simulation finished ?
    if(cnt_episodes > total_episodes) {
        std::cout << "Finishes after: " << cnt_episodes << " episodes highest reward:" << highest_reward << std::endl;
        return 1;   // finished
    }

    // simulation first iteration ?
    if(is_after_reset) {
        is_after_reset = false;

        // get inital observation
        agentenv->observe(env_state);
        state = env_state.state;
        env_inital_state = env_state.state;

        // get inital action
        action = qlearn.generate_action( state );
    }

    // 1. do observation
    agentenv->observe(env_state);

    // 2. if obsvervation is the same as befor continoue with current action
    if( env_state.state == state && !env_state.done) {
        agentenv->execute( action ); // do the same, no learning
        return -1;
    }

    // 2. learn
    // learn(state, action, reward, newstate)
    // std::cout << "action:" << action << " env_state.reward:" << env_state.reward << std::endl;
    qlearn.learn(state, action, env_state.reward, env_state.state, env_state.done);

    // 3. transition
    state = env_state.state;
    cumulated_reward += env_state.reward;
    highest_reward = env_state.reward > highest_reward ? env_state.reward : highest_reward;
    cnt_steps++;

    // 4. select next action
    if( !is_user_controlled )
        action = qlearn.generate_action( state );
    else
        action = user_action;
    agentenv->execute( action );

    // chek if episode finished
    if(cnt_steps == max_steps || env_state.done ) {
        std::cout << "EP: " << cnt_episodes << " steps: " << cnt_steps <<  " eps: " << qlearn.getEpsilon()
                  << " reward: " << cumulated_reward << " visited states: " << qlearn.size() << std::endl;

        // monitor variables
        rewards.push_back(cumulated_reward);
        epsilons.push_back(qlearn.getEpsilon());
        num_of_steps.push_back(cnt_steps);
        visited_states.push_back(qlearn.size());
        selected_actions.push_back(action);

        cnt_steps = 0;
        cumulated_reward = 0;

        if( qlearn.getEpsilon() > 0.05 )
            qlearn.setEpsilon( qlearn.getEpsilon() * epsilon_discount );

        cnt_episodes++;
        agentenv->reset();
        is_after_reset = true;
    }
    return 0;
}
