#include "qlearn.h"
#include <algorithm>

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;
using namespace ql;

template< typename T_S, typename T_A, typename T_V>
QLearn<T_S, T_A, T_V>::QLearn(int n_actions, double epsilon, double alpha, double gamma)
    : n_actions(n_actions), epsilon(epsilon), alpha(alpha), gamma(gamma)
{
    rng = std::mt19937(rd());
    int_uni = std::uniform_int_distribution<T_A>(0, n_actions-1);
    real_uni = std::uniform_real_distribution<double>(0, 1);
}

template< typename T_S, typename T_A, typename T_V>
void QLearn<T_S, T_A, T_V>::initialze(int n_actions, double epsilon, double alpha, double gamma)
{
    this->n_actions = n_actions;
    this->epsilon = epsilon;
    this->alpha = alpha;
    this->gamma = gamma;

    int_uni = std::uniform_int_distribution<T_A>(0, n_actions-1);
}

/* returns all q value of given state */
template< typename T_S, typename T_A, typename T_V>
std::vector<T_V>& QLearn<T_S, T_A, T_V>::getQs(const std::vector<T_S>& state)
{
    std::vector<T_V>& Qs = qtable[state];
    if(Qs.empty())
        Qs.resize(n_actions, 0); // not exsisting, allocate memory, fill with 0s
    return Qs;
}

template< typename T_S, typename T_A, typename T_V>
T_V QLearn<T_S, T_A, T_V>::getQ(const std::vector<T_S>& state, T_A action)
{
    return getQs(state)[action];
}

template< typename T_S, typename T_A, typename T_V>
void QLearn<T_S, T_A, T_V>::learn(const std::vector<T_S> &state, T_A action, T_V reward, const std::vector<T_S>& next_state, bool is_finished)
{
    // Q(s, a) += alpha * (reward(s,a) + max(Q(s')) - Q(s,a) )

    // 1. find maximum of actions for next_state:
    std::vector<T_V>& new_Qs = getQs(next_state);
    T_V max_q = *(std::max_element( new_Qs.begin(), new_Qs.end() ));

    // 2. get old value
    std::vector<T_V>& old_Qs = getQs(state);
    T_V old_q = old_Qs[action];

    // 3. compute update
    if( !is_finished )
        old_Qs[action] += alpha * ( reward + gamma * max_q - old_q );
    else
        old_Qs[action] = alpha * reward; // no next_state for final state
}

template< typename T_S, typename T_A, typename T_V>
T_A QLearn<T_S, T_A, T_V>::generate_action(const std::vector<T_S>& state)
{
    /*T_A action;
    std::vector<T_V>& Qs = getQs(state);
    auto max_elem = std::max_element(Qs.begin(), Qs.end());
    float exploration = std::min(1.0f, std::max(-1.0f, ( 166.0f - (float)*max_elem ) / ( 166.0f + 50.0f )));

    // choose eps greedy action
    if( real_uni(rng) > exploration ) {
        // select optimal action
        action = std::distance(Qs.begin(), max_elem);
    } else {
        // select complety random
        action = int_uni(rng);
    }
    return action;*/

    T_A action;
    std::vector<T_V>& Qs = getQs(state);

    // choose eps greedy action
    if( real_uni(rng) > epsilon ) {
        // select optimal action (hm what about multiple maximums??? )
        action = std::distance(Qs.begin(), std::max_element(Qs.begin(), Qs.end()));
    } else {
        // select complety random
        action = int_uni(rng);
    }
    return action;
}

template< typename T_S, typename T_A, typename T_V>
void QLearn<T_S, T_A, T_V>::print() const
{
    for ( auto it = qtable.begin(); it != qtable.end(); ++it ) {
        const std::vector<T_S>& state = it->first;
        const std::vector<T_V>& qVal = it->second;

        std::cout << "[";
        for( auto it_state = state.begin(); it_state != state.end(); ++it_state )
            std::cout << *it_state << " ";
        std::cout << "]=";
        for( auto it_qVal = qVal.begin(); it_qVal != qVal.end(); ++it_qVal)
            std::cout << *it_qVal << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// explicit template instantiation
template class ql::QLearn<int, int, double>;
template class ql::QLearn<int, size_t, double>;
template class ql::QLearn<int, int, float>;
template class ql::QLearn<int, size_t, float>;
template class ql::QLearn<char, char, double>;
template class ql::QLearn<char, char, float>;
