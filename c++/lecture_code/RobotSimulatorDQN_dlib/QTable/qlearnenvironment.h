#ifndef QLEARNENVIRONMENT_H
#define QLEARNENVIRONMENT_H

#include "Simulator/agentenvironment.h"

/*
 * Decribes the Environment as perceived form the perspective of a individual agent
 */
class QLearnEnvironment : public AgentEnvironment
{

public:
    QLearnEnvironment(Environment *env, ag::Agent* agent, const b2Vec2& goal_position);

    void set_goal_position(const b2Vec2& goal_position );

    /* perform one step (apply action) */
    void execute(action_type action);
    /* returns the new observations */
    bool observe(EnvState& env_state);
    /* do random reset, keep area around goal position free */
    void reset(bool random = false, b2AABB* clear_area = NULL);

private:
    // robot state settings
    b2Vec2 goal_position;                   // goal position
    b2AABB clear_goal_area;                 // free area around gool position
    float min_goal_dist;                    // min distance to goal     (stop simulation)
    float min_obstical_dist;                // min distance to obsticle (stop simulation)
    int range_dim;                          // number of rays for state

    // robot state
    float distance;                         // robot world position
    float theta;                            // robto world angle
    std::vector<int> discretized_ranges;    // discretized laser range data

    action_type prev_action;
    float prev_distance;
    std::vector<int> prev_state;

    std::vector<int> cashed_angles;

    /* setup variables */
    void setup();

    /* callback modelstates: compute distance + angle to goal */
    void modelStatesCallback(const ag::ModelStates& modelstate);
    /* callback lidar data: discretize ranges */
    bool scanCallback(const ag::SensorReading& ranges, double min_range);

    /* discretize distance and angle to goal */
    static bool discretize_goal_dist(float distance, float* discretized_distance, float min_range);
    float discretize_goal_angle(float theta);

    /* signed rotation angle form src to dst */
    static float rotation_angle(float src, float dst);
};

#endif // QLEARNENVIRONMENT_H
