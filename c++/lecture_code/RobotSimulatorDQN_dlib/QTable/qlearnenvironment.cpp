#include "qlearnenvironment.h"

#include <iostream>

QLearnEnvironment::QLearnEnvironment(Environment *env, ag::Agent* agent, const b2Vec2& goal_position)
    : AgentEnvironment(env, agent, 3, 7), goal_position(goal_position)
{
    // setup hyper parameters
    min_goal_dist = 0.2f;
    min_obstical_dist = 0.2f;
    range_dim = 5;
    setup();
}

/* setup variables */
void QLearnEnvironment::setup()
{
    // setup free area around goal
    clear_goal_area.upperBound = b2Vec2(min_goal_dist / 2.0f, min_goal_dist / 2.0f) + goal_position;
    clear_goal_area.lowerBound = b2Vec2(-min_goal_dist / 2.0f, -min_goal_dist / 2.0f) + goal_position;

    // number of laser rays
    discretized_ranges.resize(range_dim);

    // prev state vector
    prev_state.resize( this->get_dim_states() );

    // cash discretized angles [ 0 - 180 ]
    cashed_angles.resize(180);
    for(int i = 0; i < 180; ++i) {
        if(i < 5)
            cashed_angles[i] = 0;
        else if(i < 15)
            cashed_angles[i] = 1;
        else if(i < 30)
            cashed_angles[i] = 2;
        else if(i < 60)
            cashed_angles[i] = 3;
        else if(i < 90)
            cashed_angles[i] = 4;
        else
            cashed_angles[i] = 5;
    }

}

void QLearnEnvironment::set_goal_position(const b2Vec2& goal_position )
{
    this->goal_position = goal_position;
    clear_goal_area.upperBound = b2Vec2(min_goal_dist / 2.0f, min_goal_dist / 2.0f) + goal_position;
    clear_goal_area.lowerBound = b2Vec2(-min_goal_dist / 2.0f, -min_goal_dist / 2.0f) + goal_position;
}

void QLearnEnvironment::execute(action_type action)
{
    // 1. remember last distance
    prev_action = action;

    // 2. execute action
    ag::VelCmd vel_cmd;
    switch(action) {
    case 0:
        vel_cmd.velocity.x = 0.3;
        vel_cmd.omega = 0.0;
        break;
    case 1:
        vel_cmd.velocity.x  = 0.0; //0.05;
        vel_cmd.omega = 0.3;
        break;
    case 2:
        vel_cmd.velocity.x = 0.0; //0.05;
        vel_cmd.omega = -0.3;
        break;
    }
    agent->command( vel_cmd );
}

bool QLearnEnvironment::observe(EnvState& env_state)
{
    float discretized_distance, discretized_theta;
    bool hit_obstacle, reached_goal, new_observation;
    hit_obstacle = reached_goal = new_observation = false;

    ag::SensorReading sensorreadings;
    ag::ModelStates modelstate;
    new_observation = agent->model_states( modelstate );
    new_observation |= agent->observation( sensorreadings );

    // 1. simulate the callback function
    modelStatesCallback( modelstate );
    hit_obstacle = scanCallback( sensorreadings, min_obstical_dist );

    // 2. convert relative position to goal into discretived state
    reached_goal = discretize_goal_dist(distance, &discretized_distance, min_goal_dist);
    discretized_theta = discretize_goal_angle(theta);

    // 3. compute reward
    if(prev_distance < 0.0)
        prev_distance = distance; // init step

    float reward;
    if(hit_obstacle)
        reward = -50.0f;
    else if(reached_goal)
        reward = 100.0f;
    else {
        reward = std::min(3.0f, std::max(-3.0f, 15.0f * (prev_distance - distance) ) );
    }

    if( prev_action != 0 )
        reward -= 1.0;

    // 4. combine into a single state vector
    //std::cout << "theta:" << discretized_theta << " d:" << discretized_distance << std::endl;
    //std::cout << "[a]: " << discretized_ranges[0] << " [b]: " <<discretized_ranges[1] << " [c]: " << discretized_ranges[2] << std::endl;

    std::cout << "distance: " << distance << " angle: " << sim::RAD2DEG * theta << std::endl;

    auto env_state_t = static_cast<TypedEnvState<int>* >(&env_state);
    env_state_t->state = discretized_ranges;
    env_state_t->state.push_back(discretized_distance);
    env_state_t->state.push_back(discretized_theta);
    env_state_t->reward = reward;
    env_state_t->done = hit_obstacle | reached_goal;

    if( prev_state != env_state_t->state ) {
        prev_distance = distance;
        prev_state = env_state_t->state;
    }
}

void QLearnEnvironment::reset(bool random, b2AABB* clear_area)
{
    AgentEnvironment::reset(&clear_goal_area);
}

bool QLearnEnvironment::discretize_goal_dist(float distance, float* discretized_distance, float min_range)
{
    // discretize distance
    // 6m grid, 0.05m resolution: 170 possible observations [0, 169]
    *discretized_distance = (int)(distance / 0.2); // 20cm: 0.2 -> 43 states | 5 cm: 0.05 -> 170 | TODO: Set to 0.05 aganin
    return distance < min_range;
}

float QLearnEnvironment::discretize_goal_angle(float theta)
{
    float deg = sim::RAD2DEG * std::abs(theta);

    if(theta < 0 || deg < 5.0f)
        theta = cashed_angles[(int)(deg)];        // [-180, 0]
    else
        theta = cashed_angles[(int)(deg)] + 6;   // [0, 180]

    return theta;
}

bool QLearnEnvironment::scanCallback(const ag::SensorReading& ranges, double min_range)
{
    int mod = (int)(ranges.size() / discretized_ranges.size() );
    bool done = false;

    // dim^6 possible observations (not used if world is empty!)
    int k  = 0;
    for(unsigned i = 0; i < ranges.size(); ++i) {
        if(i % mod == 0) {
            if( std::isinf(ranges[i]) ) // catch inf
                discretized_ranges[k] = 2;
            else if( std::isnan(ranges[i]) ) // catch nan
                discretized_ranges[k] = 0;
            else { // discretize
                if(ranges[i] < 0.6 )
                    discretized_ranges[k] = 0;
                else if(ranges[i] < 1.5)
                    discretized_ranges[k] = 1;
                else
                    discretized_ranges[k] = 2;
            }
            k++;
        }
        // done?
        if(ranges[i] < min_range)
            done = true;
    }
    return done;
}

void QLearnEnvironment::modelStatesCallback(const ag::ModelStates& modelstate)
{
    // distance to goal
    b2Vec2 distance_vec = goal_position - modelstate.position;
    distance = sqrt(distance_vec.x * distance_vec.x + distance_vec.y * distance_vec.y);

    // angle to goal
    theta = rotation_angle(atan2(distance_vec.y, distance_vec.x), modelstate.theta );
}

float QLearnEnvironment::rotation_angle(float src, float dst)
{
    return (src > dst) ? -sim::PI + std::fmod(src - dst + sim::PI, sim::PI_2)
                       :  sim::PI - std::fmod(dst - src + sim::PI, sim::PI_2);
}
