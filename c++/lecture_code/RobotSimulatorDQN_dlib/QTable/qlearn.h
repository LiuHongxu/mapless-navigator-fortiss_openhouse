#ifndef QLEARN_H
#define QLEARN_H

#include <vector>
#include <unordered_map>
#include <random>
#include <boost/functional/hash.hpp>

namespace ql {

    /*
     *  define hash function that operates on entire sequences
     */
    template < typename T_S > struct seq_hash {
        std::size_t operator() ( const T_S& s ) const {
            std::size_t hash = 0 ;
            boost::hash_range( hash, s.begin(), s.end() ) ;
            return hash ;
        }
    };
    /*
     * Implement QTable as unordered map, hash (key) is based on state vector
     * Map saves vectors with q values for each action (allows faster max operation)
     */
    template< typename T_S, typename T_V >
    using QTable = std::unordered_map< T_S, T_V, seq_hash<T_S> > ;

    /*
     * QLearning
     */
    template< typename T_S, typename T_A, typename T_V>
    class QLearn
    {
    public:
        QLearn(int n_actions = 2, double epsilon = 1.0, double alpha = 1.0, double gamma = 1.0);

        /* set all learning parameters */
        void initialze(int n_actions, double epsilon, double alpha, double gamma);
        /* clear qtable */
        void clear() { qtable.clear(); }

        size_t size() const { return qtable.size(); }

        void setEpsilon(double epsilon) { this->epsilon = epsilon; }
        double getEpsilon() const { return epsilon; }

        /*
         * do learning step:
         *  s1: prev state
         *  a: action
         *  r: reward
         *  s2: current state
         */
        void learn(const std::vector<T_S> &state, T_A action, T_V reward, const std::vector<T_S>& next_state, bool is_finished);

        /* generates action based on q table */
        T_A generate_action(const std::vector<T_S>& state);

        /* returns q value of given state action pair */
        T_V getQ(const std::vector<T_S>& state, T_A action);

        /* return entire qtable */
        const QTable<std::vector<T_S>, std::vector<T_V>>& getQTable() const { return qtable; }
        /* set entire qtable */
        void setQTable(const QTable<std::vector<T_S>, std::vector<T_V>>& qtable) { this->qtable = qtable; }

        /* printing for debug tests */
        void print() const;

    private:
        QTable<std::vector<T_S>, std::vector<T_V>> qtable;
        int n_actions;
        double epsilon;
        double alpha;
        double gamma;

        std::random_device rd;  //  (seed) engine
        std::mt19937 rng;       // random-number engine
        std::uniform_int_distribution<T_A> int_uni;
        std::uniform_real_distribution<double> real_uni;

        /* returns all q value of given state */
        std::vector<T_V>& getQs(const std::vector<T_S>& state);
    };
}

#endif // QLEARN_H
