#include <QCoreApplication>
#include "Simulator/simconstants.h"
#include "Simulator/contactlistener.h"
#include "Simulator/environment/mazeenvironment.h"
#include "Tests/singleagentrobotlearner.h"

int main(int argc, char *argv[])
{
    float realtime_factor;
    bool graphics = true;

    // 1. create environment
    float loop_freq = 100.0f;
    MazeEnvironment environment(b2Vec2(0.0f, 0.0f), loop_freq);

    // 2. create learner
    SingleAgentRobotLearner learner(&environment, "log/");
    learner.set_mode(DQNLearner::Mode::TRAIN);
    learner.setup();

    // 3. go!!!
    learner.start(9000000);
    environment.start(true);
    while(environment.is_started()) {
        // 1. simulation step
        realtime_factor = environment.update();

        // 2. learning step
        if(learner.update( ) == 1)
            environment.start(false);

        // 3. take user input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F1))
            graphics = true;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F2))
            graphics = false;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F3))
            learner.save_all();
        if(graphics)
            environment.update_graphics();
    }
    // learning stopped, save all
    learner.save_all();
    return 0;
}
