#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "body.h"
#include "simconstants.h"
#include <random>

/*
 * Create sf::Vertex Array of line segments,
 * faster than sending each line seperated to graphics card
 */
class Grid : public sf::Drawable, public sf::Transformable
{
public:
    Grid(const b2Vec2& size, const sf::Color& color = sf::Color(200, 200, 200)) {
        sf::Vector2f tilesize = sf::Vector2f(sim::METER2PIXEL_X * size.x, sim::METER2PIXEL_Y * size.y);
        int cols = sim::WINDOWSIZE_X / tilesize.x - 1;
        int rows = sim::WINDOWSIZE_Y / tilesize.y -1;

        lines.setPrimitiveType(sf::Lines);
        lines.resize(2*(rows+cols));

        size_t k = 0;
        for(int i = 0; i < cols; ++i, k += 2) {
            lines[k].position = sf::Vector2f(0.0f, tilesize.y * (i + 1));
            lines[k].color = color;
            lines[k + 1].position = sf::Vector2f(sim::WINDOWSIZE_Y, tilesize.y * (i + 1));
            lines[k + 1].color = color;
        }
        for(int i = 0; i < rows; ++i, k += 2) {
            lines[k].position = sf::Vector2f(tilesize.x * (i + 1), 0.0f);
            lines[k].color = color;
            lines[k + 1].position = sf::Vector2f(tilesize.x * (i + 1), sim::WINDOWSIZE_X);
            lines[k + 1].color = color;
        }
    }

private:
    sf::VertexArray lines;

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(lines, states);
    }
};

/*
 * Environment runs physics simulator and manages bodies
 * Bodies will be destroyed by Environment
 * Subclass this to create custorm Environemnts
 *  - reimplement on_add() which describes how to add an object
 *  - reimplenent on_reset() which describes how to reset the bodes inside the world
 */
class Environment : public sf::Drawable, public sf::Transformable
{
public:
    Environment(const b2Vec2& gravity = b2Vec2(0.0f, 0.0f), float loop_frequency = -1.0f, float step_size = 0.05f );
    virtual ~Environment();

    bool is_started() const { return _is_started; }

    /* add new body to environment */
    void add(Body *body, int pos = -1);

    /* start, stop simulation */
    void start(bool ok);

    /* add walls to the environment */
    void setWall(const b2Vec2& margin = b2Vec2(0.025f, 0.025f));

    /* reset back to inital state
     * clear_area will never be occupied by an object */
    void reset(b2AABB* clear_area = nullptr);

    /* main loop of environment (call as fast as possible)
       returns realtime factor of simulator */
    float update();

    /* return the step size dt of the simulator */
    float get_stepsize() { return step_size; }

    // return ptr to physics simulator object
    b2World* get_b2World() const { return world; }

    // return ptr to graphics window
    sf::RenderWindow* get_sfWindow() const { return window; }

    void update_graphics();
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

protected:
   /* add new body to environment */
   virtual void on_add(b2World *world, Body *body, int pos = -1);

    /* reset back to inital state
     * clear_area will never be occupied by an object */
    virtual void on_reset(std::vector<Body*>::iterator start, std::vector<Body*>::iterator stop, std::vector<Body*>& bodies, b2AABB* clear_area = nullptr);

private:
    sf::RenderWindow* window;
    b2World* world;

    sf::Clock clock;
    float loop_frequency;
    float realtime_factor;
    float step_size;

    float dt;                       // delta time
    float elapsed_time;             // measure framerate
    int cnt;

    bool _is_started;               // simulation started
    bool graphic_activated;         // graphical output activated
    bool walls_activated;           // world has boundary walls

    Grid grid;                      // background
    std::vector<Body*> bodies;      // vector of all bodies that take part in simulation
};

#endif // ENVIRONMENT_H
