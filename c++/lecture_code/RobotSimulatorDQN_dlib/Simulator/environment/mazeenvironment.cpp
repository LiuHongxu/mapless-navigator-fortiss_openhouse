#include "mazeenvironment.h"
#include "Simulator/staticobstacle.h"
#include "Simulator/agent.h"

using namespace ag;

// Constructor.
DisjointSets::DisjointSets(size_t n)
    : n(n)
{
    parent.resize(n+1);
    rnk.resize(n+1);

    // Initially, all vertices are in
    // different sets and have rank 0.
    for (size_t i = 0; i <= n; i++) {
        rnk[i] = 0;
        //every element is parent of itself
        parent[i] = i;
    }
}

// Find the parent of a node 'u'
// Path Compression
size_t DisjointSets::find(size_t u)
{
    /* Make the parent of the nodes in the path
       from u--> parent[u] point to parent[u] */
    if (u != parent[u])
        parent[u] = find(parent[u]);
    return parent[u];
}

// Union by rank
void DisjointSets::merge(size_t x, size_t y)
{
    x = find(x); y = find(y);

    /* Make tree with smaller height
       a subtree of the other tree  */
    if (rnk[x] > rnk[y])
        parent[y] = x;
    else // If rnk[x] <= rnk[y]
        parent[x] = y;

    if (rnk[x] == rnk[y])
        rnk[y]++;
}

//--------------------------------------------------------------------------------------------------------------------------

Graph::Graph(size_t v, size_t e) : v(v), e(e)
{
}

/* add edge between node u and v, with weight w */
void Graph::add_edge(size_t u, size_t v, size_t w)
{
    edges.push_back({w, {u, v}});
}

/* Functions returns weight of the MST*/

std::vector< Graph::Pair > Graph::kruskal_mst()
{
   std::vector< Graph::Pair > mst_edges;

   // Sort edges in increasing order on basis of cost
   std::sort(edges.begin(), edges.end());

   // Create disjoint sets
   DisjointSets ds(v);

   // Iterate through all sorted edges
   for (auto it = edges.begin(); it != edges.end(); ++it)
   {
       size_t u = it->second.first;
       size_t v = it->second.second;

       size_t set_u = ds.find(u);
       size_t set_v = ds.find(v);

       // Check if the selected edge is creating
       // a cycle or not (Cycle is created if u
       // and v belong to same set)
       if (set_u != set_v)
       {
           // Current edge will be in the MST
           // so print it
           mst_edges.push_back( { u, v } );

           // Merge two sets
           ds.merge(set_u, set_v);
       }
   }
   return mst_edges;
}

//--------------------------------------------------------------------------------------------------------------------------


MazeEnvironment::MazeEnvironment(const b2Vec2& gravity, float loop_frequency, float step_size )
    : Environment(gravity, loop_frequency, step_size), maze_start(0)
{
    drop_out = 0.25f;
    tile_size = b2Vec2(1.0f, 1.0f);
    cols = sim::WORLDSIZE_X / tile_size.x;
    rows = sim::WORLDSIZE_Y / tile_size.y;

    rng = std::mt19937(rd());
    int_uni = std::uniform_int_distribution<size_t>(0, cols * rows);    // rand num for maze generator

    rng = std::mt19937(rd());
    real_uni = std::uniform_real_distribution<float>(0.0f, 1.0f);
    real_uni_x = std::uniform_real_distribution<float>(0.3f, sim::WORLDSIZE_X - 0.3f);
    real_uni_y = std::uniform_real_distribution<float>(0.3f, sim::WORLDSIZE_Y - 0.3f);
    real_uni_theta = std::uniform_real_distribution<float>(-sim::PI, sim::PI);
}

MazeEnvironment::~MazeEnvironment()
{
}

/* add new body to environment */
void MazeEnvironment::on_add(b2World *world, Body *body, int pos)
{
    // check if body is an agent
    ag::Agent* agent = dynamic_cast<Agent*>(body);
    if(agent)
        agents.push_back(body);

    // base class fnc
    Environment::on_add(world, body, pos);
}

/* reset back to inital state
* clear_area will never be occupied by an object */
void MazeEnvironment::on_reset(std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, std::vector<Body*>& bodies, b2AABB* clear_area)
{
    // 1. construct a random grid like graph
    size_t nodes = rows * cols;
    size_t edges = 2 * rows * cols - ( rows + cols );
    Graph graph(nodes, edges);

    size_t k = 0;
    for( size_t i = 0; i < rows; ++i) {
        for( size_t j = 0; j < cols; ++j) {
            if( j != cols - 1)
                graph.add_edge(k, k + 1, int_uni(rng));     // horizontal edges k -> k + 1
            if( i != rows - 1 )
                graph.add_edge(k, k + cols, int_uni(rng));  // vertical edges k -> k + cols
            k++;
        }
    }
    std::vector<Graph::Pair> mst = graph.kruskal_mst();

    // 2. delete maze form prev iteration
    if( maze_start > 0) {
        for( auto it = start + maze_start; it != end; ++it ) {
            delete (*it);
        }
        bodies.erase(start + maze_start, end);
    }
    maze_start = std::distance(start, bodies.end());

    // 3. build new maze
    b2Vec2 size_v = b2Vec2(0.05f, tile_size.x);
    b2Vec2 size_h = b2Vec2(tile_size.y, 0.05f);

    // only set walls if they do not block the minimum spanning tree
    k = 0;
    for( size_t i = 0; i < rows; ++i) {
        for( size_t j = 0; j < cols; ++j) {
            if( std::find(mst.begin(), mst.end(), Graph::Pair(k, k + cols) ) == mst.end() ) {
                b2Vec2 pos = b2Vec2( (j + 0.5f) * tile_size.x, (i+1) * tile_size.y );
                if( real_uni(rng) > drop_out)
                    Environment::add( new StaticRectangle(pos, size_h) );   // horizontal wall
            }
            if( std::find(mst.begin(), mst.end(), Graph::Pair(k, k + 1) ) == mst.end() ) {
                b2Vec2 pos = b2Vec2( (j+1) * tile_size.x, (i + 0.5f) * tile_size.y );
                if( real_uni(rng) > drop_out)
                    Environment::add( new StaticRectangle(pos, size_v) );   // vertical wall
            }
            k++;
        }
    }
    // TODO !!!
    // 4. randomly place obsticles into the finished maze
    rand_place_bodies(bodies.begin() + 4, bodies.begin() + 4 + maze_start, bodies.begin() + 4 + maze_start, bodies.end());
}

void MazeEnvironment::rand_place_bodies(std::vector<Body*>::iterator start_obj, std::vector<Body*>::iterator obj_end,
                       std::vector<Body*>::iterator start_maze, std::vector<Body*>::iterator end_maze,  b2AABB* clear_area)
{
    b2Vec2 rand_pos;
    float rand_orient;

    for( auto it = start_obj; it != obj_end; ++it) {
        // set new random position
        int cnt = 0;
        rand_pos.x = real_uni_x(rng);
        rand_pos.y = real_uni_y(rng);
        rand_orient = real_uni_theta(rng);
        (*it)->reset( rand_pos, rand_orient );

        // check if positon overlapps with something else (resample)
        b2AABB rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
        if(!clear_area) {
            while(is_colliding( rect, start_maze, end_maze) && cnt < 1000 ) { // check for overlapping
                rand_pos.x = real_uni_x(rng);
                rand_pos.y = real_uni_y(rng);
                rand_orient = real_uni_theta(rng);
                (*it)->reset( rand_pos, rand_orient );
                rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                cnt++;
            }
        }
        else {
            while(( is_colliding( rect, start_maze, end_maze) || sim::intersect(rect, *clear_area) ) && cnt < 1000 ) { // check for overlapping + clear area
                rand_pos.x = real_uni_x(rng);
                rand_pos.y = real_uni_y(rng);
                rand_orient = real_uni_theta(rng);
                (*it)->reset( rand_pos, rand_orient );
                rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                cnt++;
            }
        }
    }
}

bool MazeEnvironment::is_colliding(const b2AABB& rectA, std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, const b2Vec2 margin)
{
    for(auto it = start; it != end; ++it) {
        b2AABB rectB = (*it)->getBoundingRectangle(margin, true);
        if(sim::intersect(rectA, rectB)) {
            return true; // overlapping
        }
    }
    return false;
}
