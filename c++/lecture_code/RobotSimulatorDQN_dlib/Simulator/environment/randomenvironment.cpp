#include "randomenvironment.h"

RandomEnvironment::RandomEnvironment(const b2Vec2& gravity, float loop_frequency, float step_size )
    : Environment(gravity, loop_frequency, step_size)
{
    rng = std::mt19937(rd());
    real_uni_x = std::uniform_real_distribution<float>(0.3f, sim::WORLDSIZE_X - 0.3f);
    real_uni_y = std::uniform_real_distribution<float>(0.3f, sim::WORLDSIZE_Y - 0.3f);
    real_uni_theta = std::uniform_real_distribution<float>(-sim::PI, sim::PI);
}

 /* reset back to inital state
  * clear_area will never be occupied by an object */
void RandomEnvironment::on_reset(std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, std::vector<Body*>& bodies, b2AABB* clear_area)
{
    b2Vec2 rand_pos;
    float rand_orient;

    for( auto it = start; it != end; ++it) {
        // set new random position
        int cnt = 0;
        rand_pos.x = real_uni_x(rng);
        rand_pos.y = real_uni_y(rng);
        rand_orient = real_uni_theta(rng);
        (*it)->reset( rand_pos, rand_orient );

        // check if positon overlapps with something else (resample)
        b2AABB rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
        if(!clear_area) {
            while(is_colliding( rect, start, it) && cnt < 1000 ) { // check for overlapping
                rand_pos.x = real_uni_x(rng);
                rand_pos.y = real_uni_y(rng);
                rand_orient = real_uni_theta(rng);
                (*it)->reset( rand_pos, rand_orient );
                rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                cnt++;
            }
        }
        else {
            while(( is_colliding( rect, start, it) || sim::intersect(rect, *clear_area) ) && cnt < 1000 ) { // check for overlapping + clear area
                rand_pos.x = real_uni_x(rng);
                rand_pos.y = real_uni_y(rng);
                rand_orient = real_uni_theta(rng);
                (*it)->reset( rand_pos, rand_orient );
                rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                cnt++;
            }
        }
    }
}

bool RandomEnvironment::is_colliding(const b2AABB& rectA, std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, const b2Vec2 margin)
{
    for(auto it = start; it != end; ++it) {
        b2AABB rectB = (*it)->getBoundingRectangle(margin, true);
        if(sim::intersect(rectA, rectB)) {
            return true; // overlapping
        }
    }
    return false;
}
