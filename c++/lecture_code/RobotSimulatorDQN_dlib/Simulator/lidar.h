#ifndef LIDAR_H
#define LIDAR_H

#include "sensor.h"
#include "raycast.h"

/*
 * Simulates Laser range scanner
 * Call attach_body() function to link sensor to an b2body, befor using any
 * of the update functions
 */

class Lidar : public Sensor, public sf::Drawable, private sf::Transformable
{
public:
    Lidar(int ray_number, float max_range, float visual_angle, float freq = 0.01f, float mean = 0.0f, float std = 0.0f);
    virtual ~Lidar();

    /* link sensor to a b2body (for position) */
    void attach_body(b2Body* body, b2Vec2 pos = b2Vec2(0.0f, 0.0f), float angle = 0.0f);

    /* get data */
    std::vector<float> get_ranges() { Sensor::read(); return ranges; }

    /* update ray data */
    bool update(float dt);
    /* update line drawings */
    void update_graphics();

private:
    // characteristic
    size_t ray_number;     // number or rays
    float max_range;    // maximum range to look for collisions
    float visual_angle; // +- angle from 0 deg

    std::vector<b2Vec2> ray_points;     // ray end points

    std::vector<RayCast*> ray_casts;
    std::vector<float> ranges;

    // sfml draw
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // LIDAR_H
