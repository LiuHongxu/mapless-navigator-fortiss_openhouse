#ifndef RAYCAST_H
#define RAYCAST_H

#include "Box2D/Box2D.h"
#include "SFML/Graphics.hpp"
#include "body.h"

class RayCast : public b2RayCastCallback, public sf::Drawable
{
public:
    struct RayData {
        const Body* startObject;  // The object (if any) where this ray started
        Body* closestObject;        // The closest object
        b2Vec2 start;               // The starting point of the ray cast
        b2Vec2 end;                 // The maximum point on the ray
        float  direction;           // The angle (in radians) at which the ray was cast
        float distance;             // The distance between start and end point
        float  shortestDist;        // The closest hitPoint on that object
        b2Vec2 hitPoint;            // The point where the ray hit

        RayData() : closestObject(nullptr), start(0.0f, 0.0f), end(0.0f, 0.0f), shortestDist(-1) {}
    };

    RayCast(b2World *world);

    virtual ~RayCast();

    /* cast ray form start_vec with angle and max distance */
    bool cast(const b2Vec2& start_vec, float angle, float distance, const Body* start_body = nullptr);
    bool cast(const b2Vec2& start_vec, const b2Vec2& end_vec, float distance, const Body* start_body = nullptr);

    /* return results of cast */
    const RayData& get_data() const { return data; }

    /* update lines */
    void update_graphics();

    // callback of box2d engine
    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction);

private:
    b2World *world;
    RayData data;

    // sfml visualization
    sf::VertexArray line;

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // RAYCAST_H
