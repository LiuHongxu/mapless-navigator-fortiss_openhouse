#ifndef SENSOR_H
#define SENSOR_H

#include <Box2D/Box2D.h>
#include <random>

/*
 * Base class for every sensor
 */
class Sensor
{
public:
    /* update freq, noise mean, noise standard deviation */
    Sensor(float freq = 0.01f, float mean = 0.0f, float std = 0.0f)
        : period( 1.0f / freq )
    {
        // init random generators
        rng = std::mt19937(rd());
        real_gaussian = std::normal_distribution<float>(mean, std);

        // force immediate sensor update on start
        elapsed = period + 1.0f;
    }
    virtual ~Sensor() { }

    /* ture if sensor been updated, false if old data */
    bool is_avialable() {
        return available;
    }

    /* reset to inital state */
    virtual void reset() {
        // force immediate sensor update after reset
        elapsed = period + 1.0f;
    }

    /* link sensor to a b2body (for position and world access) */
    virtual void attach_body(b2Body* body, b2Vec2 pos, float angle)
    {
        this->body = body;
        this->pos = pos;
        this->angle = angle;
        world = body->GetWorld();
    }

    /* update sensor returns true if new data available otherwise data are not updated
     * use base class function to check if sensor values must be updated */
    virtual bool update(float dt) {
        elapsed += dt;
        if( elapsed > period) {
            elapsed = 0.0f;
            available = true;
            return true;
        }
        return false;
    }

protected:
    b2World *world;
    b2Body *body;

    b2Vec2 pos;     // realtive pos to body center
    float angle;    // realtive angle to body

    // noise ( addative gaussian noise )
    std::random_device rd;                          // (seed) engine
    std::mt19937 rng;                               // random-number engine
    std::normal_distribution<float> real_gaussian;  // noise

    /* call from derived class if data has been read */
    void read() { available = false; }

private:
    float period;
    float elapsed;
    bool available;
};

#endif // SENSOR_H
