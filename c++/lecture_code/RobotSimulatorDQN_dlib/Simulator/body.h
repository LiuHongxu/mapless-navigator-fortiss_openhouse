#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

/*
 * Baseclass all objects that can be drawn on the screen and take part in the simulated envrionment
 */
class Body : public sf::Drawable, public sf::Transformable
{
public:
    Body(b2BodyType bodytype, const b2Vec2& pos = b2Vec2(0.0, 0.0), float orientation = 0.0);
    virtual ~Body();

    /*
     * register body within the world
     * construct class here, not in constructor
     */
    virtual void construct(b2World* world);

    /* b2Vec2 callback function on collisions */
    virtual void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) = 0;
    virtual void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) = 0;

    /* update body */
    virtual void update(float dt) = 0;

    /* update graphical content */
    virtual void update_graphics();

    /* reset to given pose */
    virtual void reset(const b2Vec2& pos = b2Vec2(0.0f, 0.0f) , float orientation = 0.0f);

    /* return bounding rectangle (parallel to x-y axis)
     * add margin to increase bounding rectangle
     * global flag to get absolute corner positions, rather than local ones */
    b2AABB getBoundingRectangle(const b2Vec2& margin = b2Vec2(0.0f, 0.0f), bool global = false) const;

    /* return current position */
    b2Vec2 getPosition() const;

    /* return body */
    b2Body* get_body() { return b2body; }

    // normal_relative_angle
    static float normal_relative_angle( float angle );

protected:
    b2Body *b2body;
    sf::Shape *sfshape;
    b2BodyDef body_def;

    // sfml draw
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // ENTITY_H
