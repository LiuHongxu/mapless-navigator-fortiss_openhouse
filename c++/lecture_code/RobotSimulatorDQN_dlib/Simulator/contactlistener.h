#ifndef CONTACTLISTENER_H
#define CONTACTLISTENER_H

#include "body.h"

class ContactListener : public b2ContactListener
{
public:
    ContactListener(b2World *world)
    {
        world->SetContactListener(this);
    }
    virtual ~ContactListener() {
    }

    // Called when two fixtures begin to touch
    virtual void BeginContact(b2Contact* contact)
    {
        b2Fixture *fixtureA = contact->GetFixtureA();
        b2Fixture *fixtureB = contact->GetFixtureB();

        Body *typeA = static_cast<Body*>(fixtureA->GetBody()->GetUserData());
        Body *typeB = static_cast<Body*>(fixtureB->GetBody()->GetUserData());

        // only call the contact func if it there bodyfixtures collide and not there sensors
        if (!fixtureA->IsSensor())
            typeA->beginContact(typeB, fixtureB, contact);
        if (!fixtureB->IsSensor())
            typeB->beginContact(typeA, fixtureA, contact);
    }

    // Called when two fixtures cease to touch
    virtual void EndContact(b2Contact* contact)
    {
        b2Fixture *fixtureA = contact->GetFixtureA();
        b2Fixture *fixtureB = contact->GetFixtureB();

        Body *typeA = static_cast<Body*>(fixtureA->GetBody()->GetUserData());
        Body *typeB = static_cast<Body*>(fixtureB->GetBody()->GetUserData());

        // only call the contact func if it there bodyfixtures collide and not there sensors
        if (!fixtureA->IsSensor())
            typeA->endContact(typeB, fixtureB, contact);
        if (!fixtureB->IsSensor())
            typeB->endContact(typeA, fixtureA, contact);
    }
};

#endif // CONTACTLISTENER_H
