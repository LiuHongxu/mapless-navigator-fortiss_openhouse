#include <QCoreApplication>
#include "robot.h"
#include "staticobstacle.h"
#include "simconstants.h"
#include "contactlistener.h"
#include "environment.h"
#include "car.h"

#include <iostream>
#include <fstream>

int main(int argc, char *argv[])
{
    // Environment bodies
    float robot_pos_std = 0.0f; //0.01;
    float robot_orient_std = 0.0f * sim::DEG2RAD;
    float robot_lin_vel_std = 0.0f; //0.003; // error 1%
    float robot_ang_vel_std = 0.0f;  // error 3.3%

    Robot* robot = new Robot(b2Vec2(1, 1), 1.56f, 0.175f, robot_pos_std, robot_orient_std, robot_lin_vel_std, robot_ang_vel_std); // pos, angle, radius
    Tire* tile = new Tire(b2Vec2(3, 3), b2Vec2(0.05f, 0.1f), 10.f);
    Car* car = new Car(b2Vec2(2, 2));

    // create environment
    Environment environment(b2Vec2(0.0f, 0.0f), 100.0f);
    environment.add(robot);
    environment.add(tile);
    environment.add(car);
    environment.setWall();

    // loop
    float realtime_factor;
    int cnt = 0;
    bool graphics = true;

    environment.start(true);
    while(environment.is_started())
    {
        // 1. simulation step
        realtime_factor = environment.update();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
            car->set_lin_velo(0.3f);
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
            car->set_lin_velo(-0.15f);
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
            car->set_steering_angle(40 * sim::DEG2RAD);
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
            car->set_steering_angle(-40 * sim::DEG2RAD);

        ag::ModelStates modelstate;
        car->model_states( modelstate );

        // 2. graphic step
        if(graphics)
            environment.update_graphics();
        cnt++;
    }

    return 0;
}
