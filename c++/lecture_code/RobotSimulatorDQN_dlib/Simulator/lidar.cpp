#include "lidar.h"

Lidar::Lidar(int ray_number, float max_range, float visual_angle, float freq, float mean, float std)
    : Sensor(freq, mean, std), ray_number(ray_number), max_range(max_range), visual_angle(visual_angle)
{
    // init ranges
    ranges.resize(ray_number, 0.0f);
}

Lidar::~Lidar()
{
    for(size_t i = 0; i < ray_number; ++i) {
        if( ray_casts[i])
            delete ray_casts[i];
    }
}

void Lidar::attach_body(b2Body* body, b2Vec2 pos, float angle)
{
    // base class
    Sensor::attach_body(body, pos, angle);

    // setup rays
    ray_casts.reserve(ray_number);
    for(size_t i = 0; i < ray_number; ++i) {
        ray_casts.push_back( new RayCast(Sensor::body->GetWorld()) );
    }

    // set points that describe rays in local coordinate frame
    float delta_phi = 2 * visual_angle / (ray_number - 1);
    ray_points.reserve(ray_number);
    float offset = angle - visual_angle;
    for(size_t i = 0; i < ray_number; ++i) {
        ray_points.push_back( max_range * b2Vec2( cosf(i * delta_phi + offset), sinf(i * delta_phi + offset) ));
    }
}

bool Lidar::update(float dt)
{
    if( Sensor::update(dt) ) {
        // time to update sensor
        b2Vec2 start_vec = Sensor::body->GetWorldPoint(pos);
        for( size_t i = 0; i < ray_number; ++i) {
            b2Vec2 end_vec = Sensor::body->GetWorldPoint(ray_points[i]);

            if( ray_casts[i]->cast(start_vec, end_vec, max_range) )
                ranges[i] = ray_casts[i]->get_data().shortestDist + Sensor::real_gaussian(rng); // add noise in distance
            else
                ranges[i] = max_range;
        }
        return true;
    }
    return false;
}

void Lidar::update_graphics()
{
    for (size_t i = 0; i < ray_number; ++i) {
        ray_casts[i]->update_graphics();
    }
}

void Lidar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for (size_t i = 0; i < ray_number; ++i) {
        target.draw(*ray_casts[i]);
    }
}
