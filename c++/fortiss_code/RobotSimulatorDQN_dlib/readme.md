# C++ Simulator

# Installation
* sudo apt-get install libsfml-dev
* sudo apt-get install libbox2d-dev
* Install [matio](https://github.com/tbeu/matio): follow build instructions
* Install [Dlib](http://dlib.net/): follow build instructions at [build dlib](http://dlib.net/compile.html)

# Build 
Open RobotSimulatorDQN_dlib.pro with [Qtcreator](https://download.qt.io/online/qt5/linux/x64/online_repository/qt.tools.qtcreator/) and build

# Keys
User inpute is given via keyboard. The accelerate learning the gui can be turned of:
* F1 gui on
* F2 gui off
* F3 save data

# Running
* see main.cpp for basic structure
* A log directory can be set in main.cpp to save all training progress
* Training happens in singleagentrobotlearner.cpp, simulation parameters can be changed here

# File IO
* Programm will create log folder in build directory
	- environment_log.mat: mat-file that contains reward, successrate, Q Values of some states
	- net.dat: weights (saved every 2000 episodes)
	- replay.dat: replay buffer (only needed for learning, not execution mode)

# Structure
* config.h: Definition of neuronal network and types (dlib is template based)
	- Change here to edit net structure
* dqn.h: Implementation of DQN and DDQN algorithm
* dqnenvironment.h: connects network state/actions with simulated observations
	- change here to edit reward / states space / action space / reset behaviour
* singleagentrobotlearner.h: learner for a single agent (robot.h)
	- change here to edit episode length, learning rate, epsilon, gamma, replay buffer, saved variables

