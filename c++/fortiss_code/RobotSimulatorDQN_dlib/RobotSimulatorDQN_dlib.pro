QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE *= -O3

QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += box2d
unix: PKGCONFIG += sfml-all
unix: PKGCONFIG += matio

LIBS += -L"/home/dlib-19.15/build"
LIBS += -ldlib

LIBS += -L"/usr/lib"

LIBS += -lopenblas

LIBS += -lcudnn

LIBS += -L"/usr/local/cuda/lib64"
LIBS += -lcublas
LIBS += -lcudart
LIBS += -lcurand
LIBS += -lcusolver

LIBS += -lboost_system
LIBS += -lboost_filesystem

SOURCES += \
    main.cpp \
    DQN/dqn.cpp \
    DQN/dqnenvironment.cpp \
    DQN/replay.cpp \
    DQN/sumtree.cpp \
    Simulator/body.cpp \
    Simulator/config.cpp \
    Simulator/environment.cpp \
    Simulator/lidar.cpp \
    Simulator/matstruct.cpp \
    Simulator/raycast.cpp \
    Simulator/agent/robot.cpp \
    Simulator/staticobstacle.cpp \
    Simulator/tinyxml2.cpp \
    Simulator/agent/car.cpp \
    Simulator/environment/randomenvironment.cpp \
    Simulator/environment/mazeenvironment.cpp \
    Tests/singlerobotsession.cpp \
    Tests/multirobotsession.cpp

HEADERS += \
    session.h \
    DQN/config.h \
    DQN/dqn.h \
    DQN/dqnenvironment.h \
    DQN/dlib/huberloss.h \
    DQN/replay.h \
    DQN/sumtree.h \
    DQN/utilities.h \
    Simulator/agent.h \
    Simulator/agentenvironment.h \
    Simulator/body.h \
    Simulator/config.h \
    Simulator/contactlistener.h \
    Simulator/environment.h \
    Simulator/lidar.h \
    Simulator/matstruct.h \
    Simulator/raycast.h \
    Simulator/agent/robot.h \
    Simulator/sensor.h \
    Simulator/simconstants.h \
    Simulator/staticobstacle.h \
    Simulator/tinyxml2.h \
    Simulator/agent/car.h \
    DQN/dlib/queue.h \
    Simulator/environment/randomenvironment.h \
    Simulator/environment/mazeenvironment.h \
    Tests/singlerobotsession.h \
    Tests/multirobotsession.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
