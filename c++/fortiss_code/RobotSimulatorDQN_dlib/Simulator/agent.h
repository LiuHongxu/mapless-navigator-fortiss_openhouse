#ifndef AGENT_H
#define AGENT_H

#include <Box2D/Box2D.h>

/*
 * Baseclass for all Objects that can act based on a command
 * and observe surrounding envrionment via a sensor
 */
namespace ag
{
    /* encode information of any sensor */
    typedef std::vector<float> SensorReading;

    /* state within the environment */
    struct ModelStates {
        b2Vec2 position;
        float theta;
        b2Vec2 velocity;
        float omega;
    };

    /* comand for agnet */
    struct VelCmd {
        b2Vec2 velocity;
        float omega;
    };

    class Agent
    {
    public:
        Agent() {}
        virtual ~Agent() {}

        /* get the observation made by the agent, return true if new observation made */
        virtual bool observation( SensorReading& sensorreading ) = 0;

        /* returns the evnironment state of the agent, return true if new state */
        virtual bool model_states( ModelStates& modelstate ) = 0;

        /* send a new command to agent */
        virtual void command(const VelCmd& cmd) = 0;
    };
}

#endif // AGENT_H
