#include "config.h"

#include <time.h>

using namespace config;
using namespace tinyxml2;

Config::Config(const std::string& filepath)
    : filepath(filepath)
{
}

int Config::load(const std::string& filepath)
{
    this->filepath = filepath;
    return load();
}

int Config::load()
{
    return read();
}

int Config::save(const std::string& filepath)
{
    this->filepath = filepath;
    return save();
}

int Config::save()
{
    return write();
}

int Config::write()
{
    // clear old data
    xmlDoc.Clear();

    // 1. Create root element
    XMLNode *pRoot = xmlDoc.NewElement("Root");
    xmlDoc.InsertFirstChild(pRoot);

    // 2. Create a header
    time_t t = time(NULL);
    struct tm timeinfo = *localtime(&t);
    XMLElement *pHeaderElement = xmlDoc.NewElement("Date");
    pHeaderElement->SetAttribute("day", timeinfo.tm_mday);
    pHeaderElement->SetAttribute("month", timeinfo.tm_mon + 1);
    pHeaderElement->SetAttribute("year", timeinfo.tm_year + 1900 );
    pHeaderElement->SetAttribute("dateFormat", "dd/mm/yyyy");
    pRoot->InsertEndChild(pHeaderElement);

    // 3. Write config variables
    XMLElement *pConfigList = xmlDoc.NewElement("Configuration");

    for(auto iter = elements.begin(); iter != elements.end(); ++iter ) {
        if( (*iter)->write(&xmlDoc, pConfigList) != XML_SUCCESS )
            break; // error
    }
    pConfigList->SetAttribute("itemCount", (int)elements.size());
    pRoot->InsertEndChild(pConfigList);

    // 4. Save Document
    XMLError eResult = xmlDoc.SaveFile(filepath.c_str());
    return eResult;
}

int Config::read()
{
    // 1. load document
    XMLError eResult = xmlDoc.LoadFile(filepath.c_str());
    if(eResult != XML_SUCCESS) {
        return eResult;
    }
    // get root node
    XMLNode * pRoot = xmlDoc.FirstChild();
    if (pRoot == NULL) {
        return XML_ERROR_FILE_READ_ERROR;
    }

    // 2. extract list elements
    XMLElement * pConfigList = pRoot->FirstChildElement("Configuration");
    XMLElement * pConfigElement = pConfigList->FirstChildElement();

    while(pConfigElement != NULL) {
        pConfigElement = pConfigElement->NextSiblingElement();
    }
    return XML_SUCCESS;
}
