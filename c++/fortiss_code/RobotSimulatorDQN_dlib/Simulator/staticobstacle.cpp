#include "staticobstacle.h"
#include "simconstants.h"

StaticCircle::StaticCircle(const b2Vec2& pos, float radius)
    : StaticObstacle(pos), radius(radius)
{
}

void StaticCircle::construct(b2World* world)
{
    // 1. construct base class
    Body::construct(world);

    // 1. define body shape
    b2CircleShape b2CircleShape;
    b2CircleShape.m_radius = radius;

    // 2. bind body to shape
    b2FixtureDef fixture_def;
    fixture_def.shape = &b2CircleShape;
    fixture_def.density = 1.0f;
    fixture_def.friction = 0.8f;
    fixture_def.isSensor = false;
    Body::b2body->CreateFixture(&fixture_def);

    // 3. sfml visualization
    Body::sfshape = new sf::CircleShape(radius * sim::METER2PIXEL_X);
    Body::sfshape->setPosition( sf::Vector2f(0.0f, 0.0f) );
    Body::sfshape->setOrigin(radius * sim::METER2PIXEL_X, radius * sim::METER2PIXEL_X);
    Body::sfshape->setFillColor(sf::Color::Green);
}



StaticRectangle::StaticRectangle(const b2Vec2& pos, const b2Vec2& size)
    : StaticObstacle(b2Vec2(pos.x, pos.y)), size(size)
{
}

void StaticRectangle::construct(b2World* world)
{
    // 1. construct base class
    Body::construct(world);

    // 2. define body shape
    b2PolygonShape b2PolygonShape;
    b2PolygonShape.SetAsBox(size.x / 2, size.y / 2);

    // 3. bind body to shape
    b2FixtureDef fixture_def;
    fixture_def.shape = &b2PolygonShape;
    fixture_def.density = 1.0f;
    fixture_def.friction = 0.8f;
    fixture_def.isSensor = false;
    b2body->CreateFixture(&fixture_def);

    // 4. sfml visualization
    sfshape = new sf::RectangleShape( sf::Vector2f(sim::METER2PIXEL_X * size.x, sim::METER2PIXEL_Y * size.y));
    sfshape->setPosition( sf::Vector2f(0.0f, 0.0f) );
    sfshape->setFillColor(sf::Color::Green);

    sf::Transformable::setOrigin(0.5f * sf::Vector2f(sim::METER2PIXEL_X * size.x, sim::METER2PIXEL_Y * size.y));
}

