#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>
#include <memory>

#include "tinyxml2.h"

namespace config
{

/* Config Element base */
class Element
{
public:
    Element(const std::string& name, const std::string& description )
        : name(name), description(description) {}
    virtual ~Element() {}

    virtual int read(const tinyxml2::XMLElement* pElement) = 0;
    virtual int write(tinyxml2::XMLDocument* xmlDoc, tinyxml2::XMLElement* pElement) = 0;

protected:
    std::string name;
    std::string description;
};

/* Config Element for spezific type */
template< typename T >
class TypedElement : public Element
{
public:
    TypedElement(const std::string& name, const std::string& description, const T& data)
        : Element(name, description), data(data) {}

    virtual int read(const tinyxml2::XMLElement* pElement) { return -1; }
    virtual int write(tinyxml2::XMLDocument* xmlDoc, tinyxml2::XMLElement* pElement) {
        tinyxml2::XMLElement *pElementAttribute = xmlDoc->NewElement(Element::name.c_str());
        pElementAttribute->SetAttribute("val", data);
        pElement->InsertEndChild(pElementAttribute);
        return tinyxml2::XML_SUCCESS;
    }

protected:
    T data;
};

/* Specilizations for some types */
template<>
int TypedElement<double>::read(const tinyxml2::XMLElement *pElement) {
    Element::name = pElement->Name();
    return pElement->QueryDoubleAttribute("val", &data);
}

template<>
int TypedElement<std::string>::read(const tinyxml2::XMLElement *pElement) {
    const char *str;
    str = pElement->Attribute("val");
    data = std::string(str);
    return tinyxml2::XML_SUCCESS;
}

class Config
{
public:
    Config(const std::string& filepath);

    int load(const std::string& filepath);
    int load();

    int save(const std::string& filepath);
    int save();

private:
    tinyxml2::XMLDocument xmlDoc;
    std::vector<std::shared_ptr<Element>> elements;
    std::string filepath;

    int write();
    int read();
};

}

#endif // CONFIG_H
