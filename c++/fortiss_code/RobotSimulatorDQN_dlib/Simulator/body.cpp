
#include "body.h"
#include "simconstants.h"

Body::Body(b2BodyType bodytype, const b2Vec2& pos, float orientation)
    : b2body(nullptr), sfshape(nullptr)
{
    // define body properties
    body_def.type = bodytype;
    body_def.position.Set(pos.x, pos.y);
    body_def.angle = orientation;
    body_def.linearDamping = sim::LINEARDAMPLING;
    body_def.angularDamping = sim::ANGULARDAMPING;
    body_def.userData = this;

    // update sfml Transformation
    sf::Transformable::setPosition( sim::meter2pixel(pos) );
}

Body::~Body()
{
    if(sfshape)
        delete sfshape;
    b2World* world = b2body->GetWorld();
    world->DestroyBody(b2body);
}

void Body::construct(b2World* world)
{
    b2body = world->CreateBody(&body_def);
}

void Body::reset(const b2Vec2& pos, float orientation)
{
    // set velocites back to zero
    Body::b2body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
    Body::b2body->SetAngularVelocity(0.0f);
    // set to given pose or inital one
    if( pos == b2Vec2(0.0f, 0.0f) && orientation == 0.0f)
       Body::b2body->SetTransform(body_def.position, body_def.angle); // inital position
    else
        Body::b2body->SetTransform(pos, orientation);
}

b2AABB Body::getBoundingRectangle(const b2Vec2& margin, bool global ) const
{
    b2AABB aabb;
    aabb.lowerBound = b2Vec2(FLT_MAX,FLT_MAX);
    aabb.upperBound = b2Vec2(-FLT_MAX,-FLT_MAX);

    // combine all fixtures into single rectangle
    b2Transform t;
    t.SetIdentity();
    b2Fixture* fixture = b2body->GetFixtureList();
    while (fixture != NULL) {
        const b2Shape *shape = fixture->GetShape();
        const int childCount = shape->GetChildCount();
        for (int child = 0; child < childCount; ++child) {
            b2AABB shapeAABB;
            shape->ComputeAABB(&shapeAABB, t, child);
            shapeAABB.lowerBound = shapeAABB.lowerBound;
            shapeAABB.upperBound = shapeAABB.upperBound;
            aabb.Combine(shapeAABB);
        }
        fixture = fixture->GetNext();
    }

    if(global) {
        aabb.lowerBound -= margin;
        aabb.upperBound += margin;
        aabb.lowerBound += b2body->GetPosition();
        aabb.upperBound += b2body->GetPosition();
    }
    else {
        aabb.lowerBound -= margin;
        aabb.upperBound += margin;
    }
    return aabb;
}

b2Vec2 Body::getPosition() const
{
    return b2body->GetPosition();
}

void Body::update_graphics()
{
    // set transformation of sfshape
    // Wtf sign of b2 angle differs form sfml angle
    sf::Transformable::setPosition( sim::meter2pixel(b2body->GetPosition()) );
    sf::Transformable::setRotation(-b2body->GetAngle() * sim::RAD2DEG);
}

void Body::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    // draw the vertex array (components of this class need a transformation!)
    states.transform *= getTransform();
    target.draw(*sfshape, states);
}

float Body::normal_relative_angle( float angle ) {

    while( angle > sim::PI)
        angle -= sim::PI_2;
    while( angle < -sim::PI)
        angle += sim::PI_2;
    return angle;
}

