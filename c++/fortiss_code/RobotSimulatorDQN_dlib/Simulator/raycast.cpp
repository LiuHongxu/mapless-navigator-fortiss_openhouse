#include "raycast.h"
#include "simconstants.h"

RayCast::RayCast(b2World *world)
    : world(world)
{
    line.resize(2);
    line.setPrimitiveType(sf::Lines);
    line[0].color = sf::Color::Red;
    line[1].color = sf::Color::Red;
}

RayCast::~RayCast()
{
}

bool RayCast::cast(const b2Vec2& start_vec, float angle, float distance, const Body *start_body)
{
    // 1. fill data struct
    data.start = start_vec;
    data.end = distance * b2Vec2(start_vec.x + cosf(angle), start_vec.y + sinf(angle));
    data.direction = angle;
    data.distance = distance;
    data.startObject = start_body;
    data.closestObject = nullptr;
    data.shortestDist = -1;

    // 2. do raycast
    world->RayCast(this, data.start, data.end);

    return data.closestObject != nullptr;
}

bool RayCast::cast(const b2Vec2& start_vec, const b2Vec2& end_vec, float distance, const Body *start_body)
{
    // 1. fill data struct
    data.start = start_vec;
    data.end = end_vec;
    data.direction = -1.0f;
    data.distance = distance;
    data.startObject = start_body;
    data.closestObject = nullptr;
    data.shortestDist = -1;

    // 2. do raycast
    world->RayCast(this, data.start, data.end);

    return data.closestObject != nullptr;
}

void RayCast::update_graphics(b2Vec2 end)
{
    // compute pixel start/end point
    line[0].position = sim::meter2pixel(data.start);
    line[1].position = sim::meter2pixel(end);

    /*if(data.closestObject)
        line[1].position = sim::meter2pixel(data.hitPoint);
    else
        line[1].position = sim::meter2pixel(data.end);*/
}

void RayCast::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    // draw the vertex array
    target.draw(line, states);
}

float32 RayCast::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
{
    // some weird behaviour, skip that
    if (fraction <= 0)
        return 1;
    // ignor sensors, no collision here
    if (fixture->IsSensor())
        return -1;
    // ignore these as well
    if(fixture->GetFilterData().categoryBits == 0x0002)
        return -1;

    // retrieve the class this fixture belongs to
    Body* hitobj = static_cast<Body*>(fixture->GetBody()->GetUserData());

    // save information
    data.shortestDist = fraction * data.distance;
    data.closestObject = hitobj;
    data.hitPoint = point;

    // only search for nearest object (non zero return)
    return fraction;
}
