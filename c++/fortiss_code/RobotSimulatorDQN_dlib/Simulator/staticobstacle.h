#ifndef STATICOBSTACLE_H
#define STATICOBSTACLE_H

#include "body.h"

class StaticObstacle : public Body
{
public:
    StaticObstacle(const b2Vec2& pos)
        : Body(b2_staticBody, pos) {}
    virtual ~StaticObstacle() {}

    /* contruct robot object */
    virtual void construct(b2World* world) = 0;

    void update(float dt) { /* no change in static object */ }

    void reset() {/* no reset for static object */ }

    virtual void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) = 0;
    virtual void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) = 0;
};

class StaticCircle : public StaticObstacle
{
public:
    StaticCircle(const b2Vec2& pos, float radius);

    void construct(b2World* world);

    void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) { }
    void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) { }

private:
    float radius;
};

class StaticRectangle : public StaticObstacle
{
public:
    StaticRectangle(const b2Vec2& pos, const b2Vec2& size);

    void construct(b2World* world);

    void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) { }
    void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) { }

private:
    b2Vec2 size;
};

#endif // STATICOBSTACLE_H
