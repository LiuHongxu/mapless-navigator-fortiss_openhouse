#ifndef CAR_H
#define CAR_H

#include "Simulator/body.h"
#include "Simulator/lidar.h"
#include "Simulator/agent.h"

class Tire : public Body
{
public:
    Tire(const b2Vec2& pos = b2Vec2(0.0f, 0.0f), const b2Vec2& size = b2Vec2(0.05f, 0.1f), float max_lateral_impuls = 10.0f);
    virtual ~Tire();

    /* contruct robot object */
    void construct(b2World* world);

    /* update sensor */
    void update(float dt) { }

    /* simulate friction with ground */
    void update_friction();
    /* apply linear velocity to tire */
    void apply_lin_velo(float vel);

    void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) { }
    void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) { }

private:
    b2Vec2 size;
    float max_lateral_impuls;

    // manipulate box2d body via impulses
    b2Vec2 get_lateral_velocity();
    b2Vec2 get_forward_velocity();
};

class Car : public Body, public ag::Agent
{
public:
    Car(const b2Vec2& pos);
    virtual ~Car();

    /* contruct car object */
    void construct(b2World* world);

    /* retruns the observation the agent has access */
    bool observation( ag::SensorReading& sensorreading );
    /* returns the evnironment state of the agent (pos, velo, orientation, omega)  */
    bool model_states( ag::ModelStates& modelstate );
    /* send a new command to robot */
    void command(const ag::VelCmd &cmd);

    /* reset sensors to inital state */
    void reset(const b2Vec2& pos = b2Vec2(0.0f, 0.0f) , float orientation = 0.0f);
    /* update sensor */
    void update(float dt);
    /* update graphical content */
    void update_graphics();

    /* set linear car velocity */
    void set_lin_velo(float vel);
    /* set steering angle of front weels */
    void set_steering_angle(float angle);

    void beginContact(Body* entety, b2Fixture* fixture,  b2Contact* contact) { }
    void endContact(Body* entety, b2Fixture* fixture, b2Contact* contact) { }

private:
    std::vector<Tire*> tires;
    b2RevoluteJoint *fl_joint, *fr_joint;

    // sensors
    Lidar lidar;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // CAR_H
