#include "car.h"
#include "Simulator/simconstants.h"
#include <iostream>

using namespace ag;

Tire::Tire(const b2Vec2& pos, const b2Vec2& size, float max_lateral_impuls)
    : Body(b2_dynamicBody, pos), size(size), max_lateral_impuls(max_lateral_impuls)
{
}

Tire::~Tire()
{
}

void Tire::construct(b2World* world)
{
    // 1. base class construction
    Body::construct(world);

    // 2. define body shape
    b2PolygonShape polygonShape;
    polygonShape.SetAsBox( size.x / 2.0f, size.y / 2.0f );

    // 3. bind body to shape
    b2FixtureDef fixture_def;
    fixture_def.shape = &polygonShape;
    fixture_def.isSensor = false;
    fixture_def.density = 1.0f;
    fixture_def.friction = 0.8f;
    fixture_def.filter.categoryBits = 0x0002;
    Body::b2body->CreateFixture(&fixture_def);

    // 4. sfml visualization
    Body::sfshape = new sf::RectangleShape(sf::Vector2f(size.x * sim::METER2PIXEL_X, size.y * sim::METER2PIXEL_X));
    Body::sfshape->setPosition(0.0f, 0.0f);
    Body::sfshape->setFillColor(sf::Color::Green);

    sf::Transformable::setOrigin(0.5f * sf::Vector2f(size.x * sim::METER2PIXEL_X, size.y * sim::METER2PIXEL_Y));
}

b2Vec2 Tire::get_lateral_velocity()
{
    // get velocity lateral to body
    b2Vec2 currentRightNormal = Body::b2body->GetWorldVector( b2Vec2(1,0) );
    return b2Dot( currentRightNormal, Body::b2body->GetLinearVelocity() ) * currentRightNormal;
}

b2Vec2 Tire::get_forward_velocity()
{
    // get velocity lateral to body
    b2Vec2 currentForwardNormal = Body::b2body->GetWorldVector( b2Vec2(0,1) );
    return b2Dot( currentForwardNormal, Body::b2body->GetLinearVelocity() ) * currentForwardNormal;
}

void Tire::update_friction() {
    //lateral linear velocity (cancle lateral impuls since tire only moves forward or backward)
    b2Vec2 impulse = Body::b2body->GetMass() * -get_lateral_velocity();
    if ( impulse.Length() > max_lateral_impuls )
        impulse *= max_lateral_impuls / impulse.Length();
    Body::b2body->ApplyLinearImpulse( impulse, Body::b2body->GetWorldCenter(), true );

    // angular velocity (friction of spinning Tire)
    Body::b2body->ApplyAngularImpulse( 0.1f * Body::b2body->GetInertia() * -Body::b2body->GetAngularVelocity(), true );

    // forward linear velocity (friction with ground)
    b2Vec2 forward_velocity = get_forward_velocity();
    Body::b2body->ApplyForce( -0.01f * forward_velocity, Body::b2body->GetWorldCenter(), true );
}

void Tire::apply_lin_velo(float speed)
{
    // current speed in forward direction
    b2Vec2 forward_normal = Body::b2body->GetWorldVector( b2Vec2(0,1) );
    float speed_forward = b2Dot( get_forward_velocity(), forward_normal );

    // delta in speed
    float d_vel = speed - speed_forward;

    b2Vec2 impuls = Body::b2body->GetMass() * d_vel * forward_normal;
    Body::b2body->ApplyLinearImpulse(impuls, Body::b2body->GetWorldCenter(), true);
}

// ----------------------------------------------------------------------------------------------------------------------

Car::Car(const b2Vec2& pos)
    : Body(b2_dynamicBody, pos), lidar(18, 2.0f, 2 * b2_pi / 3, 3.0f, 0.0f, 0.0f )
{
}

Car::~Car()
{
    for(size_t i = 0; i < tires.size(); ++i)
        delete tires[i];
}

/* contruct robot object */
void Car::construct(b2World* world)
{
    // 1. base class construction
    Body::construct(world);

    // 2. define body shape
    b2Vec2 vertices[8];
    vertices[0].Set( 0.075f,   0);
    vertices[1].Set( 0.15f, 0.125f);
    vertices[2].Set( 0.14f, 0.275f);
    vertices[3].Set( 0.05f,  0.5f);
    vertices[4].Set(-0.05f,  0.5f);
    vertices[5].Set(-0.14f, 0.275f);
    vertices[6].Set(-0.15f, 0.125f);
    vertices[7].Set(-0.075f,   0);
    b2PolygonShape polygonShape;
    polygonShape.Set( vertices, 8 );

    // 3. bind body to shape
    b2FixtureDef fixture_def;
    fixture_def.shape = &polygonShape;
    fixture_def.isSensor = false;
    fixture_def.density = 1.0f;
    fixture_def.friction = 0.8f;
    Body::b2body->CreateFixture(&fixture_def);

    // 4. sfml visualization
    sf::ConvexShape convex_shape(8);
    for(size_t i = 0; i < convex_shape.getPointCount(); ++i)
        convex_shape.setPoint(i, sf::Vector2f(vertices[i].x * sim::METER2PIXEL_X, -vertices[i].y * sim::METER2PIXEL_Y));

    // 4. sfml visualization
    Body::sfshape = new sf::ConvexShape(convex_shape);
    Body::sfshape->setPosition(0.0f, 0.0f);
    Body::sfshape->setFillColor(sf::Color::Green);

    // 5. add tires

    //prepare common joint parameters
    b2RevoluteJointDef jointDef;
    jointDef.bodyA = Body::b2body;
    jointDef.enableLimit = true;
    jointDef.lowerAngle = 0;
    jointDef.upperAngle = 0;
    jointDef.localAnchorB.SetZero(); //center of tire

    //back left tire
    Tire* tire = new Tire();
    tire->construct(world);
    jointDef.bodyB = tire->get_body();
    jointDef.localAnchorA.Set( -0.15f, 0.0375f );
    world->CreateJoint( &jointDef );
    tires.push_back(tire);

    //back right tire
    tire = new Tire();
    tire->construct(world);
    jointDef.bodyB = tire->get_body();
    jointDef.localAnchorA.Set( 0.15f, 0.0375f );
    world->CreateJoint( &jointDef );
    tires.push_back(tire);

    //front left tire
    tire = new Tire();
    tire->construct(world);
    jointDef.bodyB = tire->get_body();
    jointDef.localAnchorA.Set( -0.15f, 0.425f );
    fl_joint = static_cast<b2RevoluteJoint*>(world->CreateJoint(&jointDef));
    tires.push_back(tire);

    //front right tire
    tire = new Tire();
    tire->construct(world);
    jointDef.bodyB = tire->get_body();
    jointDef.localAnchorA.Set( 0.15f, 0.425f );
    fr_joint = static_cast<b2RevoluteJoint*>(world->CreateJoint(&jointDef));
    tires.push_back(tire);

    // add lidar
    lidar.attach_body(Body::b2body, b2Vec2(0.0f, 0.4f), b2_pi / 2);
}

/* retruns the observation the agent has access */
bool Car::observation( ag::SensorReading& sensorreading )
{
    bool available = lidar.is_avialable();
    sensorreading = lidar.get_ranges();
    return available;
}
/* returns the evnironment state of the agent (pos, velo, orientation, omega)  */
/* important position wrt world at back axis of car, velocity wrt world */
bool Car::model_states( ag::ModelStates& modelstate )
{
    modelstate.position = Body::b2body->GetPosition();
    modelstate.theta = Body::normal_relative_angle( Body::b2body->GetAngle() );
    modelstate.velocity = Body::b2body->GetLinearVelocity();
    modelstate.omega = Body::b2body->GetAngularVelocity();

    /*printf("X=%4.2f Y=%4.2f Vx=%4.2f Vy=%4.2f w=%4.2f\n", modelstate.position.x, modelstate.position.y,
           modelstate.velocity.x, modelstate.velocity.y, modelstate.omega);*/
    return true;
}

/* send a new command to robot */
void Car::command(const VelCmd& cmd)
{
    set_lin_velo(cmd.velocity.x);   // lin speed
    set_steering_angle(cmd.omega);  // steering angle
}

/* reset sensors to inital state */
void Car::reset(const b2Vec2& pos, float orientation)
{
    // Base class
    Body::reset(pos, orientation);
    // reset robot specific stuff
    lidar.reset();
}

/* update sensor */
void Car::update(float dt)
{
    lidar.update(dt);
}

void Car::update_graphics()
{
    Body::update_graphics();
    for(size_t i = 0; i < tires.size(); ++i) {
        tires[i]->update_graphics();
    }
    lidar.update_graphics();
}

void Car::set_lin_velo(float vel)
{
    for(size_t i = 0; i < tires.size(); ++i) {
        tires[i]->apply_lin_velo(vel);
        tires[i]->update_friction();
    }
}

void Car::set_steering_angle(float angle)
{
    float turnSpeedPerSec = 320 * sim::DEG2RAD; //from lock to lock in 0.25 sec
    float turnPerTimeStep = turnSpeedPerSec / 100.0f;

    float current_angle = fl_joint->GetJointAngle();
    float d_angle = angle - current_angle;
    d_angle = b2Clamp( d_angle, -turnPerTimeStep, turnPerTimeStep );

    float newAngle = current_angle + d_angle;
    fl_joint->SetLimits( newAngle, newAngle );
    fr_joint->SetLimits( newAngle, newAngle );
}

void Car::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    Body::draw(target, states);
    for(size_t i = 0; i < tires.size(); ++i) {
        target.draw(*tires[i]);
    }
    target.draw(lidar);
}
