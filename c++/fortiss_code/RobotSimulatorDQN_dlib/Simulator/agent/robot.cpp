#include "robot.h"
#include "Simulator/simconstants.h"

#include <iostream>

using namespace ag;

Robot::Robot(const b2Vec2 &pos, float orientation, float radius, float pos_std, float orient_std, float lin_vel_std, float ang_vel_std)
    : Body(b2_dynamicBody, pos, orientation),
      radius(radius), lidar(18, 2.0f, b2_pi, 3.0f, 0.0f, 0.0f ) // 5 rays, 6m max dist, +- 45 deg angle, 5Hz, 0cm mean noise, +-3 cm std noise
{
    // init random generators
    rng = std::mt19937(rd());
    real_gaussian_pos = std::normal_distribution<float>(0.0f, pos_std);
    real_gaussian_orient = std::normal_distribution<float>(0.0f, orient_std);
    real_gaussian_lin_vel = std::normal_distribution<float>(0.0f, lin_vel_std);
    real_gaussian_ang_vel = std::normal_distribution<float>(0.0f, ang_vel_std);
}

Robot::~Robot()
{
}

void Robot::construct(b2World* world)
{
    // 1. base class construction
    Body::construct(world);

    // 2. define body shape
    b2CircleShape b2CircleShape;
    b2CircleShape.m_radius = radius;

    // 3. bind body to shape
    b2FixtureDef fixture_def;
    fixture_def.shape = &b2CircleShape;
    fixture_def.isSensor = false;
    fixture_def.density = 1.0f;
    fixture_def.friction = 0.8f;
    Body::b2body->CreateFixture(&fixture_def);

    // 4. sfml visualization
    Body::sfshape = new sf::CircleShape(radius * sim::METER2PIXEL_X);
    Body::sfshape->setPosition(0.0f, 0.0f);
    Body::sfshape->setOrigin(radius * sim::METER2PIXEL_X, radius * sim::METER2PIXEL_X);
    Body::sfshape->setFillColor(sf::Color::Green);

    // 5. attach lidar sensor to robot frame
    lidar.attach_body(Body::b2body);
}


/* retruns the observation the agent has access */
bool Robot::observation( ag::SensorReading& sensorreading )
{
    bool available = lidar.is_avialable();
    sensorreading = lidar.get_ranges();
    return available;
}
/* returns the evnironment state of the agent (pos, velo, orientation, omega)  */
bool Robot::model_states( ag::ModelStates& modelstate )
{
    modelstate.position = Body::b2body->GetPosition();
    modelstate.theta = Body::normal_relative_angle( Body::b2body->GetAngle() );
    modelstate.velocity = Body::b2body->GetLinearVelocity();
    modelstate.omega = Body::b2body->GetAngularVelocity();
    return true;
}

/* send a new command to robot */
void Robot::command(const VelCmd& cmd)
{
    apply_lin_velo(cmd.velocity);
    apply_ang_velo(cmd.omega);
}

/* reset sensors to inital state */
void Robot::reset(const b2Vec2& pos, float orientation)
{
    // Base class
    Body::reset(pos, orientation);
    // reset robot specific stuff
    lidar.reset();
}

void Robot::update(float dt) {
    lidar.update(dt);
}

void Robot::apply_lin_velo(const b2Vec2& vel)
{
    // use impuls to generate change in linear velocities ( add gaussian noise to local velocity )
    b2Vec2 d_vel = sim::rotate(vel, Body::b2body->GetAngle()) - Body::b2body->GetLinearVelocity();
    b2Vec2 impuls = Body::b2body->GetMass() * ( d_vel + b2Vec2( real_gaussian_lin_vel(rng), real_gaussian_lin_vel(rng) ));
    Body::b2body->ApplyLinearImpulse(impuls, Body::b2body->GetWorldCenter(), true);
}

void Robot::apply_ang_velo(float omega)
{
    // use impuls to generate change in angular velo ( add gaussian noise to omega )
    float impuls = Body::b2body->GetInertia() * ( omega - Body::b2body->GetAngularVelocity() + real_gaussian_ang_vel(rng) );
    Body::b2body->ApplyAngularImpulse(impuls, true);
}

void Robot::update_graphics()
{
    Body::update_graphics();
    lidar.update_graphics();
}

void Robot::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    Body::draw(target, states);
    target.draw(lidar);
}
