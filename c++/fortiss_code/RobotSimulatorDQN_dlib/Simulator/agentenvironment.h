#ifndef AGENTENVIRONMENT_H
#define AGENTENVIRONMENT_H

#include "environment.h"
#include "agent.h"

typedef size_t action_type;
typedef float reward_type;

class EnvState
{
public:
    reward_type reward;
    bool done;
    bool success;
};

template< typename T >
class TypedEnvState : public EnvState
{
public:
    std::vector<T> state;
};

/*
 * Base class for any Agent Envrionment
 * Describes how Agent observes envrionment (agents perspective) and executes actions
 */
class AgentEnvironment {
public:
    AgentEnvironment(Environment *env, ag::Agent* agent, int n_actions, int dim_states)
        : env(env), agent(agent), n_actions(n_actions), dim_states(dim_states) { }

    virtual ~AgentEnvironment() { }

    /* return dimention of action space */
    int get_n_actions() const { return n_actions; }

    /* return dimension of state space */
    int get_dim_states() const { return dim_states; }

    /* perform one step (apply action) */
    virtual void execute(action_type action) = 0;

    /* returns the new observations */
    virtual bool observe(EnvState& env_state) = 0;

    /* reset environment and return to inital observation */
    virtual void reset(b2AABB* clear_area = nullptr, bool full_reset = true) {
        if(full_reset)
            env->reset(clear_area);  // reset everything
        else
            env->reset(clear_area, dynamic_cast<Body*>(agent)); // only reset this agent
    }

    /* get agent of this environment */
    ag::Agent* get_agent() { return agent; }

protected:
    Environment* env;
    ag::Agent* agent;

private:
    int n_actions;  // number of actions
    int dim_states; // dimension of state vector
};

#endif // AGENTENVIRONMENT_H
