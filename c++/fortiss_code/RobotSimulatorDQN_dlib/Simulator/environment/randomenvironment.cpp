#include "randomenvironment.h"

RandomEnvironment::RandomEnvironment(const b2Vec2& gravity, float loop_frequency, float step_size )
    : Environment(gravity, loop_frequency, step_size)
{
    rng = std::mt19937(rd());
    real_uni_x = std::uniform_real_distribution<float>(0.3f, sim::WORLDSIZE_X - 0.3f);
    real_uni_y = std::uniform_real_distribution<float>(0.3f, sim::WORLDSIZE_Y - 0.3f);
    real_uni_theta = std::uniform_real_distribution<float>(-sim::PI, sim::PI);
}

 /* reset back to inital state
  * clear_area will never be occupied by an object */
void RandomEnvironment::on_reset(std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, b2AABB* clear_area, Body* body)
{
    b2Vec2 rand_pos;
    float rand_orient;

    // if body is nullptr: reset everything, start with by randomly placing bodies ( if they overlap: replace them again )
    if(!body)
    {
        for( auto it = start; it != end; ++it) {
            // set new random position
            int cnt = 0;
            rand_pos.x = real_uni_x(rng); rand_pos.y = real_uni_y(rng);
            rand_orient = real_uni_theta(rng);
            (*it)->reset( rand_pos, rand_orient );

            // check if positon overlapps with something else (if so do resample)
            b2AABB rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
            if(!clear_area) {
                while(is_colliding( rect, start, it) && cnt < 1000 ) { // check for overlapping
                    rand_pos.x = real_uni_x(rng); rand_pos.y = real_uni_y(rng);
                    rand_orient = real_uni_theta(rng);
                    (*it)->reset( rand_pos, rand_orient );
                    rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                    cnt++;
                }
            }
            else {
                while(( is_colliding( rect, start, it) || sim::intersect(rect, *clear_area) ) && cnt < 1000 ) { // check for overlapping + clear area
                    rand_pos.x = real_uni_x(rng); rand_pos.y = real_uni_y(rng);
                    rand_orient = real_uni_theta(rng);
                    (*it)->reset( rand_pos, rand_orient );
                    rect = (*it)->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                    cnt++;
                }
            }
        }
    }
    // only reset one body, keep the rest at its position
    else
    {
        // get position of body
        int body_idx = std::distance(start, std::find(start, end, body));

        // set new random position
        int cnt = 0;
        rand_pos.x = real_uni_x(rng);
        rand_pos.y = real_uni_y(rng);
        rand_orient = real_uni_theta(rng);
        body->reset( rand_pos, rand_orient );

        b2AABB rect = body->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
        if(!clear_area) {
            while( is_colliding(rect, start, end, body_idx) && cnt < 1000 ) {
                rand_pos.x = real_uni_x(rng); rand_pos.y = real_uni_y(rng);
                rand_orient = real_uni_theta(rng);
                body->reset( rand_pos, rand_orient );
                rect = body->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                cnt++;
            }
        }
        else {
            while(( is_colliding( rect, start, end, body_idx) || sim::intersect(rect, *clear_area) ) && cnt < 1000 ) {
                rand_pos.x = real_uni_x(rng); rand_pos.y = real_uni_y(rng);
                rand_orient = real_uni_theta(rng);
                body->reset( rand_pos, rand_orient );
                rect = body->getBoundingRectangle(b2Vec2(0.3f, 0.3f), true);
                cnt++;
            }
        }
    }
}

bool RandomEnvironment::is_colliding(const b2AABB& rectA, std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, int skip, const b2Vec2 margin)
{
    int i = 0;
    for(auto it = start; it != end; ++it) {
        if( i != skip ) {
            b2AABB rectB = (*it)->getBoundingRectangle(margin, true);
            if(sim::intersect(rectA, rectB)) {
                return true; // overlapping
            }
        }
        ++i;
    }
    return false;
}
