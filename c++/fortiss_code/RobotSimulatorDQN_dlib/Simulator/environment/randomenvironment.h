#ifndef RANDOMENVIRONMENT_H
#define RANDOMENVIRONMENT_H

#include "Simulator/environment.h"


class RandomEnvironment : public Environment
{
public:
    RandomEnvironment(const b2Vec2& gravity = b2Vec2(0.0f, 0.0f), float loop_frequency = -1.0f, float step_size = 0.05f );

     /* reset back to inital state
      * clear_area will never be occupied by an object */
     void on_reset(std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, b2AABB* clear_area = nullptr, Body* body = nullptr);

private:
    std::random_device rd;  //  (seed) engine
    std::mt19937 rng;       // random-number engine
    std::uniform_real_distribution<float> real_uni_x, real_uni_y, real_uni_theta;

    // check Body* or b2AABB against every other body between start and end, except for skip index
    bool is_colliding(const b2AABB& rectA, std::vector<Body*>::iterator start, std::vector<Body*>::iterator end, int skip = -1,
                      const b2Vec2 margin = b2Vec2(0.1f, 0.1f));
};

#endif // RANDOMENVIRONMENT_H
