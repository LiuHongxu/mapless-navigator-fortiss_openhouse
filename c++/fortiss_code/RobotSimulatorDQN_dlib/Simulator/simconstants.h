#ifndef SIMCONSTANTS_H
#define SIMCONSTANTS_H

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <numeric>

namespace sim
{
    /* constants */
    const float PI = 3.14159265359f;
    const float PI_2 = 2.0f * PI;
    const float RAD2DEG = 57.2957795131f;
    const float DEG2RAD = 0.01745329251f;

    /* simulation world */
    const float LINEARDAMPLING = 1.5;
    const float ANGULARDAMPING = 1.5;

    const int WINDOWSIZE_X = 1000;
    const int WINDOWSIZE_Y = 1000;

    const float WORLDSIZE_X = 5.0;
    const float WORLDSIZE_Y = 5.0;
    const float MAXWORLDDIST = sqrtf( WORLDSIZE_X * WORLDSIZE_X + WORLDSIZE_Y * WORLDSIZE_Y );

    /* converter functions */
    const float PIXEL2METER_X = (float)WORLDSIZE_X / (float)WINDOWSIZE_X;
    const float PIXEL2METER_Y = (float)WORLDSIZE_Y / (float)WINDOWSIZE_Y;

    const float METER2PIXEL_X = (float)WINDOWSIZE_X / (float)WORLDSIZE_X;
    const float METER2PIXEL_Y = (float)WINDOWSIZE_Y / (float)WORLDSIZE_Y;

    inline b2Vec2 pixel2meter(const sf::Vector2f& vec) {
        return b2Vec2(vec.x * PIXEL2METER_Y, WORLDSIZE_Y - vec.y * PIXEL2METER_Y);
    }

    inline sf::Vector2f meter2pixel(const b2Vec2& vec) {
        return sf::Vector2f(vec.x * METER2PIXEL_X, WINDOWSIZE_Y - vec.y * METER2PIXEL_Y);
    }

    inline b2Vec2 rotate(const b2Vec2& vec, float a) {
        return b2Vec2(vec.x * cosf(a) - vec.y * sinf(a), vec.x * sinf(a) + vec.y * cosf(a));
    }

    inline bool intersect(const b2AABB& rectA, const b2AABB& rectB) {
        return rectA.lowerBound.x < rectB.upperBound.x &&
               rectA.upperBound.x > rectB.lowerBound.x &&
               rectA.lowerBound.y < rectB.upperBound.y &&
               rectA.upperBound.y > rectB.lowerBound.y;
    }

    template <typename T> int sgn(T val) {
        return (T(0) < val) - (val < T(0));
    }

    template <typename T> T mean(const std::vector<T>& v) {
        return std::accumulate(v.begin(), v.end(), (T)0.0) / (T)v.size();
    }
}

#endif // SIMCONSTANTS_H
