#include "dqn.h"
#include <fstream>
#include "utilities.h"

#include <iostream>

using namespace dqn;
using namespace dlib;

/* ----------------------------------------------------------------------------------------------------------
 * Deep Q-Network
 * ----------------------------------------------------------------------------------------------------------*/

DQN::DQN(Replay &replay, size_t state_size, size_t action_size, float epsilon, float alpha, float gamma, size_t update_freq)
    : trainer(net, adam(0.0005f, 0.9f, 0.999f)), replay(replay), state_size(state_size), action_size(action_size),
      epsilon(epsilon), alpha(alpha), gamma(gamma), update_freq(update_freq), cnt(0)
{
    rng = std::mt19937(rd());
    int_uni = std::uniform_int_distribution<int>(0, action_size-1);
    real_uni = std::uniform_real_distribution<double>(0, 1);

    // check if priorized replay is used
    use_per = dynamic_cast<PriorizedReplay*>(&replay) != nullptr;

    trainer.be_verbose();
    trainer.set_learning_rate_shrink_factor(1.0);
    trainer.set_learning_rate( alpha );
    trainer.set_mini_batch_size( replay.get_batch_size() );
    std::cout << net << std::endl;
}

DQN::~DQN()
{
}

float DQN::learn(const std::vector<scalar_t> &state, const action_t& action, const scalar_t& reward, const std::vector<scalar_t> &next_state, bool isfinished)
{
    Matrix state_ = mat(state);
    Matrix next_state_ = mat(next_state);
    cnt++;

    // save transition
    if(!replay.is_filled()) {
        // replay buffer not filled, use reward as replacement of td error
        replay.push_back(state_, action, reward, next_state_, reward, isfinished);
    }
    else {
        // replay buffer filled, per is used, compute correct td error for new sample
        if(use_per)
            replay.push_back(state_, action, reward, next_state_, td_error(state_, action, reward, next_state_, isfinished), isfinished);
        // replay buffer filled, per is not used, set td to zero
        else
            replay.push_back(state_, action, reward, next_state_, 0.0f, isfinished);

        // run experience replay if buffer is filled
        if( cnt % update_freq == 0)
            return experience_replay();
    }
    return 0.0f;
}

action_t DQN::policy(const std::vector<scalar_t>& state)
{
    Matrix state_ = mat(state);
    Matrix qValues = net(state_);
    return index_of_max( qValues );
}

scalar_t DQN::getQ(const std::vector<scalar_t>& state, int action)
{
    Matrix state_ = mat(state);
    Matrix qValues = net(state_);
    return qValues(action);
}

std::vector<scalar_t> DQN::getQs(const std::vector<scalar_t> &state)
{
    Matrix state_ = mat(state);
    Matrix qValues = net(state_);
    return std::vector<scalar_t>(qValues.begin(), qValues.end());
}

action_t DQN::generate_action(const std::vector<scalar_t> &state)
{
    action_t action;
    // choose eps greedy action
    if( real_uni(rng) > epsilon ) {
        // use prediction
        Matrix state_ = mat(state);
        Matrix qValues = net( state_ );
        action = index_of_max( qValues );
    } else {
        // select complety random
        action = int_uni(rng);
    }
    return action;
}

float DQN::experience_replay()
{
    // 1. get samples form history buffer
    std::vector<float> weights; std::vector<int> indices;
    const Memory& samples = replay.sample(weights, indices);

    // 3. compute target values
    auto targets = qtarget(samples, indices, gamma);

    // 4. update network
    trainer.train_one_step(samples.states, targets);

    return (float)trainer.get_average_loss();
}

Batch DQN::qtarget(const Memory& samples, std::vector<int>& indicies, float gamma)
{
    // 1. get references
    auto& rewards = samples.rewards;
    auto& actions = samples.actions;
    auto& isfinished = samples.isfinished;

    // 2. evaluate neural net at all states / next states
    auto qValues = net( samples.states );                     // qValues of state
    auto next_qValues = net( samples.next_states );            // qValues of next state

    // deep cpy of qValues
    Batch targets = qValues;
    float max_value;

    // 3. update bellman equation
    // Q(s,a) = r + gamma * argmax_a Q(s', a)
    for(unsigned int i = 0; i < rewards.size(); ++i ) {
        if(!isfinished[i]) {
            max_value = max( next_qValues[i] );
            targets[i](actions[i]) = rewards[i] + gamma * max_value;
        }
        else
            targets[i](actions[i]) = rewards[i];

        // 4. update the replay buffer td error
        if(use_per)
            replay.update(indicies[i], qValues[i](actions[i]) - targets[i](actions[i]));
    }
    return targets;
}

scalar_t DQN::td_error(const Matrix& state, const action_t& action, const scalar_t& reward, const Matrix& next_state, bool isfinished)
{
    auto qValues = net( state );
    auto next_qValues = net( next_state );
    if(isfinished)
        return qValues(0, action) - reward;
    else
        return qValues(0, action) - (reward + gamma * max( next_qValues ));
}

/* ----------------------------------------------------------------------------------------------------------
 * Double Deep Q-Network
 * ----------------------------------------------------------------------------------------------------------*/

DDQN::DDQN(Replay& replay, size_t state_size, size_t action_size, size_t swap_cnt, float epsilon, float alpha, float gamma )
    : DQN(replay, state_size, action_size, epsilon, alpha, gamma ), swap_cnt(swap_cnt)
{
    target_net.subnet() = net.subnet();
}

DDQN::~DDQN()
{
}

float DDQN::experience_replay()
{
    // 1. call base class experience replay
    float loss = DQN::experience_replay();

    // 2. alternate between network and target network (every swap_cnt training steps)
    if(cnt % swap_cnt) {
        target_net.subnet() = net.subnet();
    }

    return loss;
}

Batch DDQN::qtarget(const Memory& samples, std::vector<int>& indicies, float gamma)
{
    // 1. get references
    auto& rewards = samples.rewards;
    auto& actions = samples.actions;
    auto& isfinished = samples.isfinished;

    // 2. evaluate neural net at all states / next states
    auto qValues = net( samples.states );                     // qValues of state
    auto next_qValues = net( samples.next_states );            // qValues of next state
    auto next_qValues_t = target_net( samples.next_states );

    // deep cpy of qValues
    Batch targets = qValues;
    int max_inx;

    // 3. update bellman equation
    // Q(s,a) = r + gamma * argmax_a Q(s', a)
    for(unsigned int i = 0; i < rewards.size(); ++i ) {
        if(!isfinished[i]) {
            max_inx = index_of_max( next_qValues[i] );
            targets[i](actions[i]) = rewards[i] + gamma * next_qValues_t[i](max_inx);
        }
        else
            targets[i](actions[i]) = rewards[i];

        // 4. update the replay buffer td error
        if(use_per)
            replay.update(indicies[i], qValues[i](actions[i]) - targets[i](actions[i]));
    }
    return targets;
}

scalar_t DDQN::td_error(const Matrix& state, const action_t& action, const scalar_t& reward, const Matrix& next_state, bool isfinished)
{
    auto qValues = net( state );                     // qValues of state
    auto next_qValues = net( next_state );            // qValues of next state
    auto next_qValues_t = target_net( next_state );

    if(isfinished)
        return qValues(0, action) - reward;
    else {
        return qValues(action) - ( reward + gamma * next_qValues_t( index_of_max(next_qValues) ) );
    }
}
