#ifndef DQN_H
#define DQN_H

#include "replay.h"

namespace dqn {

/* ----------------------------------------------------------------------------------------------------------
 * Deep Q-Network
 * ----------------------------------------------------------------------------------------------------------*/
class DQN
{
public:
    DQN(Replay& replay, size_t state_size, size_t action_size, float epsilon, float alpha, float gamma, size_t update_freq = 1);
    virtual ~DQN();

    void set_epsilon(float epsilon) { this->epsilon = epsilon; }
    float get_epsilon() const { return epsilon; }

    void set_alpha(float alpha) { this->alpha = alpha; trainer.set_learning_rate(alpha); }
    float get_alpha() const { return alpha; }

    /* update step */
    float learn(const std::vector<scalar_t>& state, const action_t& action, const scalar_t& reward, const std::vector<scalar_t>& next_state, bool isfinished);

    /* action wtth hightes q value */
    action_t generate_action(const std::vector<scalar_t>& state);

    /* get learned policy for state */
    action_t policy(const std::vector<scalar_t> &state);

    /* returns q value of given state action pair (can be used to monitor state action pairs) */
    scalar_t getQ(const std::vector<scalar_t> &state, int action);
    std::vector<scalar_t> getQs(const std::vector<scalar_t> &state);

    /* get network ref */
    const net_type& get_net() const { return net; }
    /* set network */
    void set_net(const net_type& net) { this->net = net; }

protected:
    net_type net;                                       // Q fnc approximation
    dlib::dnn_trainer<net_type, dlib::adam> trainer;    // optimizer
    Replay& replay;                                     // replay buffer (reference since shared)

    size_t action_size;
    size_t state_size;

    size_t cnt;
    size_t update_freq;
    float epsilon;
    float alpha;
    float gamma;

    bool use_per;   // flag to indicate if extra comutations for per required

    std::random_device rd;  //  (seed) engine
    std::mt19937 rng;       // random-number engine
    std::uniform_int_distribution<int> int_uni;
    std::uniform_real_distribution<double> real_uni;

    /* replay sampled history */
    virtual float experience_replay();

private:
    /* return the target values and optimal actions */
    virtual Batch qtarget(const Memory& samples, std::vector<int>& indicies, float gamma);
    /* compute the td error for a given transition */
    virtual scalar_t td_error(const Matrix& state, const action_t& action, const scalar_t& reward, const Matrix& next_state, bool isfinished);
};


/* ----------------------------------------------------------------------------------------------------------
 * Double Deep Q-Network
 * ----------------------------------------------------------------------------------------------------------*/
class DDQN : public DQN
{
public:
    DDQN(Replay& replay, size_t state_size, size_t action_size, size_t swap_cnt, float epsilon, float alpha, float gamma );
    virtual ~DDQN();

    /* get network ref */
    const net_type& get_target_net() const { return target_net; }
    /* set network */
    void set_target_net(const net_type& target_net) { this->target_net = target_net; }

protected:
    /* replay sampled history */
    virtual float experience_replay();

private:
   net_type target_net;    // target network, keep stable till swap_cnt reached
   size_t swap_cnt;

   /* return the target values and optimal actions */
   Batch qtarget(const Memory& samples, std::vector<int>& indicies, float gamma);
   /* compute the td error for a given transition */
   scalar_t td_error(const Matrix& state, const action_t& action, const scalar_t& reward, const Matrix& next_state, bool isfinished);
};

}

#endif // DQN_H
