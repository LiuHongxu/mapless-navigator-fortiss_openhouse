#ifndef REPLAY_H
#define REPLAY_H

#include <dlib/matrix.h>
#include <random>
#include <vector>

#include "config.h"
#include "sumtree.h"

namespace dqn {

typedef dlib::matrix<scalar_t> Matrix;
typedef std::vector<Matrix> Batch;

/* ----------------------------------------------------------------------------------------------------------
 * Memory, used by replay buffer to store matrices and vectors
 * ----------------------------------------------------------------------------------------------------------*/
class Memory {
public:
    Batch states;
    std::vector<action_t> actions;
    std::vector<float> rewards;
    Batch next_states;
    std::vector<float> td_errors;
    std::vector<uint8_t> isfinished;

    Memory& operator= ( const Memory& m ) {
        if (this != &m)
        {
            resize( m.get_buffer_size(), m.get_state_size() );
            for (size_t i = 0; i < m.get_buffer_size(); ++i) {
                states[i] = m.states[i];
                actions[i] = m.actions[i];
                rewards[i] = m.rewards[i];
                next_states[i] = m.next_states[i];
                td_errors[i] = m.td_errors[i];
                isfinished[i] = m.isfinished[i];
            }
        }
        return *this;
    }

    void resize(size_t buffer_size, int state_size) {
        states.resize(buffer_size);
        actions.resize(buffer_size);
        rewards.resize(buffer_size);
        next_states.resize(buffer_size);
        td_errors.resize(buffer_size);
        isfinished.resize(buffer_size);

        for(size_t i = 0; i < buffer_size; ++i) {
            states[i].set_size(state_size, 1);
            next_states[i].set_size(state_size, 1);
        }
    }
    size_t get_buffer_size() const { return states.size(); }
    int get_state_size() const { return states[0].nr(); }
};

/* ----------------------------------------------------------------------------------------------------------
 * Replay Buffer, sampling happens uniformely
 * ----------------------------------------------------------------------------------------------------------*/
class Replay
{
public:
    Replay(size_t buffer_size, size_t batch_size, int state_size = 1);
    virtual ~Replay() { }

    /* set new state size */
    void initialize(int state_size);
    void initialize(const Memory& memory, size_t cur_index, bool filled );

    /* buffer completly filled */
    bool is_filled() const { return filled; }

    /* return length of Replay */
    size_t get_buffer_size() const { return buffer_size; }

    /* return state dimension */
    int get_state_size() const { return memory.get_state_size(); }

    /* return size of one batch */
    size_t get_batch_size() const { return batch_size; }

    /* add new Element to Replay */
    virtual void push_back(const Matrix& state, const size_t& action, const scalar_t& reward, const Matrix& next_state,  const scalar_t& td, bool isfinished);

    /* update td error for at given index (not used by standard replay) */
    virtual void update(size_t index, const scalar_t& td) { }

    /* return references to uniform sampled Replay elements */
    virtual const Memory& sample(std::vector<float> &weights, std::vector<int> &indices);

    /* return reference to complete memory */
    const Memory& get_memory() const { return memory; }
    size_t get_current_index() const { return cur_index; }

protected:
    bool filled;
    size_t cur_index;

    size_t buffer_size;                        // max buffer size
    size_t batch_size;
    int state_size;

    Memory memory;
    Memory sampled_memory;
    Memory short_term_memory;

    std::random_device rd;                  //  (seed) engine
    std::mt19937 rng;                       // random-number engine
    std::uniform_int_distribution<int> int_uni;
};

/* ----------------------------------------------------------------------------------------------------------
 * Priorized Replay Buffer, sampling happens based on td error
 * alpha
 * ----------------------------------------------------------------------------------------------------------*/
class PriorizedReplay : public Replay
{
public:
    PriorizedReplay(size_t buffer_size, size_t batch_size, int state_size = 1, float alpha = 0.6f, float epsilon = 0.01f);
    virtual ~PriorizedReplay() { }

    /* add new Element to Replay */
    virtual void push_back(const Matrix& state, const size_t& action, const scalar_t& reward, const Matrix& next_state,  const scalar_t& td, bool isfinished);

    /* return references to uniform sampled Replay elements */
    virtual const Memory& sample(std::vector<float> &weights, std::vector<int> &indices);

    /* update td error for at given index */
    void update(size_t index, const scalar_t& td);

private:
    SumTree sumtree;
    float alpha;
    float epsilon;
};

/* ----------------------------------------------------------------------------------------------------------
 * Replay Buffer, sampling happens uniformely, using n-step rewards
 * ----------------------------------------------------------------------------------------------------------*/
class NStepReplay : public Replay
{
public:
    NStepReplay(size_t buffer_size, size_t batch_size, int state_size = 1, size_t n_step = 25, float gamma = 0.99f);
    virtual ~NStepReplay() { }

    /* add new Element to Replay */
    void push_back(const Matrix& state, const size_t& action, const scalar_t& reward, const Matrix& next_state, const scalar_t& td, bool isfinished);

private:
    Memory shorttermmemory;
    size_t n_step;
    size_t start_idx;
    size_t cur_idx;
    size_t cur_size;

    float R;
    float gamma_n;
    float gamma;
};

/* ----------------------------------------------------------------------------------------------------------
 * Priorized Replay Buffer, sampling happens based on td error, using n-step rewards
 * ----------------------------------------------------------------------------------------------------------*/
class PriorizedNStepReplay : public NStepReplay
{
public:
    PriorizedNStepReplay(size_t buffer_size, size_t batch_size, int state_size = 1, size_t n_step = 25, float gamma = 0.99f, float alpha = 0.6f, float epsilon = 0.01f);
    virtual ~PriorizedNStepReplay() { }

    /* add new Element to Replay */
    virtual void push_back(const Matrix& state, const size_t& action, const scalar_t& reward, const Matrix& next_state,  const scalar_t& td, bool isfinished);

    /* return references to uniform sampled Replay elements */
    virtual const Memory& sample(std::vector<float> &weights, std::vector<int> &indices);

    /* update td error for at given index */
    void update(size_t index, const scalar_t& td);

private:
    SumTree sumtree;
    float alpha;
    float epsilon;
};

}

#endif // REPLAY_H
