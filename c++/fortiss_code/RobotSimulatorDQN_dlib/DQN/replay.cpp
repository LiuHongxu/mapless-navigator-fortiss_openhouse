#include "replay.h"

using namespace dqn;

/* ----------------------------------------------------------------------------------------------------------
 * Replay Buffer, sampling happens uniformely
 * ----------------------------------------------------------------------------------------------------------*/
Replay::Replay(size_t buffer_size, size_t batch_size, int state_size)
    : buffer_size(buffer_size), batch_size(batch_size), state_size(state_size), cur_index(0), filled(false)
{
    memory.resize(buffer_size, state_size);
    sampled_memory.resize(batch_size, state_size);

    rng = std::mt19937(rd());
    int_uni = std::uniform_int_distribution<int>(0, buffer_size-1);
}

void Replay::initialize(int state_size)
{
    this->state_size = state_size;
    memory.resize(buffer_size, state_size);
    sampled_memory.resize(batch_size, state_size);
}

void Replay::initialize(const Memory& memory, size_t cur_index, bool filled )
{
    this->memory = memory;
    this->cur_index = cur_index;
    this->filled = filled;

    buffer_size = memory.get_buffer_size();
    state_size = memory.get_state_size();

    sampled_memory.resize(batch_size, state_size);
    int_uni = std::uniform_int_distribution<int>(0, buffer_size-1);
}

void Replay::push_back(const Matrix& state,
                       const size_t &action,
                       const scalar_t& reward,
                       const Matrix& next_state,
                       const scalar_t& td,
                       bool isfinished)
{
    if(cur_index >= buffer_size) {
        filled = true;
        cur_index = 0;
    }
    memory.states[cur_index] = state;
    memory.actions[cur_index] = action;
    memory.rewards[cur_index] = reward;
    memory.next_states[cur_index] = next_state;
    memory.td_errors[cur_index] = td;
    memory.isfinished[cur_index] = isfinished;
    cur_index++;
}

const Memory& Replay::sample(std::vector<float> &weights, std::vector<int> &indices)
{
    for( int i = 0; i < batch_size; ++i ) {
        int idx = int_uni(rng);
        sampled_memory.states[i] = memory.states[idx];
        sampled_memory.actions[i] = memory.actions[idx];
        sampled_memory.rewards[i] = memory.rewards[idx];
        sampled_memory.next_states[i] = memory.next_states[idx];
        sampled_memory.td_errors[i] = memory.td_errors[idx];
        sampled_memory.isfinished[i] = memory.isfinished[idx];
    }
    return sampled_memory;
}

/* ----------------------------------------------------------------------------------------------------------
 * Priorized Replay Buffer, sampling happens based on td error
 * ----------------------------------------------------------------------------------------------------------*/

PriorizedReplay::PriorizedReplay(size_t buffer_size, size_t batch_size, int state_size, float alpha, float epsilon)
    : Replay (buffer_size, batch_size, state_size), sumtree(buffer_size), alpha(alpha), epsilon(epsilon)
{
}

void PriorizedReplay::push_back(const Matrix& state,
                                const size_t& action,
                                const scalar_t& reward,
                                const Matrix& next_state,
                                const scalar_t& td,
                                bool isfinished)
{
    float priority = powf( std::abs(td) + epsilon, alpha);
    Replay::push_back(state, action, reward, next_state, td, isfinished);
    sumtree.set(cur_index - 1, priority);
}

const Memory& PriorizedReplay::sample(std::vector<float> &weights, std::vector<int> &indices)
{
    sumtree.sample(batch_size, indices, 1.0f, weights);
    for( int i = 0; i < batch_size; ++i ) {
        int idx = indices[i];
        sampled_memory.states[i] = memory.states[idx];
        sampled_memory.actions[i] = memory.actions[idx];
        sampled_memory.rewards[i] = memory.rewards[idx];
        sampled_memory.next_states[i] = memory.next_states[idx];
        sampled_memory.td_errors[i] = memory.td_errors[idx];
        sampled_memory.isfinished[i] = memory.isfinished[idx];
    }
    return sampled_memory;
}

/* update td error for at given index */
void PriorizedReplay::update(size_t index, const scalar_t& td)
{
    float priority = powf( std::abs(td) + epsilon, alpha);
    sumtree.set(index, priority);
}

/* ----------------------------------------------------------------------------------------------------------
 * Replay Buffer, sampling happens uniformely, using n-step rewards
 * ----------------------------------------------------------------------------------------------------------*/
NStepReplay::NStepReplay(size_t buffer_size, size_t batch_size, int state_size, size_t n_step, float gamma)
    : Replay (buffer_size, batch_size, state_size), n_step(n_step), gamma(gamma)
{
    start_idx = 0;
    cur_idx = 0;
    R = 0.0f;
    cur_size = 0;
    gamma_n = std::pow(gamma, (float)n_step);
    shorttermmemory.resize(n_step, state_size);
}

void NStepReplay::push_back(const Matrix& state,
                                const size_t& action,
                                const scalar_t& reward,
                                const Matrix& next_state,
                                const scalar_t& td,
                                bool isfinished)
{
    // 1. add new sample to shormemory
    shorttermmemory.states[cur_idx] = state;
    shorttermmemory.actions[cur_idx] = action;
    shorttermmemory.rewards[cur_idx] = reward;
    shorttermmemory.next_states[cur_idx] = next_state;
    shorttermmemory.td_errors[cur_idx] = td;
    shorttermmemory.isfinished[cur_idx] = isfinished;
    cur_idx++;
    cur_size++;

    // 2. update n-step cummulative reward
    R = (R + reward * gamma_n) / gamma;

    // 3. special case: next_state is terminal state
    if(isfinished) {
        while( cur_size > 0 ) {
            Replay::push_back(shorttermmemory.states[start_idx],            // s_0
                              shorttermmemory.actions[start_idx],           // a_0
                              R,                                            // n step reward
                              shorttermmemory.next_states[cur_idx - 1],     // s_n-1
                              shorttermmemory.td_errors[start_idx],         // td_n-1
                              shorttermmemory.isfinished[cur_idx - 1]);     // is finished flag

            R = ( R - shorttermmemory.rewards[start_idx] ) / gamma;         // update n step reward ( remove r_0 )
            start_idx++;    // remove sample_0
            cur_size--;
            start_idx = start_idx % n_step;
        }
        R = 0.0f;
    }

    // 4. start filling replay buffer if enough samples availble to comute n-step reward
    if( cur_size >= n_step) {
        Replay::push_back(shorttermmemory.states[start_idx],            // s_0
                          shorttermmemory.actions[start_idx],           // a_0
                          R,                                            // n step reward
                          shorttermmemory.next_states[cur_idx - 1],     // s_n-1
                          shorttermmemory.td_errors[start_idx],         // td_n-1
                          shorttermmemory.isfinished[cur_idx - 1]);     // is finished flag

        R = (R - shorttermmemory.rewards[start_idx]) / gamma;         // update n step reward ( remove r_0 )
        start_idx++;    // remove sample_0
        cur_size--;
    }

    // 5. make sure index of circular buffer stayes within array range
    start_idx = start_idx % n_step;
    cur_idx = cur_idx % n_step;
}

/* ----------------------------------------------------------------------------------------------------------
 * Priorized Replay Buffer, sampling happens based on td error, using n-step rewards
 * ----------------------------------------------------------------------------------------------------------*/

PriorizedNStepReplay::PriorizedNStepReplay(size_t buffer_size, size_t batch_size, int state_size, size_t n_step, float gamma, float alpha, float epsilon)
    : NStepReplay(buffer_size, batch_size, state_size, n_step, gamma), sumtree(buffer_size), alpha(alpha), epsilon(epsilon)
{
}

void PriorizedNStepReplay::push_back(const Matrix& state,
                                const size_t& action,
                                const scalar_t& reward,
                                const Matrix& next_state,
                                const scalar_t& td,
                                bool isfinished)
{
    float priority = powf( std::abs(td) + epsilon, alpha);
    NStepReplay::push_back(state, action, reward, next_state, td, isfinished);
    sumtree.set(cur_index - 1, priority);
}

const Memory& PriorizedNStepReplay::sample(std::vector<float> &weights, std::vector<int> &indices)
{
    sumtree.sample(batch_size, indices, 1.0f, weights);
    for( int i = 0; i < batch_size; ++i ) {
        int idx = indices[i];
        sampled_memory.states[i] = memory.states[idx];
        sampled_memory.actions[i] = memory.actions[idx];
        sampled_memory.rewards[i] = memory.rewards[idx];
        sampled_memory.next_states[i] = memory.next_states[idx];
        sampled_memory.td_errors[i] = memory.td_errors[idx];
        sampled_memory.isfinished[i] = memory.isfinished[idx];
    }
    return sampled_memory;
}

/* update td error for at given index */
void PriorizedNStepReplay::update(size_t index, const scalar_t& td)
{
    float priority = powf( std::abs(td) + epsilon, alpha);
    sumtree.set(index, priority);
}
