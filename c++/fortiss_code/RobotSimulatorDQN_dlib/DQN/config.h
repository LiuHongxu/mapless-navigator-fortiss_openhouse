#ifndef CONFIG_H
#define CONFIG_H

#include <dlib/dnn.h>
#include "dlib/huberloss.h"
#include <cstddef>

namespace dqn {

/*
 * define basic types here
 */
typedef float scalar_t;
typedef size_t action_t;

/*
 * define network structure here
 */
/*using net_type = dlib::loss_huber_multioutput<
                            dlib::fc<3,
                            dlib::relu<dlib::fc<128,
                            dlib::relu<dlib::fc<256,
                            dlib::relu<dlib::input<dlib::matrix<float>
                            >>>>>>>>;*/

/*using net_type = dlib::loss_huber_multioutput<
                            dlib::fc<3,
                            dlib::relu<dlib::fc<256,
                            dlib::relu<dlib::fc<512,
                            dlib::relu<dlib::input<dlib::matrix<float>
                            >>>>>>>>;*/

using net_type = dlib::loss_huber_multioutput<
                            dlib::fc<3,
                            dlib::relu<dlib::fc<32,
                            dlib::relu<dlib::fc<64,
                            dlib::relu<dlib::fc<128,
                            dlib::relu<dlib::fc<256,
                            dlib::relu<dlib::input<dlib::matrix<float>
                            >>>>>>>>>>>>;

/*using net_type = dlib::loss_huber_multioutput<
                            dlib::fc<3,
                            dlib::relu<dlib::fc<16,
                            dlib::relu<dlib::fc<32,
                            dlib::relu<dlib::fc<64,
                            dlib::relu<dlib::fc<128,
                            dlib::relu<dlib::input<dlib::matrix<float>
                            >>>>>>>>>>>>;*/
}

#endif // CONFIG_H
