#include "dqnenvironment.h"

#include <iostream>

DQNEnvironment::DQNEnvironment(Environment *env, ag::Agent* agent, const b2Vec2& goal_position)
    : AgentEnvironment(env, agent, 3, 20), goal_position(goal_position)
{
    // setup hyper parameters
    min_goal_dist = 0.25f;       // 15 cm
    min_obstical_dist = 0.22f;   // 40 cm
    range_dim = 18;              // 5 rayes
    max_range = 2.0;             // 3 m
    hist_len = 9;                // not used at the moment

    setup();
}

/* setup variables */
void DQNEnvironment::setup()
{
    // number of laser rays
    normalized_ranges.resize(range_dim);

    // setup free area around goal
    clear_goal_area.upperBound = b2Vec2(min_goal_dist / 2.0f, min_goal_dist / 2.0f) + goal_position;
    clear_goal_area.lowerBound = b2Vec2(-min_goal_dist / 2.0f, -min_goal_dist / 2.0f) + goal_position;

    prev_distance = -1.0f;
    hist_states.resize(hist_len * (range_dim + 2), 0.0f);
}

void DQNEnvironment::set_goal_position(const b2Vec2& goal_position )
{
    this->goal_position = goal_position;
    clear_goal_area.upperBound = b2Vec2(min_goal_dist / 2.0f, min_goal_dist / 2.0f) + goal_position;
    clear_goal_area.lowerBound = b2Vec2(-min_goal_dist / 2.0f, -min_goal_dist / 2.0f) + goal_position;
}

void DQNEnvironment::execute(action_type action)
{
    // 1. remember last action
    prev_action = action;

    // 2. execute action
    ag::VelCmd vel_cmd;
    vel_cmd.velocity.y = 0.0f;
    switch(action) {
    case 0:
        vel_cmd.velocity.x = 0.3f;
        vel_cmd.omega = 0.0f;
        break;
    case 1:
        vel_cmd.velocity.x = 0.04f; // 0.05f;
        vel_cmd.omega = 0.5f;
        break;
    case 2:
        vel_cmd.velocity.x = 0.04f; // 0.05;
        vel_cmd.omega = -0.5f;
        break;
    }
    agent->command( vel_cmd );
}

bool DQNEnvironment::observe(EnvState& env_state)
{
    bool hit_obstacle, reached_goal, new_observation;
    hit_obstacle = reached_goal = false;

    ag::SensorReading sensorreadings;
    ag::ModelStates modelstate;
    new_observation = agent->model_states( modelstate );
    new_observation &= agent->observation( sensorreadings );

    if( !new_observation )
        return false; // no new observation

    // 1. simulate the callback function
    reached_goal = modelStatesCallback( modelstate, min_goal_dist );
    hit_obstacle = scanCallback( sensorreadings, min_obstical_dist );

    // 2. compute reward
    if(prev_distance < 0.0f) {
        prev_distance = distance; // init step
    }

    float reward = 0;
    if(hit_obstacle)
        reward = -150.0f;
    else if(reached_goal)
        reward = 100.0f;
    reward += std::min(1.5f, std::max(-1.5f, 15.0f * (prev_distance - distance) ) );
    //reward -= potential_distance( sensorreadings, min_obstical_dist, 0.258f );
    reward = std::min( 100.0f, std::max( -150.0f, reward)); // clip

    // 4. combine into a single state vector ( scale state to range [0, 1] )
    auto env_state_t = static_cast<TypedEnvState<float>* >(&env_state);
    env_state_t->state = normalized_ranges;
    env_state_t->state.push_back( 1.0f / distance * min_goal_dist );
    env_state_t->state.push_back( theta / sim::PI + 1.0f );
    //env_state_t->state.insert(env_state_t->state.end(), hist_states.begin(), hist_states.end());
    env_state_t->reward = reward;
    env_state_t->success = reached_goal;
    env_state_t->done = hit_obstacle | reached_goal;

    // 5. save history
    prev_distance = distance;
    hist_states.insert(hist_states.begin(), env_state_t->state.begin(), env_state_t->state.begin() + range_dim + 2);
    hist_states.erase(hist_states.end() - range_dim - 2, hist_states.end());
    return true;
}

void DQNEnvironment::reset(bool full_reset )
{
    std::fill(hist_states.begin(), hist_states.end(), 0.0f);
    AgentEnvironment::reset(&clear_goal_area, full_reset);
}

bool DQNEnvironment::scanCallback(const ag::SensorReading& ranges, float min_range)
{
    size_t mod = (size_t)(ranges.size() / normalized_ranges.size() );
    bool done = false;

    // laser range data scaled to range [0, 1]
    size_t k  = 0;
    for(size_t i = 0; i < ranges.size(); ++i) {
        if(i % mod == 0) {
            if( std::isinf(ranges[i]) )         // catch inf
                normalized_ranges[k] = 1.0f / max_range * min_range;
            else if( std::isnan(ranges[i]) )    // catch nan
                normalized_ranges[k] = 0.0f;
            else
                normalized_ranges[k] = 1.0f / ranges[i] * min_range;
            k++;
        }
        // done?
        if(ranges[i] < min_range)
            done = true;
    }
    return done;
}

bool DQNEnvironment::modelStatesCallback(const ag::ModelStates& modelstate, float min_range)
{
    // distance to goal
    b2Vec2 distance_vec = goal_position - modelstate.position;
    distance = sqrtf(distance_vec.x * distance_vec.x + distance_vec.y * distance_vec.y);
    // angle to goal
    theta = rotation_angle(atan2f(distance_vec.y, distance_vec.x), modelstate.theta );

    return distance < min_range;
}

float DQNEnvironment::rotation_angle(float src, float dst)
{
    return (src > dst) ? -sim::PI + std::fmod(src - dst + sim::PI, sim::PI_2)
                       :  sim::PI - std::fmod(dst - src + sim::PI, sim::PI_2);
}

float DQNEnvironment::potential_distance( std::vector<float> d, float d_min, float tau )
{
    float mean = 0.0f;
    for( size_t i = 0; i < d.size(); ++i ) {
        mean += 12.0f * expf( -(d[i] - d_min) / tau );
    }
    return mean / d.size();
}
