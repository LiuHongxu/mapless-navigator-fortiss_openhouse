#include "multirobotsession.h"
#include "Simulator/staticobstacle.h"
#include "Simulator/agent/robot.h"
#include "Simulator/matstruct.h"
#include "DQN/utilities.h"

// github.com/lvella/dlib-rnn/blob/master/rnn.h

using namespace dqn;

MultiRobotSession::MultiRobotSession(Environment *env, const std::string &log_dir, size_t cnt_save_steps, size_t num_robots)
    : Session(env, log_dir, "multi_robot_session/", cnt_save_steps), ddqn(nullptr), replay(nullptr), num_robots(num_robots),
      observation_cnt(0)
{
    // resize
    actions.resize(num_robots);
    env_states.resize(num_robots);
    states.resize(num_robots);
    env_inital_states.resize(num_robots);
    agentenvs.resize(num_robots, nullptr);
    is_after_resets.resize(num_robots, true);

    rng = std::mt19937(rd());
    int_uni = std::uniform_int_distribution<int>(0, num_robots-1);
}

MultiRobotSession::~MultiRobotSession()
{
    if(ddqn)
        delete ddqn;
    if(replay)
        delete replay;
    for( size_t i = 0; i < num_robots; ++i)
        if(agentenvs[i])
            delete agentenvs[i];
}

/* add objects to envrionment */
void MultiRobotSession::setup_environment(Environment* env)
{
    // 0. setup goal positions
    goal_positions = { b2Vec2(1.0f, 1.0f), b2Vec2(4.0f, 1.0f), b2Vec2(4.0f, 4.0f), b2Vec2(1.0f, 4.0f) };

    // 1. setup obsticales ( define size and pos )
    env->add( new StaticRectangle(b2Vec2(3, 3), b2Vec2(0.25f, 0.7f)) );
    env->add( new StaticRectangle(b2Vec2(2.5, 0.5), b2Vec2(0.4f, 0.7f)) );
    env->add( new StaticRectangle(b2Vec2(2, 4.5), b2Vec2(0.3f, 0.5f)) );
    env->add( new StaticCircle(b2Vec2(0.5, 2), 0.3f) );
    env->setWall();

    // 2. setup agents
    float robot_pos_std = 0.0f;         // 0.01;
    float robot_orient_std = 0.0f * sim::DEG2RAD;
    float robot_lin_vel_std = 0.0f;     //0.003; // error 1%
    float robot_ang_vel_std = 0.0f;     // error 3.3%

    for( size_t i = 0; i < num_robots; ++i) {
        Robot* robot = new Robot(b2Vec2(1, 1), 1.56f, 0.175f, robot_pos_std, robot_orient_std, robot_lin_vel_std, robot_ang_vel_std);
        env->add( robot );
        agentenvs[i] = new DQNEnvironment( env, robot, goal_positions[i] );
    }
}

/* gets called after setup is finished */
void MultiRobotSession::on_start(size_t total_episodes)
{
    // 1. setup reinforcement learning algorithm
    eps_discount = 0.99988f; // 0.999975f
    eps_f = 0.4f; // 0.1f

    size_t buffer_size = 1000000; // 1000000
    size_t batch_size = 256 * num_robots;
    size_t swap_cnt = 5000;
    float eps_init = 0.9f; // 0.9f;
    float alpha = 0.0004f;
    float gamma = 0.99f; // 0.95

    replay = new Replay(buffer_size, batch_size, agentenvs[0]->get_dim_states());
    ddqn = new DDQN(*replay, agentenvs[0]->get_dim_states(), agentenvs[0]->get_n_actions(), swap_cnt, eps_init, alpha, gamma);

    // 2. setup monitor variables
    rewards.resize(num_robots);
    num_of_steps.resize(num_robots);
    succsess_rate.resize(num_robots);
    for(size_t i = 0; i < num_robots; ++i) {
        rewards.reserve(total_episodes);
        num_of_steps.reserve(total_episodes);
        succsess_rate.reserve(total_episodes);
    }
    epsilons.reserve(total_episodes);
    alphas.reserve(total_episodes);
    fitting_errs.reserve(total_episodes);

    // 3. reset everything befor training
    for(auto iter = agentenvs.begin(); iter != agentenvs.end(); ++iter)
        (*iter)->reset(false);

    std::fill(is_after_resets.begin(), is_after_resets.end(), true);
    cnt_steps = 0;
}

/* run neuronal net training here */
void MultiRobotSession::train(size_t& cnt_episodes)
{
    float err = 0;

    // for every single agnet perform the following steps
    for(size_t i = 0; i < num_robots; ++i) {
        auto& agentenv = agentenvs[i];
        auto& env_state = env_states[i];
        auto& action = actions[i];
        auto& state = states[i];
        auto& env_inital_state = env_inital_states[i];
        auto is_after_reset = is_after_resets[i];

        // 1. check if after reset
        if(is_after_reset) {
            is_after_reset = false;
            // get inital observation
            agentenv->observe(env_state);
            state = env_state.state;
            env_inital_state = env_state.state;
            // get inital action
            action = ddqn->generate_action( state );
        }

        // 2. do observation (if not available, repeat the same action again, no learning)
        if( !agentenv->observe(env_state) ) {
            agentenv->execute( action );
            continue;
        }
        observation_cnt++; // observation done, inc cnt

        // 3. learn
        err = ddqn->learn(state, action, env_state.reward, env_state.state, env_state.done);

        // 4. transition to new state
        state = env_state.state;
        cumulated_reward += env_state.reward;
        cnt_steps++;

        // 5. execute next action
        action = ddqn->generate_action( state );
        agentenv->execute( action );
    }

    // for every single agnet perform the following steps only if every agent has made a new observation
    if(observation_cnt >= num_robots) {
        observation_cnt = 0;
        for(size_t i = 0; i < num_robots; ++i) {
            auto& agentenv = agentenvs[i];
            auto& env_state = env_states[i];

            // 6. check if episode is finished
            if( env_state.done || cnt_steps > 1000 ) {

                // print episode results
                printf("EP: %d, steps: %d, eps: %4.5f, alpha: %4.5f, rew: %4.2f, err: %4.2f\n",
                       cnt_episodes, cnt_steps, ddqn->get_epsilon(), ddqn->get_alpha(), cumulated_reward, err);

                // save monitor variabels
                rewards[i].push_back(cumulated_reward);
                num_of_steps[i].push_back(cnt_steps);
                succsess_rate[i].push_back((int)env_state.success);

                fitting_errs.push_back(err);
                epsilons.push_back(ddqn->get_epsilon());
                alphas.push_back(ddqn->get_alpha());

                // update epsilon
                if( ddqn->get_epsilon() > eps_f && err != 0.0f)
                    ddqn->set_epsilon( ddqn->get_epsilon() * eps_discount );

                // reset the goal position
                agentenv->set_goal_position( goal_positions[int_uni(rng)] );

                // reset simulation
                cnt_steps = 0;
                cumulated_reward = 0;
                agentenv->reset(false);
                is_after_resets[i] = true;

                cnt_episodes++;
            }
        }
    }
}

/* execture learned policy here */
void MultiRobotSession::execute()
{
    // for every single agnet perform the following steps
    for(size_t i = 0; i < num_robots; ++i) {
        auto& agentenv = agentenvs[i];
        auto& env_state = env_states[i];
        auto& action = actions[i];
        auto& state = states[i];
        auto& env_inital_state = env_inital_states[i];
        auto is_after_reset = is_after_resets[i];

        // 1. check if after reset
        if(is_after_reset) {
            is_after_reset = false;
            // get inital observation
            agentenv->observe(env_state);
            state = env_state.state;
            env_inital_state = env_state.state;
            // get inital action
            action = ddqn->generate_action( state );
        }

        // 2. do observation (if not available, repeat the same action again, no learning)
        if( !agentenv->observe(env_state) ) {
            agentenv->execute( action );
            return;
        }

        // 3. execute next action
        action = ddqn->policy( state );
        agentenv->execute( action );

        // 4. transition to new state
        state = env_state.state;
        cumulated_reward += env_state.reward;
        cnt_steps++;

        // 5. check if episode is finished
        if( env_state.done ) {
            // print episode results
            printf("steps: %d, eps: %4.5f, alpha: %4.5f, rew: %4.2f\n",
                   cnt_steps, ddqn->get_epsilon(), ddqn->get_alpha(), cumulated_reward);

            // reset simulation
            cnt_steps = 0;
            cumulated_reward = 0;
            agentenv->reset();
            is_after_resets[i] = true;
        }
    }
}

/* save all data */
int MultiRobotSession::save(const std::string& dirpath, bool all, size_t cnt_episodes)
{
    // allways save network weights
    std::string file_name = dirpath + "net_" + std::to_string(cnt_episodes) + ".dat";
    std::fstream ofs(file_name, std::ofstream::out | std::ostream::binary);
    ofs << *ddqn;
    ofs.close();

    // only save repay and environment variables if all flag activated
    if(all) {
        ofs.open(dirpath + "net.dat", std::ofstream::out | std::ostream::binary);
        ofs << *ddqn;
        ofs.close();

        ofs.open(dirpath + "replay.dat", std::ofstream::out | std::ostream::binary);
        ofs << *replay;
        ofs.close();

        matio::MatStruct monitor;
        monitor.add("cumulated_reward", alphas);
        monitor.add("fitting_errs", fitting_errs);
        monitor.add("epsilons", epsilons);
        for(size_t i = 0; i < num_robots; ++i) {
            auto id = std::to_string(6);
            monitor.add("succsess_rate_" + id, succsess_rate[i]);
            monitor.add("cumulated_reward_" + id, rewards[i]);
            monitor.add("num_of_steps_" + id, num_of_steps[i]);
        }
        monitor.write(dirpath+"environment.mat", "env_struct");
    }
}

/* load all data */
int MultiRobotSession::load(const std::string& dirpath)
{
    // 1. load repaly buffer
    std::fstream ifs(dirpath+"replay.dat", std::ifstream::in);
    if(ifs.is_open()) {
        ifs >> *replay;
        ifs.close();
        std::cout << "loaded old replay data: " << replay->get_buffer_size() << std::endl;
    }
    // 2. load network
    ifs.open(dirpath+"net.dat", std::ifstream::in);
    if(ifs.is_open()) {
        ifs >> *ddqn;
        ifs.close();
        std::cout << "loaded old net structure" << std::endl;
    }
}
