#include "singlerobotsession.h"
#include "Simulator/staticobstacle.h"
#include "Simulator/agent/robot.h"
#include "Simulator/matstruct.h"
#include "DQN/utilities.h"

// github.com/lvella/dlib-rnn/blob/master/rnn.h

using namespace dqn;

SingleRobotSession::SingleRobotSession(Environment *env, const std::string &log_dir, size_t cnt_save_steps)
    : Session(env, log_dir, "single_robot_session/", cnt_save_steps), ddqn(nullptr), replay(nullptr), agentenv(nullptr)
{
}

SingleRobotSession::~SingleRobotSession()
{
    if(ddqn)
        delete ddqn;
    if(replay)
        delete replay;
    if(agentenv)
        delete agentenv;
}

/* add objects to envrionment */
void SingleRobotSession::setup_environment(Environment* env)
{
    // 1. setup obsticales ( define size and pos )
    env->add( new StaticCircle(b2Vec2(2.5, 2.5), 0.15f) );
    env->add( new StaticCircle(b2Vec2(2.8, 2.6), 0.15f) );
    env->add( new StaticCircle(b2Vec2(4, 1), 0.2f) );
    env->add( new StaticCircle(b2Vec2(2, 4), 0.3f) );
    env->add( new StaticCircle(b2Vec2(2, 4), 0.3f) );
    //env->add( new StaticRectangle(b2Vec2(3, 3), b2Vec2(0.25f, 0.7f)) );
    //env->add( new StaticRectangle(b2Vec2(1, 4), b2Vec2(0.4f, 0.7f)) );
    //env->add( new StaticRectangle(b2Vec2(1, 4), b2Vec2(0.3f, 0.5f)) );
    //env->add( new StaticRectangle(b2Vec2(3, 3), b2Vec2(0.25f, 0.7f)));
    env->setWall();

    // 2. setup agents
    float robot_pos_std = 0.0f;         // 0.01;
    float robot_orient_std = 0.0f * sim::DEG2RAD;
    float robot_lin_vel_std = 0.0f;     //0.003; // error 1%
    float robot_ang_vel_std = 0.0f;     // error 3.3%
    Robot* robot = new Robot(b2Vec2(2.4, 1.8), 1.56, 0.175f, robot_pos_std, robot_orient_std, robot_lin_vel_std, robot_ang_vel_std);
    env->add( robot );
    agentenv = new DQNEnvironment( env, robot, b2Vec2(3.5f, 3.5f) ); // b2Vec2(3.8f, 3.8f)
}

/* gets called after setup is finished */
void SingleRobotSession::on_start(size_t total_episodes)
{
    // 1. setup reinforcement learning algorithm
    eps_discount = 0.99988f; // 0.999975f
    eps_f = 0.3f; // 0.1f

    int buffer_size = 1000000; // 1000000
    int batch_size = 256;
    int swap_cnt = 5000;
    float eps_init = 0.9f; // 0.9f;
    float alpha = 0.0004f;
    float gamma = 0.99f; // 0.95

    replay = new PriorizedNStepReplay(buffer_size, batch_size, agentenv->get_dim_states(), 25, 0.99, 0.2);
    ddqn = new DDQN(*replay, agentenv->get_dim_states(), agentenv->get_n_actions(), swap_cnt, eps_init, alpha, gamma);

    // 2. setup monitor variables
    rewards.reserve(total_episodes);
    fitting_errs.reserve(total_episodes);
    epsilons.reserve(total_episodes);
    alphas.reserve(total_episodes);
    succsess_rate.reserve(total_episodes);
    q_value_start.resize( (size_t)agentenv->get_n_actions() );
    q_value_goal.resize( (size_t)agentenv->get_n_actions() );
    for(int i = 0; i < agentenv->get_n_actions(); ++i) {
        q_value_start.reserve( total_episodes );
        q_value_goal.reserve( total_episodes );
    }

    // 3. reset everything befor training
    agentenv->reset();
    is_after_reset = true;
    cnt_steps = 0;
}

/* run neuronal net training here */
void SingleRobotSession::train(size_t& cnt_episodes)
{
    // 1. check if after reset
    if(is_after_reset) {
        is_after_reset = false;
        // get inital observation
        agentenv->observe(env_state);
        state = env_state.state;
        env_inital_state = env_state.state;
        // get inital action
        action = ddqn->generate_action( state );
    }

    // 2. do observation (if not available, repeat the same action again, no learning)
    if( !agentenv->observe(env_state) ) {
        agentenv->execute( action );
        return;
    }

    // 3. learn
    float err = ddqn->learn(state, action, env_state.reward, env_state.state, env_state.done);

    // 4. transition to new state
    state = env_state.state;
    cumulated_reward += env_state.reward;
    cnt_steps++;

    // 5. execute next action
    action = ddqn->generate_action( state );
    agentenv->execute( action );

    // 6. check if episode is finished
    if( env_state.done || cnt_steps > 1000 ) {
        // print episode results
        printf("EP: %d, steps: %d, eps: %4.5f, alpha: %4.5f, rew: %4.2f, err: %4.2f\n",
               cnt_episodes, cnt_steps, ddqn->get_epsilon(), ddqn->get_alpha(), cumulated_reward, err);

        // save monitor variabels
        rewards.push_back(cumulated_reward);
        fitting_errs.push_back(err);
        epsilons.push_back(ddqn->get_epsilon());
        alphas.push_back(ddqn->get_alpha());
        num_of_steps.push_back(cnt_steps);
        succsess_rate.push_back((int)env_state.success);

        q_value_start[0].push_back( ddqn->getQ(env_inital_state, 0) );
        q_value_start[1].push_back( ddqn->getQ(env_inital_state, 1) );
        q_value_start[2].push_back( ddqn->getQ(env_inital_state, 2) );
        if( env_state.success ) {
            q_value_goal[0].push_back( ddqn->getQ(state, 0) );
            q_value_goal[1].push_back( ddqn->getQ(state, 1) );
            q_value_goal[2].push_back( ddqn->getQ(state, 2) );
        }

        // update epsilon
        if( ddqn->get_epsilon() > eps_f && err != 0.0f)
            ddqn->set_epsilon( ddqn->get_epsilon() * eps_discount );

        // reset simulation
        cnt_steps = 0;
        cumulated_reward = 0;
        agentenv->reset();
        is_after_reset = true;

        cnt_episodes++;
    }
}

/* execture learned policy here */
void SingleRobotSession::execute()
{
    // 1. check if after reset
    if(is_after_reset) {
        is_after_reset = false;
        // get inital observation
        agentenv->observe(env_state);
        state = env_state.state;
        env_inital_state = env_state.state;
        // get inital action
        action = ddqn->generate_action( state );
    }

    // 2. do observation (if not available, repeat the same action again, no learning)
    if( !agentenv->observe(env_state) ) {
        agentenv->execute( action );
        return;
    }

    // 3. execute next action
    action = ddqn->policy( state );
    agentenv->execute( action );

    // 4. transition to new state
    state = env_state.state;
    cumulated_reward += env_state.reward;
    cnt_steps++;

    // 5. check if episode is finished
    if( env_state.done ) {
        // print episode results
        printf("steps: %d, eps: %4.5f, alpha: %4.5f, rew: %4.2f\n",
               cnt_steps, ddqn->get_epsilon(), ddqn->get_alpha(), cumulated_reward);

        // reset simulation
        cnt_steps = 0;
        cumulated_reward = 0;
        agentenv->reset();
        is_after_reset = true;
    }
}

/* save all data */
int SingleRobotSession::save(const std::string& dirpath, bool all, size_t cnt_episodes)
{
    // allways save network weights
    std::string file_name = dirpath + "net_" + std::to_string(cnt_episodes) + ".dat";
    std::fstream ofs(file_name, std::ofstream::out | std::ostream::binary);
    ofs << *ddqn;
    ofs.close();

    // only save repay and environment variables if all flag activated
    if(all) {
        ofs.open(dirpath + "net.dat", std::ofstream::out | std::ostream::binary);
        ofs << *ddqn;
        ofs.close();

        ofs.open(dirpath + "replay.dat", std::ofstream::out | std::ostream::binary);
        ofs << *replay;
        ofs.close();

        matio::MatStruct monitor;
        monitor.add("cumulated_reward", rewards);
        monitor.add("fitting_errs", fitting_errs);
        monitor.add("num_of_steps", num_of_steps);
        monitor.add("epsilons", epsilons);
        monitor.add("succsess_rate", succsess_rate);

        monitor.add("q_value_start_1", q_value_start[0]);
        monitor.add("q_value_start_2", q_value_start[1]);
        monitor.add("q_value_start_3", q_value_start[2]);

        monitor.add("q_value_goal_1", q_value_goal[0]);
        monitor.add("q_value_goal_2", q_value_goal[1]);
        monitor.add("q_value_goal_3", q_value_goal[2]);

        monitor.write(dirpath+"environment.mat", "env_struct");
    }
}

/* load all data */
int SingleRobotSession::load(const std::string& dirpath)
{
    // 1. load repaly buffer
    std::fstream ifs(dirpath+"replay.dat", std::ifstream::in);
    if(ifs.is_open()) {
        ifs >> *replay;
        ifs.close();
        std::cout << "loaded old replay data: " << replay->get_buffer_size() << std::endl;
    }

    // 2. load network
    ifs.open(dirpath+"net.dat", std::ifstream::in);
    if(ifs.is_open()) {
        ifs >> *ddqn;
        ifs.close();
        std::cout << "loaded old net structure" << std::endl;
    }
}
