#ifndef SINGLEROBOTSESSION_H
#define SINGLEROBOTSESSION_H

#include "session.h"
#include "DQN/dqnenvironment.h"
#include "DQN/dqn.h"

class SingleRobotSession : public Session
{
public:
    SingleRobotSession(Environment* env, const std::string& log_dir = "", size_t cnt_save_steps = 2000);
    ~SingleRobotSession();

private:
    /* add objects to envrionment */
    void setup_environment(Environment* env);

    /* gets called after setup is finished */
    void on_start(size_t total_episodes);

    /* run neuronal net training here */
    void train(size_t& cnt_episodes);

    /* execture learned policy here */
    void execute();

    /* save all data */
    int save(const std::string& dirpath, bool all, size_t cnt_episodes = 0);

    /* load all data */
    int load(const std::string& dirpath);

private:
    dqn::DDQN *ddqn;
    dqn::Replay *replay;
    DQNEnvironment *agentenv;

    bool is_after_reset;
    size_t cnt_steps;
    float eps_discount, eps_f;

    dqn::action_t action;
    TypedEnvState<dqn::scalar_t> env_state;
    std::vector<dqn::scalar_t> state;
    std::vector<dqn::scalar_t> env_inital_state;

    float cumulated_reward;
    std::vector<float> rewards;
    std::vector<float> fitting_errs;
    std::vector<float> epsilons;
    std::vector<float> alphas;
    std::vector<int> succsess_rate;
    std::vector<int> num_of_steps;
    std::vector< std::vector<float> > q_value_start;
    std::vector< std::vector<float> > q_value_goal;
};

#endif // SINGLEROBOTSESSION_H
