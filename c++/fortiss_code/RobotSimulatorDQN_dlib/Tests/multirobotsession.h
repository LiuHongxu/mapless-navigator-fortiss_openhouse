#ifndef MULTIROBOTSESSION_H
#define MULTIROBOTSESSION_H

#include "session.h"
#include "DQN/dqnenvironment.h"
#include "DQN/dqn.h"

class MultiRobotSession : public Session
{
public:
    MultiRobotSession(Environment* env, const std::string& log_dir = "", size_t cnt_save_steps = 2000, size_t num_robots = 4);
    ~MultiRobotSession();

private:
    /* add objects to envrionment */
    void setup_environment(Environment* env);

    /* gets called after setup is finished */
    void on_start(size_t total_episodes);

    /* run neuronal net training here */
    void train(size_t& cnt_episodes);

    /* execture learned policy here */
    void execute();

    /* save all data */
    int save(const std::string& dirpath, bool all, size_t cnt_episodes = 0);

    /* load all data */
    int load(const std::string& dirpath);

private:
    // objects
    dqn::DDQN *ddqn;
    dqn::Replay *replay;
    std::vector<DQNEnvironment*> agentenvs;

    size_t num_robots;
    size_t cnt_steps;
    float eps_discount, eps_f;
    int observation_cnt;            // cnt number of observations

    // robot state variabels
    std::vector<bool> is_after_resets;
    std::vector<dqn::action_t> actions;
    std::vector<TypedEnvState<dqn::scalar_t>> env_states;
    std::vector<std::vector<dqn::scalar_t>> states;
    std::vector<std::vector<dqn::scalar_t>> env_inital_states;
    std::vector<b2Vec2> goal_positions;

    // monitor variabes
    float cumulated_reward;
    std::vector<std::vector<float>> rewards;
    std::vector<std::vector<int>> num_of_steps;
    std::vector<std::vector<int>> succsess_rate;
    std::vector<float> epsilons;
    std::vector<float> alphas;
    std::vector<float> fitting_errs;

    // random num generator
    std::random_device rd;  //  (seed) engine
    std::mt19937 rng;       // random-number engine
    std::uniform_int_distribution<int> int_uni;
};

#endif // MULTIROBOTSESSION_H
