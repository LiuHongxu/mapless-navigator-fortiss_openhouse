#include <QCoreApplication>
#include "Simulator/simconstants.h"
#include "Simulator/contactlistener.h"
#include "Simulator/environment/mazeenvironment.h"
#include "Simulator/environment/randomenvironment.h"
#include "Tests/singlerobotsession.h"
#include "Tests/multirobotsession.h"

int main(int argc, char *argv[])
{
    bool graphics = true;
    float realtime_factor = 0;

    // 1. create environment
    float loop_freq = 100.0f;
    RandomEnvironment environment(b2Vec2(0.0f, 0.0f), loop_freq);

    // 2. create learner
    SingleRobotSession session(&environment, "log/");
    session.set_mode(Session::Mode::TRAIN);
    session.setup();

    // 3. go!!!
    session.start(9000000);
    environment.start(true);
    while(environment.is_started()) {
        // 1. simulation step
        realtime_factor = environment.update();

        // 2. learning step
        if(session.update( ) == 1)
            environment.start(false);

        // 3. take user input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F1))
            graphics = true;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F2))
            graphics = false;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F3))
            session.save_all();
        if(graphics)
            environment.update_graphics();
    }
    // learning stopped, save all
    session.save_all();
    return 0;
}
