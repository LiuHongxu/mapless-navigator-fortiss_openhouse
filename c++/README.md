# Map-less Navigation with Obstacle avoidance

Contains the c++ version of the simulator. All project are build with qt-creator (see .pro files)
* [lecture_code](lecture_code) contains the old lecture code
* [fortiss_code](fortiss_code) contains the updated simulator 

## Delpendencies

* [matio](https://github.com/tbeu/matio)
* [Dlib](http://dlib.net/)
* [turtlebot packages](http://wiki.ros.org/turtlebot) (ros)
* [SFML](https://www.sfml-dev.org/)
* [Box2D](http://box2d.org/)

## Running
* see [readme](lecture_code/RobotSimulatorDQN_dlib/readme.md) to run the c++ code

## Overview

### Box2D-Simulation
In order to increase the Simulation speed we programmed a fast 2D robot
simulator in c++. It is based on the physics lirbary [Box2D](http://box2d.org/)
and uses [SFML](https://www.sfml-dev.org/) to produce graphical output.
The simulator is able to handle multible agents and dynamic obstacles in a
rectangular environment. See [here](RobotSimulatorDQN_dlib).

![](box2d_simulator.png "box2d-simulation")

### Q-Learning
In our first approch we used tabular Q-Learning. Although we reduced the possible number of state action combinations to 60.000 the agent was not able to learn the mapless navigation with obstacle avoidance problem. In order to get a sufficient small input space we used a five-dimensional input space consisting of 3 laser rays
(-45°, 0, 45°), the distance and orientation to the target.

### Double DQN
Our final approch uses a Double Deep Q-learning algorithm which successfully
solves Map-less Navigation with Obstacle avoidance task.

The input of the network is 20 dimensional vector that contains 18 laser rays,
the distance and orientation to the target. Two hidden layers with 256 and 128
relu-neurons estimate the Q-Values for each of the three possible actions.

![](dqn_architecture.png "DQN architecture")

The feedforward neuronal network is implemented with [Dlib](http://dlib.net/) which is a machine learning library for c++. 
The training runs on gpu (or on cpu if dlib is compiled this way) 
Learning progess is continously saved in a Mat file by the lirbary [matio](https://github.com/tbeu/matio).

### Hyperparameters
* minibatch size: 256
* replay memory size: 3000000
* target network update frequency: 5000 steps
* discount factor: 0.95
* action update frequency: 5Hz
* optimizer: adam
* learning rate: 0.0004
* loss function: huber loss
* inital exploration: 1
* final exploration: 0.1
* exploration discount: 0.999975

