# Pyhton version of simulator
* [backup](backup) contains old implementation
* [results](results) contains some test results and plots
* [robot_simulator](robot_simulator) contains the simulator code

## Installation
* Install tensorflow
* Install pygame: apt-get install build-essential python-dev swig python-pygame
* Install pybox2d: pip install Box2D

## Running
* see main for basic program structure
* edit training parameters in singlerobotsession.py
* set a training directory in singlerobotsession.py
* observe progress via tensorboard

## Structure
* ddqnenvironment.py defines functions like execute(), observe(), reset() and is used by singlerobotsession.py
* networks.py: builds tensorflow graph
* ddqn.py: used the network and implements double dqn
* singlerobotsession.py connects the learning algorithm with the simulator (steps the episodes)
* main.py launches the environment and singlerobotsession
