from dqn.networks import DQNNetwork
from dqn.networks import DUELDQNNetwork
import random
import numpy as np
import pickle
import tensorflow as tf
from dqn.sumtree import SumTree


# ----------------------------------------------------------------------------------------------------------------------
# Baseclass for replay buffers
# ----------------------------------------------------------------------------------------------------------------------
class ReplayBase:
    def __init__(self, buffer_size: int, state_size: int):
        # buffer
        self.state_buf = np.zeros((buffer_size, state_size), dtype=np.float32)
        self.action_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.reward_buf = np.zeros((buffer_size,), dtype=np.float32)
        self.state_next_buf = np.zeros((buffer_size, state_size), dtype=np.float32)
        self.done_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.buffer_size = buffer_size
        self._cnt = 0
        self.filled = False

    def is_full(self):
        return self.filled

    def save(self, file):
        """ save to file """
        out_file = open(file, 'wb')
        pickle.dump((self.state_buf,
                     self.action_buf,
                     self.reward_buf,
                     self.state_next_buf,
                     self.done_buf,
                     self._cnt,
                     self.filled), out_file)

    def load(self, file):
        """ load form file """
        in_file = open(file, 'rb')
        list_buf = pickle.load(in_file)
        self.state_buf = list_buf[0]
        self.action_buf = list_buf[1]
        self.reward_buf = list_buf[2]
        self.state_next_buf = list_buf[3]
        self.done_buf = list_buf[4]
        self._cnt = list_buf[5]
        self.filled = list_buf[6]


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves transitions
# ----------------------------------------------------------------------------------------------------------------------
class Replay(ReplayBase):
    """ Replay Buffer """
    def __init__(self, buffer_size: int, state_size: int):
        super(Replay, self).__init__(buffer_size, state_size)

    def add(self, state, action, reward, state_next, done, td = 0.0):
        """ add new transition to buffer """
        self.state_buf[self._cnt] = state
        self.action_buf[self._cnt] = action
        self.reward_buf[self._cnt] = reward
        self.state_next_buf[self._cnt] = state_next
        self.done_buf[self._cnt] = done

        self._cnt += 1
        if self._cnt >= self.buffer_size:
            self.filled = True
            self._cnt = 0

    def sample(self, batch_size: int):
        """
        sample uniformly form buffer, return batch_size elements
        :param batch_size: int
        :return: np array of batch_size x buffer_size
        """
        idx = np.random.randint(self.buffer_size, size=batch_size)
        return self.state_buf[idx], \
               self.action_buf[idx], \
               self.reward_buf[idx], \
               self.state_next_buf[idx], \
               self.done_buf[idx]


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves n-step transitions (using n-step rew for faster convergence of q-values)
# ----------------------------------------------------------------------------------------------------------------------
class NStepReplay(Replay):
    """ N-Step Replay Buffer """
    def __init__(self, buffer_size: int, state_size: int, n_steps: int, gamma: float):
        super(NStepReplay, self).__init__(buffer_size, state_size)
        self.n_steps = n_steps

        self.state_seq = np.zeros((n_steps, state_size), dtype=np.float32)
        self.action_seq = np.zeros((n_steps,), dtype=np.float32)
        self.reward_seq = np.zeros((n_steps,), dtype=np.float32)
        self.state_next_seq = np.zeros((n_steps, state_size), dtype=np.float32)
        self.td_seq = np.zeros((n_steps,), dtype=np.float32)
        self.done_seq = np.zeros((n_steps,), dtype=np.float32)

        self.start_idx = 0
        self.cur_idx = 0
        self.cur_size = 0
        self.R = 0.0
        self.gamma = gamma
        self.gamma_n = gamma ** n_steps

    def add(self, state, action, reward, state_next, done, td=0.0):
        """
        Add a new transition to sequence, update n-step reward
        If sequence buffer full or terminal state reached: update the replay buffer with n step transition
        """
        # 1. add new transition to intermediate sequenze buffer
        self.state_seq[self.cur_idx] = state
        self.action_seq[self.cur_idx] = action
        self.reward_seq[self.cur_idx] = reward
        self.state_next_seq[self.cur_idx] = state_next
        self.td_seq[self.cur_idx] = td
        self.done_seq[self.cur_idx] = done
        self.cur_idx += 1
        self.cur_size += 1

        # 2. update n-step cummulative reward ( recursive )
        self.R = (self.R + reward * self.gamma_n) / self.gamma

        # 3. empty sequence buffer if next state is a terminal state
        if done:
            while self.cur_size > 0:
                super(NStepReplay, self).add( self.state_seq[self.start_idx],
                                              self.action_seq[self.start_idx],
                                              self.R,
                                              self.state_next_seq[self.cur_idx - 1],
                                              self.done_seq[self.cur_idx - 1],
                                              self.td_seq[self.cur_idx - 1])

                self.R = (self.R - self.reward_seq[self.start_idx]) / self.gamma
                self.start_idx += 1
                self.cur_size -= 1
                self.start_idx = self.start_idx % self.n_steps
            self.R = 0.0

        # 4. start filling replay buffer if enough samples available to compute n-step reward
        if self.cur_size >= self.n_steps:
            super(NStepReplay, self).add(self.state_seq[self.start_idx],
                                         self.action_seq[self.start_idx],
                                         self.R,
                                         self.state_next_seq[self.cur_idx - 1],
                                         self.done_seq[self.cur_idx - 1],
                                         self.td_seq[self.cur_idx - 1])

            self.R = (self.R - self.reward_seq[self.start_idx]) / self.gamma
            self.start_idx += 1
            self.cur_size -= 1

        # 5. make sure index of circular buffer stayes within array range
        self.start_idx = self.start_idx % self.n_steps
        self.cur_idx = self.cur_idx % self.n_steps


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves individual samples
# ----------------------------------------------------------------------------------------------------------------------
class PriorizedReplay(Replay):
    def __init__(self, buffer_size, state_size, alpha = 0.5, epsilon = 0.01):
        super(PriorizedReplay, self).__init__(buffer_size, state_size)
        self.alpha = alpha
        self.epsilon = epsilon
        self.sumtree = SumTree(buffer_size)

    def add(self, state, action, reward, state_next, done, td = 0.0):
        # add to buffer
        super(PriorizedReplay, self).add(state, action, reward, state_next, td, done)
        # compute priority
        p = ( td + self.epsilon ) ** self.alpha
        # add to sumtree
        self.sumtree.add(p, self._cnt - 1)

    def sample(self, batch_size: int):
        data_indices = []
        indices = []

        segment = self.sumtree.total() / batch_size
        for i in range(batch_size):
            a = segment * i
            b = segment * (i + 1)
            s = random.uniform(a, b)
            idx, data_idx= self.sumtree.get(s)
            indices.append(idx)
            data_indices.append(data_idx)

        return self.state_buf[data_indices], \
               self.action_buf[data_indices], \
               self.reward_buf[data_indices], \
               self.state_next_buf[data_indices], \
               self.done_buf[data_indices], \
               indices

    def update(self, idx, td):
        # update priority
        p = (td + self.epsilon) ** self.alpha
        self.sumtree.update(idx, p)


# ----------------------------------------------------------------------------------------------------------------------
# Double Deep Q-Lerning algorithm
# ----------------------------------------------------------------------------------------------------------------------
class DDQN:
    def __init__(self, tf_sess, config):
        """
        Implementation of Double Qlearning algorithm
        """

        self.tf_sess = tf_sess
        self.mode = config['mode']

        # 1. set hyper parameters
        self.state_size = config['num_states']
        self.action_size = config['num_actions']
        self.buffer_size = config['buffer_size']
        self.epsilon = config['eps_start']
        self.alpha = config['alpha']
        self.gamma = config['gamma']
        self.target_update_steps = config['target_update_steps']
        self.online_update_steps = config['online_update_steps']
        self.batch_size = config['batch_size']
        self.n_steps = config['n_step_replay']

        # 1. create networks ( can be changed )
        self.model = DQNNetwork(self.tf_sess, self.state_size, self.action_size, self.alpha, "q")
        self.target_model = DQNNetwork(self.tf_sess, self.state_size, self.action_size, self.alpha, "target_q")

        # 3. setup replay buffer only if in training mode
        if self.mode == True:
            if self.n_steps > 1:
                self.replay = NStepReplay(self.buffer_size, self.state_size, self.n_steps, self.gamma)
            else:
                self.replay = Replay(self.buffer_size, self.state_size)
            self.use_per = isinstance(self.replay, PriorizedReplay)
        self.cnt = 0

    def learn(self, state, action, reward, next_state, done):
        """
        Insert new sample into replay buffer, do learning if buffer is completely filled
        """
        self.cnt += 1

        # 2. add sample to replay buffer, use reward as td error during filling phase
        if not self.replay.is_full():
            self.replay.add(state, action, reward, next_state, done, reward)
        else:
            if self.use_per:
                # compute td error for new sample TODO: get td errors!!!
                self.replay.add(state, action, reward, next_state, done, None)
            else:
                self.replay.add(state, action, reward, next_state, done)

            # 3. once replay buffer is filled, start with experience replay (udpate every online_update_steps)
            if self.cnt % self.online_update_steps == 0:
                return self._experience_replay()

        return -1.0

    def _experience_replay(self):
        """
        get batch form replay buffer, do gradient decent
        """
        # 0. Update target every swap steps
        if self.cnt % self.target_update_steps == 0:
            self._copy_model_parameters(self.model, self.target_model)

        # 1. sample targets form replay buffer
        if self.use_per:
            states, actions, rewards, states_next, dones, indices = self.replay.sample(self.batch_size)
        else:
            states, actions, rewards, states_next, dones = self.replay.sample(self.batch_size)

        # 2. forward pass to obtain estimation for state and next_state
        q_values_next = self.model.predict(states)
        q_values_next_t = self.target_model.predict(states_next)

        # 4. Q(s,a) = r + gamma * argmax_a Q(s', a)
        q_values_next = np.argmax(q_values_next, axis=1)
        end_multiplier = -(dones - 1)

        double_q = q_values_next_t[range(self.batch_size), q_values_next.reshape((self.batch_size))]
        targets = rewards + (self.gamma * double_q * end_multiplier)

        # 5. Update the model weights
        loss, td_err = self.model.optimize(states, actions, targets)

        # 6. Update the priorization for per
        if self.use_per:
            for i in range(self.batch_size):
                self.replay.update(indices[i], td_err[i])

        return loss

    def generate_action(self, state):
        """
        get eps-greedy actions
        """
        if random.uniform(0, 1) < self.epsilon:
            return random.randint(0, self.action_size - 1)
        else:
            state_np = np.array(state, dtype=np.float32).reshape((1, self.state_size))
            return np.argmax(self.model.predict(state_np))

    def _copy_model_parameters(self, model_1, model_2):
        """
        copy model parameters form model 1 to 2
        """
        e1_params = [t for t in tf.trainable_variables() if t.name.startswith(model_1.scope)]
        e1_params = sorted(e1_params, key=lambda v: v.name)
        e2_params = [t for t in tf.trainable_variables() if t.name.startswith(model_2.scope)]
        e2_params = sorted(e2_params, key=lambda v: v.name)
        update_ops = []
        for e1_v, e2_v in zip(e1_params, e2_params):
            op = e2_v.assign(e1_v)
            update_ops.append(op)

        self.tf_sess.run(update_ops)

