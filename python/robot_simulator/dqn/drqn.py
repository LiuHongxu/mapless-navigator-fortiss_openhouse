from dqn.networks import DRQNNetwork
import random
import numpy as np
import pickle
import tensorflow as tf

# ----------------------------------------------------------------------------------------------------------------------
# Baseclass for replay buffers
# ----------------------------------------------------------------------------------------------------------------------
class ReplayBase:
    def __init__(self, buffer_size: int, state_size: int):
        # buffer
        self.state_buf = np.zeros((buffer_size, state_size), dtype=np.float32)
        self.action_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.reward_buf = np.zeros((buffer_size,), dtype=np.float32)
        self.state_next_buf = np.zeros((buffer_size, state_size), dtype=np.float32)
        self.done_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.collision_buf = np.zeros((buffer_size,), dtype=np.int32)
        self.buffer_size = buffer_size
        self._cnt = 0
        self.filled = False

    def is_full(self):
        return self.filled

    def save(self, file):
        """ save to file """
        out_file = open(file, 'wb')
        pickle.dump((self.state_buf,
                     self.action_buf,
                     self.reward_buf,
                     self.state_next_buf,
                     self.done_buf,
                     self.collision_buf,
                     self._cnt,
                     self.filled), out_file)

    def load(self, file):
        """ load form file """
        in_file = open(file, 'rb')
        list_buf = pickle.load(in_file)
        self.state_buf = list_buf[0]
        self.action_buf = list_buf[1]
        self.reward_buf = list_buf[2]
        self.state_next_buf = list_buf[3]
        self.done_buf = list_buf[4]
        self.collision_buf = list_buf[5]
        self._cnt = list_buf[6]
        self.filled = list_buf[7]


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves transitions
# ----------------------------------------------------------------------------------------------------------------------
class Replay(ReplayBase):
    """ Replay Buffer """
    def __init__(self, buffer_size: int, batch_size: int, state_size: int, seq_len: int):
        super(Replay, self).__init__(buffer_size, state_size)

        self.seq_len = seq_len
        self.batch_size = batch_size

        self.state_seq_batch = np.zeros((batch_size * seq_len, state_size), dtype=np.float32)
        self.action_seq_batch = np.zeros((batch_size * seq_len,), dtype=np.float32)
        self.reward_seq_batch = np.zeros((batch_size * seq_len,), dtype=np.float32)
        self.state_next_seq_batch = np.zeros((batch_size * seq_len, state_size), dtype=np.float32)
        self.done_seq_batch = np.zeros((batch_size * seq_len,), dtype=np.int32)

    def add(self, state, action, reward, state_next, done, collision):
        """ add new transition to buffer """
        self.state_buf[self._cnt] = state
        self.action_buf[self._cnt] = action
        self.reward_buf[self._cnt] = reward
        self.state_next_buf[self._cnt] = state_next
        self.done_buf[self._cnt] = done
        self.collision_buf[self._cnt] = collision

        self._cnt += 1
        if self._cnt >= self.buffer_size:
            self.filled = True
            self._cnt = 0

    def sample(self):
        i = 0
        while i < self.batch_size:
            idx = random.randint(0, self.buffer_size - self.seq_len - 1)

            seq_idx = range(idx, idx + self.seq_len)
            seq_range = range(i * self.seq_len, (i + 1) * self.seq_len)

            self.state_seq_batch[seq_range] = self.state_buf[seq_idx]
            self.action_seq_batch[seq_range] = self.action_buf[seq_idx]
            self.reward_seq_batch[seq_range] = self.reward_buf[seq_idx]
            self.state_next_seq_batch[seq_range] = self.state_next_buf[seq_idx]
            self.done_seq_batch[seq_range] = self.done_buf[seq_idx]

            if self.collision_buf[seq_idx].any():
                end_idx = i * self.seq_len + self.collision_buf[seq_idx].argmax()
                self.state_seq_batch[end_idx: (i + 1) * self.seq_len] = self.state_seq_batch[end_idx]
                self.reward_seq_batch[end_idx: (i + 1) * self.seq_len] = self.reward_seq_batch[end_idx]
                self.state_next_seq_batch[end_idx: (i + 1) * self.seq_len] = self.state_seq_batch[end_idx]
                self.done_seq_batch[end_idx: (i + 1) * self.seq_len] = self.done_seq_batch[end_idx]

            i += 1

        return self.state_seq_batch, \
               self.action_seq_batch, \
               self.reward_seq_batch, \
               self.state_next_seq_batch, \
               self.done_seq_batch


# ----------------------------------------------------------------------------------------------------------------------
# Double Deep Q-Lerning algorithm
# ----------------------------------------------------------------------------------------------------------------------
class DRQN:
    def __init__(self, tf_sess, config):
        """
        Implementation of Double Qlearning algorithm
        """

        self.tf_sess = tf_sess

        # 1. set hyper parameters
        self.state_size = config['num_states']
        self.action_size = config['num_actions']
        self.buffer_size = config['buffer_size']
        self.epsilon = config['eps_start']
        self.alpha = config['alpha']
        self.gamma = config['gamma']
        self.target_update_steps = config['target_update_steps']
        self.online_update_steps = config['online_update_steps']
        self.batch_size = config['batch_size']
        self.sequenze_length = config['sequenze_length']
        self.grad_used_fraction = config['grad_used_fraction']
        self.n_steps = config['n_step_replay']

        # 1. create networks ( can be changed )
        self.model = DRQNNetwork(self.tf_sess, self.state_size, self.action_size, self.alpha, "q")
        self.target_model = DRQNNetwork(self.tf_sess, self.state_size, self.action_size, self.alpha, "target_q")
        self.rnn_state_online = (np.zeros([1, 256]), np.zeros([1, 256]))
        self.rnn_state_train = (np.zeros([self.batch_size, 256]), np.zeros([self.batch_size, 256]))

        # 3. setup replay buffer
        self.replay = Replay(self.buffer_size, self.batch_size, self.state_size, self.sequenze_length)
        self.cnt = 0

    def learn(self, state, action, reward, next_state, done):
        """
        Insert new sample into replay buffer, do learning if buffer is completely filled
        """
        self.cnt += 1

        # 2. add sample to replay buffer, use reward as td error during filling phase
        collision = done & (reward < 0)
        if collision:
            self.rnn_state_online = (np.zeros([1, 256]), np.zeros([1, 256]))

        self.replay.add(state, action, reward, next_state, done, collision)

        # 3. once replay buffer is filled, start with experience replay (udpate every online_update_steps)
        if self.replay.is_full():
            if self.cnt % self.online_update_steps == 0:
                return self._experience_replay()

        return -1.0

    def _experience_replay(self):
        """
        get batch form replay buffer, do gradient decent
        """
        # 0. Update target every swap steps
        if self.cnt % self.target_update_steps == 0:
            self._copy_model_parameters(self.model, self.target_model)

        # 1. sample targets form replay buffer
        states, actions, rewards, states_next, dones = self.replay.sample()

        # 2. forward pass to obtain estimation for state and next_state
        q_values_next, _ = self.model.predict(states, self.rnn_state_train, self.batch_size, self.sequenze_length)
        q_values_next_t, _ = self.target_model.predict(states_next, self.rnn_state_train, self.batch_size, self.sequenze_length)

        # 4. Q(s,a) = r + gamma * argmax_a Q(s', a)
        q_values_next = np.argmax(q_values_next, axis=1)
        end_multiplier = -(dones - 1)

        double_q = q_values_next_t[range(self.batch_size * self.sequenze_length), q_values_next.reshape((self.batch_size * self.sequenze_length))]
        targets = rewards + (self.gamma * double_q * end_multiplier)

        # 5. Update the model weights
        loss, td_err = self.model.optimize(states, self.rnn_state_train, actions, targets, self.batch_size, self.sequenze_length, self.grad_used_fraction)
        return loss

    def generate_action(self, state):
        """
        get eps-greedy actions
        """
        if random.uniform(0, 1) < self.epsilon:
            return random.randint(0, self.action_size - 1)
        else:
            state_np = np.array(state, dtype=np.float32).reshape((1, self.state_size))
            q_values, self.rnn_state_online = self.model.predict(state_np, self.rnn_state_online, 1, 1)
            return np.argmax(q_values)

    def _copy_model_parameters(self, model_1, model_2):
        """
        copy model parameters form model 1 to 2
        """
        e1_params = [t for t in tf.trainable_variables() if t.name.startswith(model_1.scope)]
        e1_params = sorted(e1_params, key=lambda v: v.name)
        e2_params = [t for t in tf.trainable_variables() if t.name.startswith(model_2.scope)]
        e2_params = sorted(e2_params, key=lambda v: v.name)
        update_ops = []
        for e1_v, e2_v in zip(e1_params, e2_params):
            op = e2_v.assign(e1_v)
            update_ops.append(op)

        self.tf_sess.run(update_ops)

