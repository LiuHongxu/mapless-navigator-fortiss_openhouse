import tensorflow as tf
import numpy as np

# ----------------------------------------------------------------------------------------------------------------------
# DQN Network
#
# Structure: state_size -> 128 -> 64 -> 32 -> 16 -> action_size
# Input: state (batch_size x state_size)
# Output: qvalues (batch_size x action_size)
# ----------------------------------------------------------------------------------------------------------------------
class DQNNetwork:
    """Q-Value Estimator neural network"""
    def __init__(self, sess: tf.Session, state_size: int, action_size: int, alpha: float, scope="dqn"):
        """
        :param sess: tensorflow session
        :param state_size: state dimension
        :param action_size: number of actions
        :param alpha: learning rate
        :param scope: name of tf scope
        """
        self.sess = sess
        self.state_size = state_size
        self.action_size = action_size
        self.alpha = alpha
        self.scope = scope

        with tf.variable_scope(scope):
            self._build_model()

    def _build_model(self):

        # define inputs placeholder
        self.states_pl = tf.placeholder(shape=[None, self.state_size], dtype=tf.float32)        # states
        self.a_pl = tf.placeholder(shape=[None], dtype=tf.int32)                                # action

        x = self.states_pl
        x = tf.layers.dense(x, 256, activation=tf.nn.relu)
        x = tf.layers.dense(x, 128, activation=tf.nn.relu)
        x = tf.layers.dense(x, 64, activation=tf.nn.relu)
        x = tf.layers.dense(x, 32, activation=tf.nn.relu)

        # estimation of q values for all posible actions
        self.Q = tf.layers.dense(x, self.action_size, activation=None)

        # estimation of q value for selected action
        actions_onehot = tf.one_hot(self.a_pl, self.action_size, dtype=tf.float32)
        self.q = tf.reduce_sum(tf.multiply(self.Q, actions_onehot), axis=1)

        # huber loss for optimization
        self.targets = tf.placeholder(shape=[None], dtype=tf.float32)
        self.td_err = (self.targets - self.q)
        self.loss = tf.reduce_mean( tf.where(tf.abs(self.td_err) < 1.0, 0.5 * tf.square(self.td_err), tf.abs(self.td_err) - 0.5) )
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.alpha).minimize(self.loss)

    def predict(self, state: np.ndarray):
        """ Run forward pass from state to q_values """
        return self.sess.run(self.Q, {self.states_pl: state})

    def optimize(self, batch_state: np.ndarray, batch_action: np.ndarray, batch_q_target: np.ndarray):
        """ backward pass on batch"""
        feed_dict = {self.states_pl: batch_state, self.a_pl: batch_action, self.targets: batch_q_target}
        loss, td_err, _ = self.sess.run([self.loss, self.td_err, self.optimizer], feed_dict)
        return loss, td_err

# ----------------------------------------------------------------------------------------------------------------------
# DQN Network
#
# Structure: state_size -> 128 -> 64 -> 32 -> 16 -> action_size
# Input: state (batch_size x state_size)
# Output: qvalues (batch_size x action_size)
# ----------------------------------------------------------------------------------------------------------------------
class DUELDQNNetwork:
    """Q-Value Estimator neural network"""

    def __init__(self, sess: tf.Session, state_size: int, action_size: int, alpha: float, scope="dqn"):
        """
        :param sess: tensorflow session
        :param state_size: state dimension
        :param action_size: number of actions
        :param alpha: learning rate
        :param scope: name of tf scope
        """
        self.sess = sess
        self.state_size = state_size
        self.action_size = action_size
        self.alpha = alpha
        self.scope = scope

        with tf.variable_scope(scope):
            self._build_model()

    def _build_model(self):
        # define inputs placeholder
        self.states_pl = tf.placeholder(shape=[None, self.state_size], dtype=tf.float32)  # states
        self.a_pl = tf.placeholder(shape=[None], dtype=tf.int32)  # action

        x = self.states_pl
        x = tf.layers.dense(x, 128, activation=tf.nn.relu)
        x = tf.layers.dense(x, 64, activation=tf.nn.relu)
        x = tf.layers.dense(x, 32, activation=tf.nn.relu)

        # spit stream into value and advantage estimation
        inital_value = tf.contrib.layers.xavier_initializer()
        self.a_weights = tf.Variable(inital_value([32, self.action_size]))
        self.v_weights = tf.Variable(inital_value([32, 1]))
        self.advantage = tf.matmul(x, self.a_weights)
        self.value = tf.matmul(x, self.v_weights)

        # estimation of q values for all posible actions
        self.Q = self.value + tf.subtract( self.advantage, tf.reduce_mean(self.advantage, axis=1, keep_dims=True))

        # estimation of q value for selected action
        actions_onehot = tf.one_hot(self.a_pl, self.action_size, dtype=tf.float32)
        self.q = tf.reduce_sum(tf.multiply(self.Q, actions_onehot), axis=1)

        # huber loss for optimization
        self.targets = tf.placeholder(shape=[None], dtype=tf.float32)
        self.td_err = (self.targets - self.q)
        self.loss = tf.reduce_mean( tf.where(tf.abs(self.td_err) < 1.0, 0.5 * tf.square(self.td_err), tf.abs(self.td_err) - 0.5) )
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.alpha).minimize(self.loss)

    def predict(self, state: np.ndarray):
        """ Run forward pass from state to q_values """
        return self.sess.run(self.Q, {self.states_pl: state})

    def optimize(self, batch_state: np.ndarray, batch_action: np.ndarray, batch_q_target: np.ndarray):
        """ backward pass on batch"""
        feed_dict = {self.states_pl: batch_state, self.a_pl: batch_action, self.targets: batch_q_target}
        loss, td_err, _ = self.sess.run([self.loss, self.td_err, self.optimizer], feed_dict)
        return loss, td_err


# ----------------------------------------------------------------------------------------------------------------------
# DRQN Network
#
# Structure: state_size -> 128 -> 64 -> 32 -> lstm(32) -> action_size
# Input: state (batch_size * sequence_len, state_size)
# Output: qvalues (batch_size * action_size,)
# ----------------------------------------------------------------------------------------------------------------------
class DRQNNetwork:
    def __init__(self, sess: tf.Session, state_size: int, action_size: int, alpha: float, scope="dqn"):
        """
        :param sess: tensorflow session
        :param state_size: state dimension
        :param action_size: number of actions
        :param sequence_len: sequence_len
        :param scope: name of tf scope
        :param alpha: learning rate
        :param scope: name of tf scope
        """
        self.sess = sess
        self.state_size = state_size
        self.action_size = action_size
        self.alpha = alpha
        self.scope = scope

        with tf.variable_scope(scope):
            self._build_model()

    def _build_model(self):
        self.batch_size_pl = tf.placeholder(tf.int32, shape=(), name="batch_size")
        self.seq_len_pl = tf.placeholder(tf.int32, shape=(), name="seq_len")
        self.grad_used_size_pl = tf.placeholder(tf.int32, shape=(), name="grad_used_size")
        self.grad_unused_size_pl = tf.placeholder(tf.int32, shape=(), name="grad_unused_size")

        self.states_pl = tf.placeholder(shape=[None, self.state_size], dtype=tf.float32)     # states
        self.a_pl = tf.placeholder(shape=[None], dtype=tf.int32)                             # action

        # 1. feature extraction part
        x = self.states_pl
        x = tf.layers.dense(x, 256, activation=tf.nn.relu)

        # 2. lstm
        x_seq = tf.reshape(x, shape=[self.batch_size_pl, self.seq_len_pl, 256])

        self.cell = tf.contrib.rnn.LSTMCell(256)
        self.rnn_state_in = self.cell.zero_state(self.batch_size_pl, dtype=tf.float32)
        x, self.rnn_state_out = tf.nn.dynamic_rnn(
            self.cell,
            x_seq,
            initial_state=self.rnn_state_in,
            dtype=tf.float32)

        x = tf.reshape(x, [-1, 256])

        x = tf.layers.dense(x, 64, activation=tf.nn.relu)
        x = tf.layers.dense(x, 32, activation=tf.nn.relu)

        # estimation of q values for all possible actions
        self.Q = tf.layers.dense(x, self.action_size, activation=None)

        # estimation of q value for selected action
        actions_onehot = tf.one_hot(self.a_pl, self.action_size, dtype=tf.float32)
        self.q = tf.reduce_sum(tf.multiply(self.Q, actions_onehot), axis=1)

        # 5. mse loss for optimization
        # only the last fraction of gradients is used for backprop
        mask_unused = tf.zeros([self.batch_size_pl, self.grad_unused_size_pl])
        mask_used = tf.ones([self.batch_size_pl, self.grad_used_size_pl])
        mask = tf.concat([mask_unused, mask_used], 1)
        self.mask = tf.reshape(mask, [-1])  # (batch * trace_len,)

        self.targets = tf.placeholder(shape=[None], dtype=tf.float32)
        self.td_err = (self.targets - self.q) * self.mask
        self.loss = tf.reduce_mean(tf.where(tf.abs(self.td_err) < 1.0, 0.5 * tf.square(self.td_err), tf.abs(self.td_err) - 0.5))
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.alpha).minimize(self.loss)

    def predict(self, state: np.ndarray, rnn_state_in, batch_size: int, seq_len: int):
        """
        predict Q Values
        :param state: input state matrix of shape (batch_size * sequence_len, state_size)
        :param rnn_state_in: state used to initialize lstm
        :param batch_size: number of samples in state
        :return:
        """
        feed_dict = {self.states_pl: state,
                     self.rnn_state_in: rnn_state_in,
                     self.batch_size_pl: batch_size,
                     self.seq_len_pl: seq_len }
        return self.sess.run([self.Q, self.rnn_state_out], feed_dict)

    def optimize(self, batch_state: np.ndarray, rnn_state_in, batch_action: np.ndarray,
                 batch_q_target: np.ndarray, batch_size: int, seq_len: int, grad_used: float):
        """
        backward pass on batch
        :param batch_state: batch of states, shape (batch_size * sequence_len, state_size)
        :param rnn_state_in: state used to initialize lstm
        :param batch_action: actions, shape (batch_size * sequence_len, 1)
        :param batch_q_target: targets, shape (batch_size * sequence_len, 1)
        :param batch_size: number of samples in state
        :return: mean batch loss
        """
        feed_dict = {self.states_pl: batch_state,
                     self.rnn_state_in: rnn_state_in,
                     self.a_pl: batch_action,
                     self.targets: batch_q_target,
                     self.batch_size_pl: batch_size,
                     self.seq_len_pl: seq_len,
                     self.grad_used_size_pl: int(seq_len * grad_used),
                     self.grad_unused_size_pl: seq_len - int(seq_len * grad_used) }
        loss, td_err, _ = self.sess.run([self.loss, self.td_err, self.optimizer], feed_dict)
        return loss, td_err
