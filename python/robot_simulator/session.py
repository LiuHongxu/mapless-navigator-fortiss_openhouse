import os
import tensorflow as tf

TRAIN_MODE = True
EXEC_MODE = False


# ----------------------------------------------------------------------------------------------------------------------
# Base Class of a Session
# ----------------------------------------------------------------------------------------------------------------------
class Session:
    """ setup of file structure and tf session, defines function that must be """
    def __int__(self, env, log_dir, name, save_steps, total_episodes, mode=TRAIN_MODE):
        self.env = env
        self.log_dir = log_dir
        self.name = name
        self.save_steps = save_steps
        self.total_episodes = total_episodes
        self.is_running = False
        self.mode = mode
        self.cnt_episodes = 0
        self.prev_save_steps = 0

        # 1. create all directories
        self.session_dir = os.path.join(log_dir, name)
        self.plot_dir = os.path.join(self.session_dir, "plots")
        self.checkpoint_dir = os.path.join(self.session_dir, "checkpoints")
        self.checkpoint_path = os.path.join(self.checkpoint_dir, "model")
        summary_dir = os.path.join(self.session_dir, "summary")
        if not os.path.exists(self.plot_dir):
            os.makedirs(self.plot_dir)
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)
        if not os.path.exists(summary_dir):
            os.makedirs(summary_dir)

        # 2. create tf session
        self.tf_sess = tf.Session()

        # 3. setup a summary writer
        self.summary_writer = tf.summary.FileWriter(summary_dir)

    def _setup_environment(self, env):
        """
        setup all environment objects in env
        :param env: environment object
        :return:
        """
        raise NotImplementedError

    def _on_start(self, total_episodes):
        """
        called if a session starts
        :param total_episodes: number of total_episodes (to resize arrays)
        :return:
        """
        raise NotImplementedError

    def _load(self, session_dir):
        """
        called if model must be loaded
        :param session_dir: directory of current session
        :return:
        """
        # Load a previous checkpoint if we find one
        latest_checkpoint = tf.train.latest_checkpoint(self.checkpoint_dir)
        if latest_checkpoint:
            print("Loading model checkpoint {}...\n".format(latest_checkpoint))
            self.tf_saver.restore(self.tf_sess, latest_checkpoint)
        else:
            print("no checkpoint found")

    def _save(self, session_dir, do_save_all, cnt_episodes):
        """
        called if model must be saved
        :param session_dir: directory of current session
        :param do_save_all: flag to indicate what must be saved
        :param cnt_episodes: current episode number
        :return:
        """
        self.tf_saver.save(self.tf_sess, self.checkpoint_path, global_step=cnt_episodes)

    def _plot(self, plot_dir, cnt_episodes):
        """
        called if data should be plotted
        :param plot_dir:
        :return:
        """
        raise NotImplementedError

    def finished(self):
        # 1. save everything
        if self.mode:
            self._save(self.session_dir, True, self.cnt_episodes)
        # 2. plot everything
        self._plot(self.plot_dir, self.cnt_episodes)
        # 3. close session
        self.tf_sess.close()
        tf.reset_default_graph()

    def _train_step(self, cnt_episodes):
        """
        called to do one training step (learning)
        :param cnt_episodes: current episode number
        :return:
        """
        raise NotImplementedError

    def _exec_step(self, cnt_episodes):
        """
        called to do one execution step (no learning)
        :return:
        """
        raise NotImplementedError

    def setup(self):
        # 1. setup environment
        self._setup_environment(self.env)

        self.tf_sess.run(tf.global_variables_initializer())

        # 2. try to load prev saved data
        self.tf_saver = tf.train.Saver()
        self._load(self.session_dir)

    def start(self):
        if not self.is_running:
            print('--- Start Session: ' + self.name + ' ---')
            self.is_running = True
            self.cnt_episodes = 0
            # call on_start func
            self._on_start(self.total_episodes)

    def update(self):
        if not self.is_running:
            return -1
        else:
            if self.mode == TRAIN_MODE:
                # Time to save parameters
                if self.cnt_episodes % self.save_steps == 0 and self.cnt_episodes != self.prev_save_steps:
                    self.prev_save_steps = self.cnt_episodes
                    self._save(self.session_dir, False, self.cnt_episodes)

                # Session finished
                if self.cnt_episodes >= self.total_episodes:
                    self._save(self.session_dir, True, self.cnt_episodes)
                    self.is_running = False
                    return 1
                # do training
                self.cnt_episodes = self._train_step(self.cnt_episodes)
            else:
                # do execution
                self.cnt_episodes = self._exec_step(self.cnt_episodes)
        return 0



