# Force tf to switch to cpu instead of graphic card (but its slower...)
# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from sessions.singlerobotsession import *
from simulator.randomenvironment import *

def main():
    # 1. create environment
    # creates the box2d engine, opens a pygame window
    environment = RandomEnvironment(b2Vec2(0.0, 0.0), loop_frequency=30)

    # 2. run a simple gird search on the reward function (remove this loop for final version)
    for i in range(1, 2):
        # step some parameters
        Config['rew_dist'] = random.uniform(5.0, 30.0)
        Config['rew_step'] = random.uniform(0.2, 1.2)
        Config['rew_potential'] = random.uniform(0.0, 10.0)
	
	# setup a training session (change parameters there)
        session = SingleRobotDQN(environment, i)
        session.setup()

        # 3. go!!!
        session.start()
        environment.start(True)

	# 4. loop
        while(environment.is_started):
            # 1. simulate step
            environment.update()

            # 2. session update
            if session.update() == 1:
                environment.start(False)

            # 3. show graphics
            environment.update_graphics()

        # 4. finished, save everything
        session.finished()
        environment.clear()

if __name__ == "__main__":
    main()
