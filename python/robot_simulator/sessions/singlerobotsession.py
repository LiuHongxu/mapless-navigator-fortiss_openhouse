import sys
from session import *
from simulator.robot import *
from dqn.dqnenvironment import *
import pickle
import json
import matplotlib.pyplot as plt
from dqn.ddqn import DDQN
from dqn.drqn import DRQN

# ----------------------------------------------------------------------------------------------------------------------
# Helper functions
# ----------------------------------------------------------------------------------------------------------------------
def printf(format, *args):
    sys.stdout.write(format % args)

def moving_average(a, n=10) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def episode_plot(data_list, xlabel, ylabel, title, dir_path):
    fig, ax = plt.subplots()
    for i in range(0, len(data_list)):
        data = data_list[i]
        ax.plot(data[0], data[1])
    ax.set(xlabel=xlabel, ylabel=ylabel, title=title)
    ax.grid()
    fig.savefig(dir_path + '/' + title + '.png')

# ----------------------------------------------------------------------------------------------------------------------
# Configuration of the session (edit parameters here)
# ----------------------------------------------------------------------------------------------------------------------
Config = {
    'mode': EXEC_MODE, #TRAIN_MODE						# switch training / execution mode
    'log_dir': '/home/simon/python/map-less-navigation/results',		# the base folder of all results
    'session_dir': 'single_robot_dqn/dqn_base_net',				# the currently used sub directory
    'log_interval': 500,							# number of steps before saveing
    'total_num_episodes': 120000,						# total episodes before stop
    'num_states': 20,								# state space dim (edit the simulator if this must be changed!)
    'num_actions': 3,								# number of actions (edit the simulator if this must be changed!)
    'buffer_size': 1000000,							# replay buffer size
    'eps_final': 0.15,								# exploration final value ( not zeros, otherwise might get stucked)		
    'eps_start': 0.15,								# exploration start value ( something around 0.8)
    'eps_discount': 0.99988,							# discound applied to eps in each step (depends on total_num_episodes )
    'alpha': 0.0004,								# learning rate
    'gamma': 0.99,								# discound fac
    'target_update_steps': 5000,						# target network update steps
    'online_update_steps': 1,							# number of steps between online network update
    'batch_size': 256,								# batch size sampled from replay buffer
    'n_step_replay': 1,								# sample n steps that occured after each other (trajectories) 
    'sequenze_length': 16,							# sequenze lenght (only relevant if recurrent net is used)
    'grad_used_fraction': 0.8,							

    'min_goal_dist': 0.25,							# min dist to goal befor success
    'min_obstical_dist': 0.22,  # 0.22						# min dist to obstacle befor failor
    'n_rays': 18,								# n of rays (edit the simulator if this must be changed!)
    'max_range': 2.0,								# max ray range in meter
    'use_random_agent_pos': True,						# start at random position in each episode
    'use_random_agent_goal_pos': False,						# set random goal position in each episode
    'rew_dist': 15.0,								# max reward for distance to obstacles
    'rew_step': 0.4,								# max reward for single step
    'rew_potential': 3.0							# max reward for potential field to obstacles
}

# ----------------------------------------------------------------------------------------------------------------------
# Single Robot Session using DDQN
# ----------------------------------------------------------------------------------------------------------------------
class SingleRobotDQN(Session):
    """ Implmentation of a Training Session
        Connects reinforcemnet learning algorithm with simuator,
        Loads and saves the session in a spcified directory 
	Baseclass: Session """

    def __init__(self, env, id, mode=TRAIN_MODE):
        super(SingleRobotDQN, self).__int__(env, Config['log_dir'],
                                            Config['session_dir'] + '_' + str(id),
                                            Config['log_interval'],
                                            Config['total_num_episodes'],
                                            Config['mode'])

        # Control the session
        self.n_state = Config['num_states']
        self.n_action = Config['num_actions']
        self.buffer_size = Config['buffer_size']
        self.eps_final = Config['eps_final']
        self.eps_discount = Config['eps_discount']

        # create the learning algorithm
        self.ddqn = DDQN(self.tf_sess, Config)

        # internal var
        self.cnt_steps = 0
        self.cnt_training_start = 0
        self.cumulated_reward = 0
        self.mean_loss = 0
        self.action = 0
        self.state = np.zeros([self.n_state, ])
        self.env_state = []

    # setup all environment objects and algorithms
    def _setup_environment(self, env):
        # 1. build world
        env.add( StaticCircle(b2Vec2(2.5, 2.5), 0.15))
        env.add( StaticCircle(b2Vec2(4.0, 1.0), 0.2) )
        env.add( StaticCircle(b2Vec2(2.0, 4.0), 0.3) )
        env.add( StaticCircle(b2Vec2(2.0, 4.0), 0.3) )
        env.add( StaticRectangle(b2Vec2(3, 3), b2Vec2(0.25, 0.7)))
        env.add( StaticRectangle(b2Vec2(1, 4), b2Vec2(0.4, 0.7)))
        env.add( StaticRectangle(b2Vec2(1, 4), b2Vec2(0.3, 0.5)))
        env.add( StaticRectangle(b2Vec2(1, 4), b2Vec2(0.3, 0.5)))
        env.add(StaticRectangle(b2Vec2(1, 4), b2Vec2(0.4, 0.7)))
        env.add(StaticRectangle(b2Vec2(1, 4), b2Vec2(0.3, 0.5)))
        env.set_walls()

        # 2. build agent
        robot = Robot( b2Vec2(3.0, 3.0), 0.0, 0.175 )
        env.add(robot)
        self.agentenv = DQNEnvironment(self.env, robot, b2Vec2(3.5, 3.5), Config)

    # called if a session starts
    def _on_start(self, total_episodes):
        # 0. resize monitor variables
        self.rewards_vec = np.zeros((total_episodes,))
        self.epsilon_vec = np.zeros((total_episodes,))
        self.success_rate_vec = np.zeros((total_episodes,))
        self.num_steps_vec = np.zeros((total_episodes,))
        self.goal_loss_vec = np.zeros((total_episodes,))
        self.mean_loss_vec = np.zeros((total_episodes,))
        self.num_of_actions_vec = np.zeros((total_episodes, self.n_action))

        # 1. reset everything before training
        self.cnt_steps = 0
        self.cumulated_reward = 0

        # 2. reset and get an initial observation
        self.env_state = self.agentenv.reset()
        self.state[:] = self.env_state.state[:]
        self.action = self.ddqn.generate_action(self.state)

        printf('Session: ' + self.name + ' started')

    # called in every training step
    def _train_step(self, cnt_episodes):
        # 1. do observation
        # If agentenv has no new sensor information (lidar has 5Hz): repeat the same action once more and start again
        env_state = self.agentenv.observe()
        if env_state is None:
            self.agentenv.execute(self.action)
            return cnt_episodes

        # 2. learn
        # Transition: state, action, reward, next_state
        loss = self.ddqn.learn( self.state, self.action, env_state.reward, env_state.state, env_state.done )
        self.mean_loss += loss

        # 3. transition to next state
        self.state[:] = env_state.state[:]
        self.cumulated_reward += env_state.reward

        # 4. execute next action
        self.action = self.ddqn.generate_action(self.state)
        self.agentenv.execute(self.action)

        self.cnt_steps += 1
        self.num_of_actions_vec[cnt_episodes][self.action] += 1

        # 6 check if episode is finished
        if env_state.done or self.cnt_steps > 1000:
            if self.cnt_training_start == 0:
                self.cnt_training_start = cnt_episodes
            self.mean_loss = self.mean_loss / self.cnt_steps

            # print user info
            if loss < 0:
                printf("Collecting: EP: %d, steps: %d, eps: %4.5f, rew: %4.2f\n",
                       cnt_episodes, self.cnt_steps, self.ddqn.epsilon, self.cumulated_reward)
            else:
                printf("Replaying: EP: %d, steps: %d, eps: %4.5f, rew: %4.2f, loss: %4.2f\n",
                       cnt_episodes, self.cnt_steps, self.ddqn.epsilon, self.cumulated_reward, self.mean_loss)

            # exponential decal of epsilon
            if self.ddqn.epsilon > self.eps_final and loss > 0:
                self.ddqn.epsilon *= self.eps_discount

            # save monitor variables
            self.rewards_vec[cnt_episodes] = self.cumulated_reward
            self.mean_loss_vec[cnt_episodes] = self.mean_loss
            self.goal_loss_vec[cnt_episodes] = loss
            self.epsilon_vec[cnt_episodes] = self.ddqn.epsilon
            self.success_rate_vec[cnt_episodes] = self.env_state.success
            self.num_steps_vec[cnt_episodes] = self.cnt_steps

            # add summaries to tensorboard
            episode_summary = tf.Summary()
            episode_summary.value.add(simple_value=self.cumulated_reward, node_name="cumulated_reward",
                                      tag="cumulated_reward")
            episode_summary.value.add(simple_value=self.mean_loss, node_name="mean_loss",
                                      tag="mean_loss")
            episode_summary.value.add(simple_value=loss, node_name="goal_loss",
                                      tag="goal_loss")
            episode_summary.value.add(simple_value=self.ddqn.epsilon, node_name="epsilon",
                                      tag="epsilon")
            episode_summary.value.add(simple_value=self.env_state.success, node_name="success",
                                      tag="success")
            episode_summary.value.add(simple_value=self.cnt_steps, node_name="cnt_steps",
                                      tag="cnt_steps")
            self.summary_writer.add_summary(episode_summary, cnt_episodes)
            self.summary_writer.flush()

            # reset episode and generate new observation
            self.cnt_steps = 0
            self.cumulated_reward = 0
            self.mean_loss = 0
            self.env_state = self.agentenv.reset()
            self.state[:] = self.env_state.state[:]
            self.action = self.ddqn.generate_action(self.state)

            cnt_episodes += 1
        return cnt_episodes

    # called in every execution step
    def _exec_step(self, cnt_episodes):
        # 1. do observation
        # If agentenv has no new sensor information (lidar has 5Hz): repeat the same action once more and start again
        env_state = self.agentenv.observe()
        if env_state is None:
            self.agentenv.execute(self.action)
            return cnt_episodes

        # 3. transition to next state
        self.state[:] = env_state.state[:]
        self.cumulated_reward += env_state.reward

        # 4. execute next action
        self.action = self.ddqn.generate_action(self.state)
        self.agentenv.execute(self.action)

        self.num_of_actions_vec[cnt_episodes][self.action] += 1

        # 6 check if episode is finished
        if env_state.done:
            # print user info
            printf("Collecting: EP: %d, steps: %d, eps: %4.5f, rew: %4.2f\n",
                   cnt_episodes, self.cnt_steps, self.ddqn.epsilon, self.cumulated_reward)

            # exponential decal of epsilon
            self.ddqn.epsilon = self.eps_final

            # save monitor variables
            self.rewards_vec[cnt_episodes] = self.cumulated_reward
            self.mean_loss_vec[cnt_episodes] = self.mean_loss
            self.goal_loss_vec[cnt_episodes] = 0.0
            self.epsilon_vec[cnt_episodes] = self.ddqn.epsilon
            self.success_rate_vec[cnt_episodes] = self.env_state.success
            self.num_steps_vec[cnt_episodes] = self.cnt_steps

            # add summaries to tensorboard
            episode_summary = tf.Summary()
            episode_summary.value.add(simple_value=self.cumulated_reward, node_name="cumulated_reward",
                                      tag="cumulated_reward")
            episode_summary.value.add(simple_value=0.0, node_name="mean_loss",
                                      tag="mean_loss")
            episode_summary.value.add(simple_value=0.0, node_name="goal_loss",
                                      tag="goal_loss")
            episode_summary.value.add(simple_value=self.ddqn.epsilon, node_name="epsilon",
                                      tag="epsilon")
            episode_summary.value.add(simple_value=self.env_state.success, node_name="success",
                                      tag="success")
            episode_summary.value.add(simple_value=self.cnt_steps, node_name="cnt_steps",
                                      tag="cnt_steps")
            self.summary_writer.add_summary(episode_summary, cnt_episodes)
            self.summary_writer.flush()

            # reset episode and generate new observation
            self.cnt_steps = 0
            self.cumulated_reward = 0
            self.mean_loss = 0
            self.env_state = self.agentenv.reset()
            self.state[:] = self.env_state.state[:]
            self.action = self.ddqn.generate_action(self.state)

            cnt_episodes += 1
        return cnt_episodes

    # called if model must be loaded
    def _load(self, session_dir):
        super(SingleRobotDQN, self)._load(session_dir)
        if os.path.isfile(os.path.join(session_dir, "replay.dat")):
            self.ddqn.replay.load(os.path.join(session_dir, "replay.dat"))

    # called if model must be saved
    def _save(self, session_dir, do_save_all, cnt_episodes):
        super(SingleRobotDQN, self)._save(session_dir, do_save_all, cnt_episodes)

        if do_save_all:
            # save the current configuration
            out_file = open(os.path.join(session_dir, "config.txt"), 'w')
            out_file.write(json.dumps(Config))
            out_file.close()

            # save environment log file
            env_log = {'cumulated_reward': self.rewards_vec,
                       'goal_loss_vec': self.goal_loss_vec,
                       'mean_loss_vec': self.mean_loss_vec,
                       'epsilon_vec': self.epsilon_vec,
                       'success_rate_vec': self.success_rate_vec,
                       'num_steps_vec': self.num_steps_vec,
                       'num_of_actions_vec': self.num_of_actions_vec}
            out_file = open(os.path.join(session_dir, "env_state.dat"), 'wb')
            pickle.dump(env_log, out_file)
            out_file.close()

            # save replay buffer
            self.ddqn.replay.save(os.path.join(session_dir, "replay.dat"))
            print('Saving replay buffer')

    # called if plots must be created
    def _plot(self, plot_dir, cnt_episodes):
        window_len = 1000
        start_idx = self.cnt_training_start
        stop_idx = cnt_episodes - 1

        if cnt_episodes > window_len:
            episodes = np.arange(start_idx + window_len - 1, stop_idx)
            rewards_vec = moving_average(self.rewards_vec[start_idx: stop_idx], window_len)
            mean_loss_vec = moving_average(self.mean_loss_vec[start_idx: stop_idx], window_len)
            success_rate_vec = moving_average(self.success_rate_vec[start_idx: stop_idx], window_len)
            num_steps = moving_average(self.num_steps_vec[start_idx: stop_idx], window_len)

            episode_plot([[episodes, rewards_vec]], 'episodes', 'cumulative_reward', 'reward', plot_dir)
            episode_plot([[episodes, mean_loss_vec]], 'episodes', 'mean_loss', 'mean_loss', plot_dir)
            episode_plot([[episodes, success_rate_vec]], 'episodes', 'success_rate_vec', 'success rate', plot_dir)
            episode_plot([[episodes, num_steps]], 'episodes', 'num_steps', 'number of steps', plot_dir)

            data_list = []
            for i in range(0, self.n_action):
                data_list.append([episodes, moving_average(self.num_of_actions_vec[start_idx: stop_idx, i], window_len)])
            episode_plot(data_list, 'episodes', 'num of actions', 'num', plot_dir)

