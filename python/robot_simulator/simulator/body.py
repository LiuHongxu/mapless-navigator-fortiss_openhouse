import simulator.simconstants as sim
from Box2D import *
import pygame


'''
Baseclass all objects that can be drawn on the screen and take part in the simulated envrionment
'''


class Body:
    def __init__(self, bodytype, pos = b2Vec2(0.0, 0.0), orientation = 0.0):
        self.body_def = b2BodyDef()
        self.body_def.type = bodytype
        self.body_def.position = pos
        self.body_def.angle = orientation
        self.body_def.linearDamping = sim.LINEARDAMPLING
        self.body_def.angularDamping = sim.ANGULARDAMPING
        self.body_def.userData = self

        self.b2body = None

    def destroy(self):
        if self.b2body is not None:
            world = self.b2body.world
            world.DestroyBody(self.b2body)

    def construct(self, world):
        self.b2body = world.CreateBody(self.body_def)

    def reset(self, pos = b2Vec2(0.0, 0.0), orientation = 0.0):
        # set all velocites back to zero
        self.b2body.linearVelocity = b2Vec2(0.0, 0.0)
        self.b2body.angularVelocity = 0.0

        # set to given pose or inital one
        if pos == b2Vec2(0.0, 0.0) and orientation == 0.0:
            self.b2body.position = self.body_def.position
            self.b2body.angle = self.body_def.angle
        else:
            self.b2body.position = pos
            self.b2body.angle = orientation

    def get_bounding_rect(self, margin = b2Vec2(0.0, 0.0), is_global = False):
        aabb = b2AABB()
        aabb.lowerBound = b2Vec2(1000000, 1000000)
        aabb.upperBound = b2Vec2(-1000000, -1000000)

        t = b2Transform()
        t.SetIdentity()
        for fixture in self.b2body.fixtures:
            shape = fixture.shape
            for i in range(0, shape.childCount):
                shapeAABB = shape.getAABB(t, i)
                aabb.Combine(shapeAABB)

        aabb.lowerBound -= margin
        aabb.upperBound += margin

        if is_global:
            aabb.lowerBound += self.b2body.position
            aabb.upperBound += self.b2body.position

        return aabb

    def update(self, dt):
        raise NotImplementedError

    def update_graphics(self, screen):
        for fixture in self.b2body.fixtures:
            shape = fixture.shape
            if isinstance(shape, b2CircleShape):
                self.draw_circle(screen, shape, self.b2body)
            if isinstance(shape, b2PolygonShape):
                self.draw_polygon(screen, shape, self.b2body)

    def draw_polygon(self, screen, polygon, body):
        vertices = [sim.meter2pixel(body.transform * v) for v in polygon.vertices]
        pygame.draw.polygon(screen, (0, 255, 0, 255), vertices)

    def draw_circle(self, screen, circle, body):
        center = sim.meter2pixel(body.transform * circle.pos)
        pygame.draw.circle(screen, (0, 255, 0, 255), [int(x) for x in center],
                           int(circle.radius * sim.METER2PIXEL_X))









