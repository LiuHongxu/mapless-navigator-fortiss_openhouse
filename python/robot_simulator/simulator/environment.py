from simulator.staticobstacle import *

from simulator.body import *
from Box2D import *

import itertools
import pygame
import time
from pygame.locals import (QUIT, KEYDOWN, K_ESCAPE)

"""
This file contains all simulation constants and comon functions
used by the simulator
"""
class Grid:

    def __init__(self, size, color = (200, 200, 200, 255)):
        self.color = color
        self.tilesize = b2Vec2( size.x * sim.METER2PIXEL_X, size.y * sim.METER2PIXEL_Y)
        cols = int(sim.WINDOWSIZE_X / self.tilesize.x - 1)
        rows = int(sim.WINDOWSIZE_Y / self.tilesize.y - 1)

        self.lines = []
        self.n_lines = 2 * (rows + cols)

        for i in range(0, cols):
            self.lines.append( (0.0, self.tilesize.y * (i + 1)) )
            self.lines.append( (sim.WINDOWSIZE_Y, self.tilesize.y * (i + 1)) )
        for i in range(0, rows):
            self.lines.append( (self.tilesize.x * (i + 1), 0.0) )
            self.lines.append( (self.tilesize.x * (i + 1), sim.WINDOWSIZE_X) )

    def update_graphics(self, screen):
        for i in range(0, self.n_lines, 2):
            pygame.draw.line(screen, self.color, self.lines[i], self.lines[i+1])


class Environment:

    def __init__(self, gravity = b2Vec2(0.0, 0.0), loop_frequency = 30, step_size = 0.05):
        self.grid = Grid(b2Vec2(0.5, 0.5))

        self.loop_frequency = loop_frequency
        self.step_size = step_size
        self.is_started = False
        self.is_graphic_activated = True
        self.is_walls_activated = True
        self.cnt = 0
        self.prev_time = 0.0
        self.realtime_factor = 0.0

        # 1. create a graphical window
        self.screen = pygame.display.set_mode((sim.WINDOWSIZE_X, sim.WINDOWSIZE_Y), 0, 32)
        pygame.display.set_caption('robot_sim')

        # 2. create physics simulator
        self.world = b2World(gravity)

        # 3. save bodies in a list
        self.bodies = []
        self.walls = []

    def __del__(self):
        pygame.quit()

    def add(self, body, pos = -1):
        # fully constuct body
        body.construct(self.world)

        # add body to world
        if pos < 0:
            self.bodies.append(body)
        else:
            self.bodies.insert(body)

    def start(self, is_started):
        self.is_started = is_started

    def set_walls(self, margin = b2Vec2(0.025, 0.025)):
        rect0 = StaticRectangle(b2Vec2(sim.WORLDSIZE_X / 2.0, margin.y / 2.0), b2Vec2(sim.WORLDSIZE_X, margin.y))
        rect1 = StaticRectangle(b2Vec2(sim.WORLDSIZE_X / 2.0, sim.WORLDSIZE_Y - margin.y / 2.0), b2Vec2(sim.WORLDSIZE_X, margin.y))
        rect2 = StaticRectangle(b2Vec2(margin.x / 2.0, sim.WORLDSIZE_Y / 2.0), b2Vec2(margin.x, sim.WORLDSIZE_Y))
        rect3 = StaticRectangle(b2Vec2(sim.WORLDSIZE_X - margin.x / 2.0, sim.WORLDSIZE_Y / 2.0), b2Vec2(margin.x, sim.WORLDSIZE_Y))

        rect0.construct(self.world)
        rect1.construct(self.world)
        rect2.construct(self.world)
        rect3.construct(self.world)

        self.walls.append(rect0)
        self.walls.append(rect1)
        self.walls.append(rect2)
        self.walls.append(rect3)

    def reset(self, clear_area = None, fixed_body = None):
        '''
        reset body position to init position except for fixed_body
        :param clear_area: -
        :param fixed_body: body that should not be reseted
        :return:
        '''
        for body in self.bodies:
            if not fixed_body == body:
                body.reset_pose()

    def update(self):
        if self.is_started:
            # 1. check if window closed
            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.is_started = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_F1:
                        self.is_graphic_activated = True
                    if event.key == pygame.K_F2:
                        self.is_graphic_activated = False

            # 2. measure times / update frequency (the cheep way)
            if self.cnt % 10000:
                tmp = time.time()
                elapsed_time = tmp - self.prev_time
                self.prev_time = tmp
                self.realtime_factor = 0.8 * self.realtime_factor + 0.2 * self.step_size / elapsed_time

            # 3. simmuate physics
            self.world.Step(self.step_size, 8, 3)

            # 4. update bodies
            for body in self.bodies:
                body.update(self.step_size)

            self.cnt += 1
            return self.realtime_factor

        return 0.0

    def update_graphics(self):
        if self.is_graphic_activated:
            # clear all
            self.screen.fill((0, 0, 0, 0))

            # draw background
            self.grid.update_graphics(self.screen)

            # draw world
            for body in itertools.chain(self.bodies, self.walls):
                body.update_graphics(self.screen)
            pygame.display.flip()

    def clear(self):
        # remove walls
        for wall in self.walls:
            wall.destroy()
        self.walls = []

        # remove objects
        for body in self.bodies:
            body.destroy()
        self.bodies = []
        self.is_started = False

