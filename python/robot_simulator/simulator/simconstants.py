"""
This file contains all simulation constants and comon functions
used by the simulator
"""

import math
from Box2D import *

PI = 3.14159265359
PI_2 = 2.0 * PI
RAD2DEG = 57.2957795131
DEG2RAD = 0.01745329251

LINEARDAMPLING = 1.5
ANGULARDAMPING = 1.5

WINDOWSIZE_X = 1000
WINDOWSIZE_Y = 1000

WORLDSIZE_X = 5.0
WORLDSIZE_Y = 5.0
MAXWORLDDIST = math.sqrt( WORLDSIZE_X * WORLDSIZE_X + WORLDSIZE_Y * WORLDSIZE_Y )

PIXEL2METER_X = WORLDSIZE_X / WINDOWSIZE_X
PIXEL2METER_Y = WORLDSIZE_Y / WINDOWSIZE_Y

METER2PIXEL_X = WINDOWSIZE_X / WORLDSIZE_X
METER2PIXEL_Y = WINDOWSIZE_Y / WORLDSIZE_Y


def pixel2meter( vec ):
    return b2Vec2(vec.x * PIXEL2METER_Y, WORLDSIZE_Y - vec.y * PIXEL2METER_Y)


def meter2pixel( vec ):
    return b2Vec2(vec.x * METER2PIXEL_X, WINDOWSIZE_Y - vec.y * METER2PIXEL_Y)


def rotate( vec, a ):
    return b2Vec2(vec.x * math.cos(a) - vec.y * math.sin(a), vec.x * math.sin(a) + vec.y * math.cos(a))


def intersect( rectA, rectB):
    return rectA.lowerBound.x < rectB.upperBound.x and \
           rectA.upperBound.x > rectB.lowerBound.x and \
           rectA.lowerBound.y < rectB.upperBound.y and \
           rectA.upperBound.y > rectB.lowerBound.y
