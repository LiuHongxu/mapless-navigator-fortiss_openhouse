from simulator.sensor import *
from simulator.raycast import *
import numpy as np


# Simulation of Lidar sensor by unsing multible raycasts
# measured distances available in numpy array called ranges
class Lidar_Recorder(Sensor):

    def __init__(self, ray_number, max_range, visual_angle, freq = 0.01, mean = 0.0, std = 0.0):
        super(Lidar_Recorder, self).__init__(freq, mean, std)

        self.ray_numer = ray_number
        self.max_range = max_range
        self.visual_angle = visual_angle
        self.ranges = np.zeros(ray_number)
        self.ray_casts = []
        self.ray_points = []

    def attach_body(self, b2body, pos = b2Vec2(0.0, 0.0), angle = 0.0):
        super(Lidar_Recorder, self).attach_body(b2body, pos, angle)

        for i in range(0, self.ray_numer):
            self.ray_casts.append( RayCast( b2body.world ))

        delta_phi = 2 * self.visual_angle / (self.ray_numer - 1)
        offset = angle - self.visual_angle

        for i in range(0, self.ray_numer):
            self.ray_points.append( self.max_range * b2Vec2( math.cos(i * delta_phi + offset), math.sin(i * delta_phi + offset) ) )

    def update(self, dt):
        if super(Lidar_Recorder, self).update(dt):
            start = self.b2body.GetWorldPoint(self.pos)
            for i in range(0, self.ray_numer):
                end =self.b2body.GetWorldPoint( self.ray_points[i] )

                if self.ray_casts[i].cast(start, end, self.max_range):
                    self.ranges[i] = self.ray_casts[i].callback.shortest_dist
                else:
                    self.ranges[i] = self.max_range
            return True
        return False

    def reset(self):
        super(Lidar_Recorder, self).reset()
        self.update(0.0)

    def get_ranges(self):
        self.available = False
        return self.ranges

    def update_graphics(self, screen):
        for i in range(0, self.ray_numer):
            pygame.draw.circle(screen, (255, 0, 0, 255), b2Vec2(50, 50), 5)