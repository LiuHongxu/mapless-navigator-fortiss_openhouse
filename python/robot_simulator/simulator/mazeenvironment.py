from simulator.environment import *
import random


class DisjointSets:
    def __init__(self, n):
        self.n = n
        self.parent = list(range(0, n + 1))
        self.rnk = [0] * (n+1)

    def find(self, u):
        if isinstance(u, tuple):
            u = u[1]

        if u != self.parent[u]:
            self.parent[u] = self.find((self, self.parent[u]))
        return int(self.parent[u])

    def merge(self, x, y):
        x = self.find(x)
        y = self.find(y)

        if self.rnk[x] > self.rnk[y]:
            self.parent[y] = x
        else:
            self.parent[x] = y
        if self.rnk[x] == self.rnk[y]:
            self.rnk[y]+=1

class Graph:
    def __init__(self, v, e):
        self.v = v
        self.e = e
        self.edges = []

    def add_egde(self, u, v, w):
        self.edges.append([w, u, v])

    def kruskal_mst(self):
        mst = []

        self.edges.sort(key=lambda x: x[0])

        ds = DisjointSets(self.v)

        for edge in self.edges:
            u = edge[1]
            v = edge[2]

            set_u = ds.find(u)
            set_v = ds.find(v)

            if set_u != set_v:
                mst.append([u, v])
                ds.merge(set_u, set_v)

        return mst


class MazeEnvironment(Environment):

    def __init__(self, gravity=b2Vec2(0.0, 0.0), loop_frequency=30, step_size=0.05):
        super(MazeEnvironment, self).__init__(gravity, loop_frequency, step_size)
        self.drop_out = 0.3
        self.tile_size = b2Vec2(1.0, 1.0)
        self.cols = int(sim.WORLDSIZE_X / self.tile_size.x)
        self.rows = int(sim.WORLDSIZE_Y / self.tile_size.y)
        self.maze_start_idx = 0

    def reset(self, clear_area = None):
        nodes = int(self.rows * self.cols)
        edges = int(2 * self.rows * self.cols - ( self.rows + self.cols ))
        graph = Graph(nodes, edges)

        k = 0
        for i in range(0, self.rows):
            for j in range(0, self.cols):
                if j != self.cols - 1:
                    graph.add_egde(k, k+1, random.uniform(0, nodes))
                if i != self.rows - 1:
                    graph.add_egde(k, k + self.cols, random.uniform(0, nodes))
                k+=1

        mst = graph.kruskal_mst()

        if self.maze_start_idx > 0:
            for i in range(self.maze_start_idx, len(self.bodies)):
                self.bodies[i].destroy()
            del self.bodies[self.maze_start_idx : len(self.bodies)]
        self.maze_start_idx = len(self.bodies)

        size_v = b2Vec2(0.1, self.tile_size.x)
        size_h = b2Vec2(self.tile_size.y, 0.1)

        k = 0
        for i in range(0, self.rows):
            for j in range(0, self.cols):
                if [k, k + self.cols] not in mst:
                    pos = b2Vec2( (j + 0.5) * self.tile_size.x, (i+1) * self.tile_size.y )
                    if random.uniform(0.0, 1.0) > self.drop_out:
                        self.add( StaticRectangle(pos, size_h) )
                if [k, k+1] not in mst:
                    pos = b2Vec2( (j+1) * self.tile_size.x, (i + 0.5) * self.tile_size.y )
                    if random.uniform(0.0, 1.0) > self.drop_out:
                        self.add( StaticRectangle(pos, size_v) )
                k+=1

        self.rand_place_bodies(0, self.maze_start_idx, self.maze_start_idx, len(self.bodies), clear_area)

    def rand_place_bodies(self, begin_obj, end_obj, begin_maze, end_maze, clear_area):
        rand_pos = b2Vec2(0.0, 0.0)

        for i in range(begin_obj, end_obj):
            cnt = 0
            body = self.bodies[i]
            rand_pos.x = random.uniform(0.3, sim.WORLDSIZE_X - 0.3)
            rand_pos.y = random.uniform(0.3, sim.WORLDSIZE_Y - 0.3)
            rand_orient = random.uniform(-sim.PI, sim.PI)

            body.reset(rand_pos, rand_orient)

            rect = body.get_bounding_rect(b2Vec2(0.3, 0.3), True)
            if clear_area is None:
                while self.is_colliding( rect, begin_maze, end_maze ) and cnt < 1000:
                    rand_pos.x = random.uniform(0.3, sim.WORLDSIZE_X - 0.3)
                    rand_pos.y = random.uniform(0.3, sim.WORLDSIZE_Y - 0.3)
                    rand_orient = random.uniform(-sim.PI, sim.PI)
                    body.reset(rand_pos, rand_orient)
                    rect = body.get_bounding_rect(b2Vec2(0.3, 0.3), True)
                    cnt += 1
            else:
                while (self.is_colliding( rect, begin_maze, end_maze ) or sim.intersect(rect, clear_area)) and cnt < 1000:
                    rand_pos.x = random.uniform(0.3, sim.WORLDSIZE_X - 0.3)
                    rand_pos.y = random.uniform(0.3, sim.WORLDSIZE_Y - 0.3)
                    rand_orient = random.uniform(-sim.PI, sim.PI)
                    body.reset(rand_pos, rand_orient)
                    rect = body.get_bounding_rect(b2Vec2(0.3, 0.3), True)
                    cnt += 1

    def is_colliding(self, rectA, begin, end, margin = b2Vec2(0.1, 0.1)):
        for i in range(begin, end):
            rectB = self.bodies[i].get_bounding_rect(margin, True)
            if sim.intersect(rectA, rectB):
                return True
        return False
