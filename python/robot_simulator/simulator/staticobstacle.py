from simulator.body import *
from Box2D import *

class StaticObstacle(Body):
    def __init__(self, pos):
        super(StaticObstacle, self).__init__(b2_staticBody, pos)

    def update(self, dt):
        pass


class StaticCircle(StaticObstacle):
    def __init__(self, pos, radius):
        super(StaticObstacle, self).__init__(b2_staticBody, pos)
        self.radius = radius

    def construct(self, world):
        # 1. call base class constructor
        super(StaticCircle, self).construct(world)

        b2circleshape = b2CircleShape()
        b2circleshape.radius = self.radius

        # 2. bind body to shape
        fixture_def = b2FixtureDef()
        fixture_def.shape = b2circleshape
        fixture_def.density = 1.0
        fixture_def.friction = 0.8
        fixture_def.sensor = False
        self.b2body.CreateFixture(fixture_def)


class StaticRectangle(StaticObstacle):
    def __init__(self, pos, size):
        super(StaticObstacle, self).__init__(b2_staticBody, pos)
        self.size = size

    def construct(self, world):
        # 1. call base class constructor
        super(StaticRectangle, self).construct(world)

        b2polygonshape = b2PolygonShape()
        b2polygonshape.SetAsBox(self.size.x / 2, self.size.y / 2)

        # 2. bind body to shape
        fixture_def = b2FixtureDef()
        fixture_def.shape = b2polygonshape
        fixture_def.density = 1.0
        fixture_def.friction = 0.8
        fixture_def.sensor = False
        self.b2body.CreateFixture(fixture_def)
