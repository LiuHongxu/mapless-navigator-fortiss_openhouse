from Box2D import *


# Model state of the agent
class ModelState:
    def __init__(self, position = None, theta = None, velocity = None, omega = None):
        self.position = position
        self.theta = theta
        self.velocity = velocity
        self.omega = omega


# command for agent
class VelCmd:
    def __init__(self):
        self.velocity = b2Vec2(0.0, 0.0)
        self.omega = 0.0


# Base class of each agent
class Agent:
    def __init__(self):
        self.modelstate = ModelState

    def observe(self):
        raise NotImplementedError

    def model(self):
        raise NotImplementedError

    def command(self, cmd):
        raise NotImplementedError
