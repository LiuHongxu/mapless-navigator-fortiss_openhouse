
from simulator.body import *
from simulator.lidar import *
from simulator.agent import *


class Robot(Body, Agent):

    def __init__(self, pos, orientation, radius, pos_std = 0.0, orient_std = 0.0, lin_vel_std = 0.0, ang_vel_std = 0.0 ):
        super(Robot, self).__init__(b2_dynamicBody, pos, orientation)

        self.radius = radius
        self.pos_std = pos_std
        self.orient_std = orient_std
        self.lin_vel_std = lin_vel_std
        self.ang_vel_std = ang_vel_std

        # 18 rays, 2.0 meter, 2/3 pi angle, 3 Hz update freq
        self.lidar = Lidar(18, 2.0, sim.PI_2 / 3, 3.0)

    def construct(self, world):
        super(Robot, self).construct(world)

        b2circleshape = b2CircleShape()
        b2circleshape.radius = self.radius

        # 2. bind body to shape
        fixture_def = b2FixtureDef()
        fixture_def.shape = b2circleshape
        fixture_def.density = 1.0
        fixture_def.friction = 0.8
        fixture_def.sensor = False
        self.b2body.CreateFixture(fixture_def)

        self.lidar.attach_body(self.b2body)

    def observe(self):
        available = self.lidar.is_available()
        sensorreading = self.lidar.get_ranges()
        return available, sensorreading

    def model(self):
        return ModelState(self.b2body.position,
                          self.normal_relative_angle(self.b2body.angle),
                          self.b2body.linearVelocity,
                          self.b2body.angularVelocity)

    def command(self, cmd):
        self.apply_lin_velo(cmd.velocity)
        self.apply_ang_velo(cmd.omega)

    def reset(self, pos = b2Vec2(0.0, 0.0), orientation = 0.0):
        super(Robot, self).reset(pos, orientation)
        self.lidar.reset()

    def update(self, dt):
        self.lidar.update(dt)

    def update_graphics(self, screen):
        super(Robot, self).update_graphics(screen)
        self.lidar.update_graphics(screen)

    def apply_lin_velo(self, vel):
        d_vel = sim.rotate(vel, self.b2body.angle) - self.b2body.linearVelocity
        impuls = self.b2body.mass * d_vel
        self.b2body.ApplyLinearImpulse(impuls, self.b2body.position, True)

    def apply_ang_velo(self, omega):
        impuls = self.b2body.inertia * (omega - self.b2body.angularVelocity)
        self.b2body.ApplyAngularImpulse(impuls, True)

    def normal_relative_angle(self, angle):
        while angle > sim.PI:
            angle -= sim.PI_2
        while angle < -sim.PI:
            angle += sim.PI_2
        return angle
