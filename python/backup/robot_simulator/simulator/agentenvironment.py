from simulator.environment import *
from simulator.agent import *

class EnvironmentState:
    def __init__(self, done = None, success = None, reward = None, state = None):
        self.done = done
        self.success = success
        self.reward = reward
        self.state = state

class AgentEnvironement:
    def __iter__(self, env, agent, n_action, n_state):
        self.env = env
        self.agent = agent
        self.n_action = n_action
        self.n_state = n_state

    def execute(self, action):
        raise NotImplementedError

    def observe(self):
        raise NotImplementedError

    def reset(self, clear_area = None):
        self.env.reset(clear_area)