from Box2D import *


class Sensor:
    def __init__(self, freq = 0.01, mean = 0.0, std = 0.0):
        self.period = 1.0 / freq
        self.mean = mean
        self.std = std
        self.elapsed = self.period + 1.0

        self.available = False
        self.b2body = None
        self.b2world = None
        self.pos = b2Vec2(0.0, 0.0)
        self.angle = 0.0

    def is_available(self):
        return self.available

    def reset(self):
        self.elapsed = self.period + 1.0

    def attach_body(self, b2body, pos, angle):
        self.b2body = b2body
        self.pos = pos
        self.angle = angle
        self.b2world = b2body.world

    def update(self, dt):
        self.elapsed += dt
        if self.elapsed > self.period:
            self.elapsed = 0.0
            self.available = True
            return True
        return False

    def read_done(self):
        self.available = False

