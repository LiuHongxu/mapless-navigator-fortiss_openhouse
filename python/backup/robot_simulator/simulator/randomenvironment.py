from simulator.environment import *
import random

class RandomEnvironment(Environment):

    def __init__(self, gravity=b2Vec2(0.0, 0.0), loop_frequency=30, step_size=0.05):
        super(RandomEnvironment, self).__init__(gravity, loop_frequency, step_size)

    def reset(self, clear_area = None):
        rand_pos = b2Vec2(0.0, 0.0)

        i = 0
        for body in self.bodies:
            cnt = 0
            rand_pos.x = random.uniform(0.3, sim.WORLDSIZE_X - 0.3)
            rand_pos.y = random.uniform(0.3, sim.WORLDSIZE_Y - 0.3)
            rand_orient = random.uniform(-sim.PI, sim.PI)

            body.reset(rand_pos, rand_orient)

            rect = body.get_bounding_rect(b2Vec2(0.3, 0.3), True)
            if clear_area is None:
                while self.is_colliding( rect, 0, i, b2Vec2(0.1, 0.1)) and cnt < 1000:
                    rand_pos.x = random.uniform(0.3, sim.WORLDSIZE_X - 0.3)
                    rand_pos.y = random.uniform(0.3, sim.WORLDSIZE_Y - 0.3)
                    rand_orient = random.uniform(-sim.PI, sim.PI)
                    body.reset(rand_pos, rand_orient)
                    rect = body.get_bounding_rect(b2Vec2(0.3, 0.3), True)
                    cnt += 1
            else:
                while (self.is_colliding( rect, 0, i, b2Vec2(0.1, 0.1)) or sim.intersect(rect, clear_area)) and cnt < 1000:
                    rand_pos.x = random.uniform(0.3, sim.WORLDSIZE_X - 0.3)
                    rand_pos.y = random.uniform(0.3, sim.WORLDSIZE_Y - 0.3)
                    rand_orient = random.uniform(-sim.PI, sim.PI)
                    body.reset(rand_pos, rand_orient)
                    rect = body.get_bounding_rect(b2Vec2(0.3, 0.3), True)
                    cnt += 1
            i += 1

    def is_colliding(self, rectA, begin, end, margin):
        # check collisions with walls
        for wall in self.walls:
            rectB = wall.get_bounding_rect(margin, True)
            if sim.intersect(rectA, rectB):
                return True
        # check collisions with bodies
        for i in range(begin, end):
            rectB = self.bodies[i].get_bounding_rect(margin, True)
            if sim.intersect(rectA, rectB):
                return True
        return False

