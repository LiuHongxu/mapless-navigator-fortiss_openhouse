from simulator.body import *
import pygame
import math

class RayCastCallback(b2RayCastCallback):

    def __init__(self, start, end, direction, distance, start_object):
        b2RayCastCallback.__init__(self)
        self.start_object = start_object
        self.start = start
        self.end = end
        self.direction = direction
        self.distance = distance
        self.shortest_dist = -1.0
        self.hit_point = b2Vec2(0.0, 0.0)
        self.closest_object = None

    def ReportFixture(self, fixture, point, normal, fraction):
        if fraction <= 0:
            return 1
        if fixture.sensor:
            return -1
        if isinstance(fixture.body.userData, Body):
            self.closest_object = fixture.body.userData

        self.shortest_dist = fraction * self.distance
        self.hit_point = b2Vec2(point[0], point[1])
        return fraction


class RayCast:

    def __init__(self, b2world):
        self.b2world = b2world
        self.line = [ b2Vec2(0.0, 0.0), b2Vec2(0.0, 0.0) ]
        self.callback = RayCastCallback(b2Vec2(0.0, 0.0), b2Vec2(0.0, 0.0), -1.0, 0.0, None)

    def cast(self, start, angle, distance, start_body = None):
        self.callback = RayCastCallback(start,
                                   distance * b2Vec2(start.x + math.cos(angle), start.y + math.sin(angle)),
                                   angle,
                                   distance,
                                   start_body)

        self.b2world.RayCast(self.callback, self.callback.start, self.callback.end)
        return self.callback.closest_object is not None

    def cast(self, start, end, distance, start_body = None):
        self.callback = RayCastCallback(start,
                                   end,
                                   -1.0,
                                   distance,
                                   start_body)

        self.b2world.RayCast(self.callback, self.callback.start, self.callback.end)
        return self.callback.closest_object is not None

    def update_graphics(self, screen):
        self.line[0] = sim.meter2pixel(self.callback.start)
        if self.callback.closest_object is None:
            self.line[1] = sim.meter2pixel(self.callback.end)
        else:
            self.line[1] = sim.meter2pixel(self.callback.hit_point)

        pygame.draw.line(screen, (255, 0, 0, 255), self.line[0], self.line[1])