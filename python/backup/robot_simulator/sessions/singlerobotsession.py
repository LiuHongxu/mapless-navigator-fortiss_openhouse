import sys
from session import *
from simulator.robot import *
from dqn.dqnenvironment import *
from dqn.ddqn import *

# get c-like format back !!!
def printf(format, *args):
    sys.stdout.write(format % args)

# ----------------------------------------------------------------------------------------------------------------------
# Single Robot Session using DDQN
# BaseClass: Session
# ----------------------------------------------------------------------------------------------------------------------
class SingleRobotDQN(Session):
    """ Implmentation of a Training Session
        Connects reinforcemnet learning algorithm with simuator,
        Loads and saves the session in a spcified directory"""

    def __init__(self, env, log_dir, save_steps):
        super(SingleRobotDQN, self).__int__(env, log_dir, "single_robot_dqn", save_steps)

        # Control the session
        self.n_state = 20  # see state in dqnenvironment.py (must fit... todo)
        self.n_action = 3
        self.buffer_size = 1000000
        self.eps_final = 0.3
        self.eps_discount = 0.999988 # 50 000 - 0.99988

        self.ddqn = DDQN(self.tf_sess, self.n_state, self.n_action, self.buffer_size)

        # internal var
        self.cnt_steps = 0
        self.cumulated_reward = 0
        self.action = 0
        self.state = []
        self.env_state = []

        # setup all environment objects and algorithms
    def _setup_environment(self, env):
        # 1. build world
        env.add( StaticCircle(b2Vec2(2.5, 2.5), 0.15))
        env.add( StaticCircle(b2Vec2(4.0, 1.0), 0.2) )
        env.add( StaticCircle(b2Vec2(2.0, 4.0), 0.3) )
        env.add( StaticCircle(b2Vec2(2.0, 4.0), 0.3) )
        env.add( StaticRectangle(b2Vec2(3, 3), b2Vec2(0.25, 0.7)))
        env.add( StaticRectangle(b2Vec2(1, 4), b2Vec2(0.4, 0.7)) )
        env.add( StaticRectangle(b2Vec2(1, 4), b2Vec2(0.3, 0.5)) )
        env.add( StaticRectangle(b2Vec2(1, 4), b2Vec2(0.3, 0.5)) )
        env.set_walls()

        # 2. build agent
        robot = Robot( b2Vec2(3.0, 3.0), 0.0, 0.175 )
        env.add(robot)
        self.agentenv = DQNEnvironment(self.env, robot, self.n_action, self.n_state, b2Vec2(3.5, 3.5))

    # called if a session starts
    def _on_start(self, total_episodes):
        # 0. resize monitor varibales
        self.rewards_vec = np.zeros((total_episodes,))
        self.epsilon_vec = np.zeros((total_episodes,))
        self.success_rate_vec = np.zeros((total_episodes,))
        self.num_steps_vec = np.zeros((total_episodes,))
        self.loss_vec = np.zeros((total_episodes,))

        # 1. reset everything before training
        self.cnt_steps = 0
        self.cumulated_reward = 0

        # 2. reset and get an initial observation
        self.env_state = self.agentenv.reset()
        self.state = self.env_state.state
        self.action = self.ddqn.generate_action(self.state)

        printf('Session: ' + self.name + ' started')

    # called in every training step
    def _train_step(self, cnt_episodes):
        # 1. do observation
        # If agentenv has no new sensor information (lidar has 5Hz): repeat the same action once more and start again
        env_state = self.agentenv.observe()
        if env_state is None:
            self.agentenv.execute(self.action)
            return cnt_episodes

        # 2. learn
        # Transition: state, action, reward, next_state
        loss = self.ddqn.learn( self.state, self.action, env_state.reward, env_state.state, env_state.done )

        # 3. transition to next state
        self.state = env_state.state
        self.cumulated_reward += env_state.reward
        self.cnt_steps+=1

        # 4. execute next action
        self.action = self.ddqn.generate_action( self.state )
        self.agentenv.execute( self.action )

        # 6 check if episode is finished
        if env_state.done or self.cnt_steps > 1000:
            # print user info
            if loss < 0:
                printf("Collecting: EP: %d, steps: %d, eps: %4.5f, rew: %4.2f\n",
                       cnt_episodes, self.cnt_steps, self.ddqn.epsilon, self.cumulated_reward)
            else:
                printf("Replaying: EP: %d, steps: %d, eps: %4.5f, rew: %4.2f, loss: %4.2f\n",
                       cnt_episodes, self.cnt_steps, self.ddqn.epsilon, self.cumulated_reward, loss)

            # exponential decal of epsilon
            if self.ddqn.epsilon > self.eps_final and loss > 0:
                self.ddqn.epsilon *= self.eps_discount

            # save monitor variables
            self.rewards_vec[cnt_episodes] = self.cumulated_reward
            self.loss_vec[cnt_episodes] = loss
            self.epsilon_vec[cnt_episodes] = self.ddqn.epsilon
            self.success_rate_vec[cnt_episodes] = self.env_state.success
            self.num_steps_vec[cnt_episodes] = self.cnt_steps

            # add summaries to tensorboard
            episode_summary = tf.Summary()
            episode_summary.value.add(simple_value=self.cumulated_reward, node_name="cumulated_reward",
                                      tag="cumulated_reward")
            episode_summary.value.add(simple_value=loss, node_name="loss",
                                      tag="loss")
            episode_summary.value.add(simple_value=self.ddqn.epsilon, node_name="epsilon",
                                      tag="epsilon")
            episode_summary.value.add(simple_value=self.env_state.success, node_name="success",
                                      tag="success")
            episode_summary.value.add(simple_value=self.cnt_steps, node_name="cnt_steps",
                                      tag="cnt_steps")
            self.summary_writer.add_summary(episode_summary, cnt_episodes)
            self.summary_writer.flush()

            # reset episode and generate new observation
            self.cnt_steps = 0
            self.cumulated_reward = 0
            self.env_state = self.agentenv.reset()
            self.state = self.env_state.state
            self.action = self.ddqn.generate_action(self.state)

            cnt_episodes += 1
        return cnt_episodes

    # called in every execution step
    def _exec_step(self):
        pass  # not used at the moment

    # called if model must be loaded
    def _load(self, session_dir):
        super(SingleRobotDQN, self)._load(session_dir)
        if os.path.isfile(os.path.join(session_dir, "replay.dat")):
            self.ddqn.replay.load(os.path.join(session_dir, "replay.dat"))
            print('Found replay buffer, Loading: ' + str(len(self.ddqn.replay.buffer)) + ' samples')

    # called if model must be saved
    def _save(self, session_dir, do_save_all, cnt_episodes):
        super(SingleRobotDQN, self)._save(session_dir, do_save_all, cnt_episodes)
        if do_save_all:
            env_log = {'cumulated_reward' : self.rewards_vec,
                       'loss_vec' : self.loss_vec,
                       'epsilon_vec' : self.epsilon_vec,
                       'success_rate_vec': self.success_rate_vec,
                       'num_steps' : self.num_steps_vec}
            out_file = open(os.path.join(session_dir, "env_state.dat"), 'wb')
            pickle.dump(env_log, out_file)
            out_file.close()

            self.ddqn.replay.save(os.path.join(session_dir, "replay.dat"))
            print('Saving replay buffer')