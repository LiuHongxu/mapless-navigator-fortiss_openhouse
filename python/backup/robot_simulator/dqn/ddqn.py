from dqn.networks import DQNNetwork
import random
import numpy as np
import pickle
import tensorflow as tf
from collections import deque
from dqn.sumtree import SumTree


# ----------------------------------------------------------------------------------------------------------------------
# Baseclass for replay buffers
# ----------------------------------------------------------------------------------------------------------------------
class ReplayBase:
    def __init__(self):
        self.buffer = []
        self._cnt = 0

    def save(self, file):
        """ save to file """
        out_file = open(file, 'wb')
        pickle.dump(self.buffer, out_file)

    def load(self, file):
        """ load form file """
        in_file = open(file, 'rb')
        self.buffer = pickle.load(in_file)
        self._cnt = len(self.buffer) - 1


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves transitions
# ----------------------------------------------------------------------------------------------------------------------
class Replay(ReplayBase):
    """ Replay Buffer """
    def __init__(self, buffer_size: int, state_size: int):
        super(Replay, self).__init__()
        self.buffer_size = buffer_size
        self.full = False

        # required size of buffer: 2 * state, 1 * action, 1 * reward, 1 * td_error, 1 * done flag
        self.buffer_cols = 2 * state_size + 4
        self.buffer = np.zeros((buffer_size, self.buffer_cols), dtype=np.float32)

        # pre compute index for buffer columns (easy access)
        self.s_col = 0
        self.a_col = state_size
        self.r_col = self.a_col+1
        self.s_next_col = self.r_col + 1
        self.td_col = self.s_next_col + state_size
        self.d_col = self.td_col + 1

    def is_full(self):
        """ true if buffer filled"""
        return self.full

    def add(self, state, action, reward, state_next, td, done):
        """ add new transition to buffer """
        self.buffer[self._cnt] = np.concatenate((state, [action], [reward], state_next, [td], [done]), axis=0)
        self._cnt += 1
        if self._cnt >= self.buffer_size:
            self.full = True
            self._cnt = 0

    def sample(self, batch_size: int):
        """
        sample uniformly form buffer, return batch_size elements
        :param batch_size: int
        :return: np array of batch_size x buffer_size
        """
        idx = np.random.randint(self.buffer_size, size=batch_size)
        return self.buffer[idx,:]

    def split_sample(self, batch):
        """
        split batch_sample into parts
        :param batch: np narray obtained by sample()
        :return: state_batch, action_batch, reward_batch, state_next_batch, done_batch
        """
        return batch[:, self.s_col:self.a_col], \
               batch[:, self.a_col], \
               batch[:, self.r_col], \
               batch[:, self.s_next_col:self.td_col], \
               batch[:, self.d_col]


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves n-step transitions (using n-step rew for faster convergence of q-values)
# ----------------------------------------------------------------------------------------------------------------------
class NStepReplay(Replay):
    """ N-Step Replay Buffer """
    def __init__(self, buffer_size: int, state_size: int, n_steps: int, gamma: float):
        super(NStepReplay, self).__init__(buffer_size, state_size)
        self.n_steps = n_steps

        self.state_seq = np.zeros((n_steps, state_size), dtype=np.float32)
        self.action_seq = np.zeros((n_steps,), dtype=np.float32)
        self.reward_seq = np.zeros((n_steps,), dtype=np.float32)
        self.state_next_seq = np.zeros((n_steps, state_size), dtype=np.float32)
        self.td_seq = np.zeros((n_steps,), dtype=np.float32)
        self.done_seq = np.zeros((n_steps,), dtype=np.float32)

        self.start_idx = 0
        self.cur_idx = 0
        self.cur_size = 0
        self.R = 0.0
        self.gamma = gamma
        self.gamma_n = gamma ** n_steps

    def add(self, state, action, reward, state_next, td, done):
        """
        Add a new transition to sequence, update n-step reward
        If sequence buffer full or terminal state reached: update the replay buffer with n step transition
        """
        # 1. add new transition to intermediate sequenze buffer
        self.state_seq[self.cur_idx] = state
        self.action_seq[self.cur_idx] = action
        self.reward_seq[self.cur_idx] = reward
        self.state_next_seq[self.cur_idx] = state_next
        self.td_seq[self.cur_idx] = td
        self.done_seq[self.cur_idx] = done
        self.cur_idx += 1
        self.cur_size += 1

        # 2. update n-step cummulative reward ( recursive )
        self.R = (self.R + reward * self.gamma_n) / self.gamma

        # 3. empty sequence buffer if next state is a terminal state
        if done:
            while self.cur_size > 0:
                super(NStepReplay, self).add( self.state_seq[self.start_idx],
                                              self.action_seq[self.start_idx],
                                              self.R,
                                              self.state_next_seq[self.cur_idx - 1],
                                              self.td_seq[self.cur_idx - 1],
                                              self.done_seq[self.cur_idx - 1])

                self.R = (self.R - self.reward_seq[self.start_idx]) / self.gamma
                self.start_idx += 1
                self.cur_size -= 1
                self.start_idx = self.start_idx % self.n_steps
            self.R = 0.0

        # 4. start filling replay buffer if enough samples available to compute n-step reward
        if self.cur_size >= self.n_steps:
            super(NStepReplay, self).add(self.state_seq[self.start_idx],
                                         self.action_seq[self.start_idx],
                                         self.R,
                                         self.state_next_seq[self.cur_idx - 1],
                                         self.td_seq[self.cur_idx - 1],
                                         self.done_seq[self.cur_idx - 1])

            self.R = (self.R - self.reward_seq[self.start_idx]) / self.gamma
            self.start_idx += 1
            self.cur_size -= 1

        # 5. make sure index of circular buffer stayes within array range
        self.start_idx = self.start_idx % self.n_steps
        self.cur_idx = self.cur_idx % self.n_steps


# ----------------------------------------------------------------------------------------------------------------------
# Replay buffer that saves individual samples
# ----------------------------------------------------------------------------------------------------------------------
class PriorizedReplay:
    def __init__(self, buffer_size, alpha = 0.5, epsilon = 0.01):
        self.buffer_size = buffer_size
        self.cnt = 0
        self.alpha = alpha
        self.epsilon = epsilon
        self.buffer = SumTree(buffer_size)

    def is_full(self):
        return self.cnt >= self.buffer_size

    def add(self, sample, td):
        priority = self.priority(abs(td))
        self.buffer.add(priority, sample)
        self.cnt += 1

    def priority(self, error):
        return ( error + self.epsilon ) ** self.alpha

    def sample(self, batch_size):
        batch = []
        indices = []

        segment = self.buffer.total() / batch_size
        for i in range(batch_size):
            a = segment * i
            b = segment * (i + 1)
            s = random.uniform(a, b)
            idx, error, data = self.buffer.get(s)
            batch.append(data)
            indices.append(idx)
        return batch, indices

    def update(self, idx, error):
        p = self.priority(error)
        self.buffer.update(idx, p)

    def clear(self):
        self.buffer = deque()
        self.cnt = 0


# ----------------------------------------------------------------------------------------------------------------------
# Double Deep Q-Lerning algorithm
# ----------------------------------------------------------------------------------------------------------------------
class DDQN:
    def __init__(self, tf_sess,
                 state_size = 20,
                 action_size = 3,
                 buffer_size = 1000000,
                 epsilon = 0.9,
                 alpha = 0.0004,
                 gamma = 0.99,
                 target_update_steps = 5000,
                 n_train_steps = 1,
                 batch_size = 256,
                 n_steps = 15,
                 ):
        """
        Implementation of Double Qlearning algorithm
        :param state_size: dimension of states
        :param action_size: number of actions
        :param buffer_size: replay buffer size
        :param epsilon: initial epsilon
        :param alpha: learning rate
        :param gamma: discount factor
        :param target_update_steps: steps between target update
        :param n_train_steps: steps between network update ( backporp)
        :param batch_size: number of elements in one batch
        :param n_steps: number of steps used for n-step rew calculations
        """

        self.tf_sess = tf_sess

        # 1. set hyper parameters
        self.state_size = state_size
        self.action_size = action_size
        self.buffer_size = buffer_size
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma
        self.target_update_steps = target_update_steps
        self.n_train_steps = n_train_steps
        self.batch_size = batch_size
        self.n_steps = n_steps

        # 1. create networks
        self.model = DQNNetwork(self.tf_sess, state_size, action_size, self.alpha, "q")
        self.target_model = DQNNetwork(self.tf_sess, state_size, action_size, self.alpha, "target_q")

        # 3. setup replay buffer( can be interchanged )
        self.replay = NStepReplay(self.buffer_size, self.state_size, self.n_steps, self.gamma)
        self.use_per = isinstance(self.replay, PriorizedReplay)
        self.cnt = 0

    def learn(self, state, action, reward, next_state, done):
        """
        Insert new sample into replay buffer, do learning if buffer is completely filled
        """
        self.cnt += 1

        # 2. add sample to replay buffer, use reward as td error during filling phase
        if not self.replay.is_full():
            self.replay.add(state, action, reward, next_state, reward, done)
        else:
            if self.use_per:
                states, targets, td = self._targets([None])
                self.replay.add(state, action, reward, next_state, td[0], done)
            else:
                self.replay.add(state, action, reward, next_state, 0.0, done)

            # 3. once replay buffer is filled, start with experience replay (udpate every n_train_steps)
            if self.cnt % self.n_train_steps == 0:
                return self._experience_replay() # trained
            else:
                return 0.0 # skip
        return -1.0

    def _experience_replay(self):
        """
        get batch form replay buffer, do gradient decent
        """
        # 3. Update target every swap steps
        if self.cnt % self.target_update_steps == 0:
            self._copy_model_parameters(self.model, self.target_model)

        # 1. Compute target values
        if self.use_per:
            batch, indices = self.replay.sample(self.batch_size)
            states_batch, targets_batch, action_batch, td_batch = self._targets(batch)
            for i in range(self.batch_size):
                self.replay.update(indices[i], td_batch[i])
        else:
            batch = self.replay.sample(self.batch_size)
            states_batch, action_batch, targets_batch, _ = self._targets(batch)

        # 2. Update model
        return self.model.optimize(states_batch, action_batch, targets_batch)

    def _targets(self, batch):
        """
        compute the target values and td errors of the current batch
        """
        # 1. get references to transitions
        states_batch, action_batch, reward_batch, next_states_batch, done_batch = self.replay.split_sample(batch)

        # 2. forward pass to obtain estimation for state and next_state
        q_values_next = self.model.predict(next_states_batch)
        q_values_next_t = self.target_model.predict(next_states_batch)

        # 3. Q(s,a) = r + gamma * argmax_a Q(s', a)
        q_values_next = np.argmax(q_values_next, axis=1)
        end_multiplier = -(done_batch - 1)

        double_q = q_values_next_t[range(self.batch_size), q_values_next.reshape((self.batch_size))]
        targets_batch = reward_batch + (self.gamma * double_q * end_multiplier)

        # 4. compute td error if priorized replay is activated
        if self.use_per:
            td_batch = targets_batch - self.model.predict(states_batch)
            return states_batch, action_batch, targets_batch, td_batch

        return states_batch, action_batch, targets_batch, None

    def generate_action(self, state):
        """
        get eps-greedy actions
        """
        if random.uniform(0, 1) < self.epsilon:
            return random.randint(0, self.action_size - 1)
        else:
            state_np = np.array(state, dtype=np.float32).reshape((1, self.state_size))
            return np.argmax(self.model.predict(state_np))

    def _copy_model_parameters(self, model_1, model_2):
        """
        copy model parameters form model 1 to 2
        """
        e1_params = [t for t in tf.trainable_variables() if t.name.startswith(model_1.scope)]
        e1_params = sorted(e1_params, key=lambda v: v.name)
        e2_params = [t for t in tf.trainable_variables() if t.name.startswith(model_2.scope)]
        e2_params = sorted(e2_params, key=lambda v: v.name)
        update_ops = []
        for e1_v, e2_v in zip(e1_params, e2_params):
            op = e2_v.assign(e1_v)
            update_ops.append(op)

        self.tf_sess.run(update_ops)

