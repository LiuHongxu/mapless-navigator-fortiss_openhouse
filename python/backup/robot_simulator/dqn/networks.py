import tensorflow as tf
import numpy as np


# ----------------------------------------------------------------------------------------------------------------------
# DQN Network
# ----------------------------------------------------------------------------------------------------------------------
class DQNNetwork:
    """Q-Value Estimator neural network
        Structure: state_size -> 128 -> 64 -> 32 -> 16 -> action_size
        Input: state (batch_size x state_size)
               action (batch size x 1)
        Output: qvalues (batch_size x action_size)
    """

    def __init__(self, sess: tf.Session, state_size: int, action_size: int, alpha: float, scope="dqn"):
        """ init the model with hyper-parameter """
        self.sess = sess
        self.state_size = state_size
        self.action_size = action_size
        self.alpha = alpha
        self.scope = scope

        # save parameters in tf scope
        with tf.variable_scope(scope):
            self._build_model()

    def _build_model(self):
        self.states_pl = tf.placeholder(shape=[None, self.state_size], dtype=tf.float32)     # states
        self.a_pl = tf.placeholder(shape=[None], dtype=tf.int32)                        # action

        x = self.states_pl
        x = tf.layers.dense(x, 128, activation=tf.nn.relu)
        x = tf.layers.dense(x, 64, activation=tf.nn.relu)
        x = tf.layers.dense(x, 32, activation=tf.nn.relu)
        x = tf.layers.dense(x, 16, activation=tf.nn.relu)

        # estimation of q values for all posible actions
        self.Q = tf.layers.dense(x, self.action_size, activation=None)

        # estimation of q value for selected action
        actions_onehot = tf.one_hot(self.a_pl, self.action_size, dtype=tf.float32)
        self.q = tf.reduce_sum(tf.multiply(self.Q, actions_onehot), axis=1)

        # huber loss for optimization
        self.targets = tf.placeholder(shape=[None], dtype=tf.float32)
        self.loss = tf.losses.huber_loss(self.targets, self.q, delta=1.0)
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.alpha).minimize(self.loss)

    def predict(self, state: np.ndarray):
        """ Run forward pass from state to q_values """
        return self.sess.run(self.Q, {self.states_pl: state})

    def optimize(self, batch_state: np.ndarray, batch_action: np.ndarray, batch_q_target: np.ndarray):
        """ backward pass on batch"""
        feed_dict = {self.states_pl: batch_state, self.a_pl: batch_action, self.targets: batch_q_target}
        loss, _ = self.sess.run([self.loss, self.optimizer], feed_dict)
        return loss


# ----------------------------------------------------------------------------------------------------------------------
# DRQN Network
#
# Structure: state_size -> 128 -> 64 -> 32 -> lstm(32) -> action_size
# Input: state (batch_size * sequence_len, state_size)
# Output: qvalues (batch_size * action_size,)
# ----------------------------------------------------------------------------------------------------------------------
class DRQNNetwork:
    def __init__(self, sess: tf.Session, state_size: int, action_size: int, alpha: float, scope="dqn"):
        """
        :param sess: tensorflow session
        :param state_size: state dimension
        :param action_size: number of actions
        :param sequence_len: sequence_len
        :param scope: name of tf scope
        :param alpha: learning rate
        :param scope: name of tf scope
        """
        self.sess = sess
        self.state_size = state_size
        self.action_size = action_size
        self.alpha = alpha
        self.scope = scope

        with tf.variable_scope(scope):
            self._build_model()

    def _build_model(self):
        self.batch_size_pl = tf.placeholder(tf.int32, shape=(), name="batch_size")
        self.sequence_len_pl = tf.placeholder(tf.int32, shape=(), name="sequence_len")
        self.grad_split_pl = tf.placeholder(tf.int32, shape=(), name="grad_split")

        self.states_pl = tf.placeholder(shape=[None, self.state_size], dtype=tf.float32)     # states
        self.a_pl = tf.placeholder(shape=[None], dtype=tf.int32)                             # action

        # 1. feature extraction part
        x = self.states_pl
        x = tf.layers.dense(x, 128, activation=tf.nn.relu)
        x = tf.layers.dense(x, 64, activation=tf.nn.relu)
        x = tf.layers.dense(x, 32, activation=tf.nn.relu)

        # 2. lstm part
        # In: x, state_in
        # Out: x, state_out
        x = tf.reshape(x, shape=[self.batch_size_pl, self.sequence_len_pl, 32])

        self.cell = tf.nn.rnn_cell.LSTMCell(32)
        self.state_in = self.cell.zero_state(self.batch_size_pl, dtype=tf.float32)
        x, self.state_out = tf.nn.dynamic_rnn(
            self.cell,
            x,
            initial_state=self.state_in,
            dtype=tf.float32)
        x = tf.reshape(x, [-1, 32])

        # 3. estimation of q values for all possible actions
        self.Q = tf.layers.dense(x, self.action_size, activation=None)

        # 4. estimation of q value for selected action
        actions_onehot = tf.one_hot(self.a_pl, self.action_size, dtype=tf.float32)  # (batch * trace_len, action_size)
        self.q = tf.reduce_sum(tf.multiply(self.Q, actions_onehot), axis=1)         # (batch * trace_len,)

        # 5. mse loss for optimization
        # only half of the gradients are propagated through network (see paper)
        maskA = tf.zeros([self.batch_size_pl, self.grad_split_pl])
        maskB = tf.ones([self.batch_size_pl, self.grad_split_pl])
        mask = tf.concat([maskA, maskB], 1) # (batch, trace_len)
        self.mask = tf.reshape(mask, [-1])  # (batch * trace_len,)

        self.targets = tf.placeholder(shape=[None], dtype=tf.float32)
        err = (self.targets - self.q) * self.mask
        self.loss = tf.reduce_mean( tf.where(tf.abs(err) < 1.0, 0.5 * tf.square(err), tf.abs(err) - 0.5) )
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.alpha).minimize(self.loss)

    def predict(self, state: np.ndarray, state_in, batch_size: int, sequence_len: int):
        """
        predict Q Values
        :param state: input state matrix of shape (batch_size * sequence_len, state_size)
        :param state_in: state used to initialize lstm
        :param batch_size: number of samples in state
        :param sequence_len: sequence length in state
        :return:
        """
        feed_dict = {self.states_pl: state,
                     self.state_in: state_in,
                     self.batch_size_pl: batch_size,
                     self.sequence_len_pl: sequence_len}
        return self.sess.run(self.Q, feed_dict)

    def optimize(self, batch_state: np.ndarray, state_in, batch_action: np.ndarray,
                 batch_q_target: np.ndarray, batch_size: int, sequence_len: int):
        """
        backward pass on batch
        :param batch_state: batch of states, shape (batch_size * sequence_len, state_size)
        :param state_in: state used to initialize lstm
        :param batch_action: actions, shape (batch_size * sequence_len, 1)
        :param batch_q_target: targets, shape (batch_size * sequence_len, 1)
        :param batch_size: number of samples in state
        :param sequence_len: sequence length in state
        :return: mean batch loss
        """
        feed_dict = {self.states_pl: batch_state,
                     self.state_in: state_in,
                     self.a_pl: batch_action,
                     self.targets: batch_q_target,
                     self.batch_size_pl: batch_size,
                     self.sequence_len_pl: sequence_len,
                     self.grad_split_pl: int(sequence_len / 2)}
        loss, _ = self.sess.run([self.loss, self.optimizer], feed_dict)
        return loss
