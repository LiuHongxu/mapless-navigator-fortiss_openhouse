from simulator.agentenvironment import *
import math
from collections import deque
from itertools import chain


class DQNEnvironment(AgentEnvironement):
    def __init__(self, env, agent, n_action, n_state, goal_position):
        super(DQNEnvironment, self).__iter__(env, agent, n_action, n_state)
        self.goal_position = goal_position

        self.min_goal_dist = 0.25
        self.min_obstical_dist = 0.22
        self.n_rays = 18
        self.max_range = 2.0
        self.hist_len = 10

        self.env_state = EnvironmentState()

        self.__setup__()

    def __setup__(self):
        self.clear_goal_area = b2AABB()
        self.clear_goal_area.upperBound = b2Vec2(self.min_goal_dist / 2.0,
                                                 self.min_goal_dist / 2.0) + self.goal_position
        self.clear_goal_area.lowerBound = b2Vec2(-self.min_goal_dist / 2.0,
                                                 -self.min_goal_dist / 2.0) + self.goal_position

        self.prev_action = -1.0
        self.prev_distance = -1.0
        self.hist_distance = deque( [0] * self.hist_len, self.hist_len )
        self.hist_theta = deque( [0] * self.hist_len, self.hist_len )
        self.hist_action = deque( [0] * self.hist_len, self.hist_len )

    def set_goal_position(self, goal_position):
        self.goal_position = goal_position

        self.clear_goal_area.upperBound = b2Vec2(self.min_goal_dist / 2.0,
                                                 self.min_goal_dist / 2.0) + self.goal_position
        self.clear_goal_area.lowerBound = b2Vec2(-self.min_goal_dist / 2.0,
                                                 -self.min_goal_dist / 2.0) + self.goal_position

    def execute(self, action):
        self.prev_action = action

        vel_cmd = VelCmd()
        vel_cmd.velocity.y = 0.0

        if action == 0:
            vel_cmd.velocity.x = 0.3
            vel_cmd.omega = 0.0
        elif action == 1:
            vel_cmd.velocity.x = 0.04
            vel_cmd.omega = 0.5
        elif action == 2:
            vel_cmd.velocity.x = 0.04
            vel_cmd.omega = -0.5

        self.agent.command( vel_cmd )

    def observe(self):
        # 1. do observation (no new sensor update return none)
        available, sensorreading = self.agent.observe()
        if not available:
            return None
        modelstate = self.agent.model()

        # 2. simulate callbacks
        reached_goal, distance, theta = self.model_state_callback(modelstate, self.min_goal_dist);
        hit_obstacle, normalized_ranges = self.scan_callback(sensorreading, self.min_obstical_dist);

        if self.prev_distance < 0:
            self.prev_distance = distance

        # 3. compute reward
        reward = 0.0
        if hit_obstacle:
            reward = -150.0
        elif reached_goal:
            reward = 100.0
        reward += min(1.5, max(-1.5, 15.0 * (self.prev_distance - distance)))
        #reward -= 1.0 * self.potential_distance(sensorreading, self.min_obstical_dist, 0.258 )
        reward = min(100.0, max(-150.0, reward))

        # 4. combine into state vector ( normalized to [0, 1] )
        self.env_state.done = reached_goal | hit_obstacle
        self.env_state.success = reached_goal
        self.env_state.reward = reward

        l = []
        l.extend(normalized_ranges)
        l.extend([1.0 / distance * self.min_goal_dist])

        # combine: normalized_ranges, dist to goal, angle to goal
        self.env_state.state = list(chain(
            normalized_ranges,
            [1.0 / distance * self.min_goal_dist],
            #list(self.hist_distance),
            [theta / sim.PI + 1.0]
            #list(self.hist_theta)
        ))

        # update history
        self.prev_distance = distance
        self.hist_distance.appendleft( 1.0 / distance * self.min_goal_dist )
        self.hist_theta.appendleft( theta / sim.PI + 1.0 )
        self.hist_action.appendleft( self.prev_action / 2.0 )

        # 5. done, success, state, reward
        return self.env_state

    def reset(self, clear_area = None):
        # 1. reset the environment
        super(DQNEnvironment, self).reset(self.clear_goal_area)

        # 2. reset the history
        self.hist_distance = deque( [0] * self.hist_len, self.hist_len )
        self.hist_theta = deque( [0] * self.hist_len, self.hist_len )
        self.hist_action = deque( [0] * self.hist_len, self.hist_len )

        # 3. do initial observation
        return self.observe()

    def model_state_callback(self, modelstate, min_range):
        # distance to goal
        distance_vec = self.goal_position - modelstate.position
        distance = math.sqrt(distance_vec.x * distance_vec.x + distance_vec.y * distance_vec.y)

        # angle to goal
        theta = self.rotation_angle(math.atan2(distance_vec.y, distance_vec.x), modelstate.theta)

        return distance < min_range, distance, theta

    def scan_callback(self, range_vec, min_range):
        done = False
        normalized_range_vec = range_vec.copy()
        for i in range(0, len(range_vec)):
            normalized_range_vec[i] = 1.0 / range_vec[i] * min_range
            if range_vec[i] < min_range:
                done = True
        return done, normalized_range_vec

    def rotation_angle(self, src, dst):
        if src > dst:
            return -sim.PI + math.fmod(src - dst + sim.PI, sim.PI_2)
        else:
            return sim.PI - math.fmod(src - dst + sim.PI, sim.PI_2)

    def potential_distance(self, d_vec, d_min, tau):
        mean = 0.0
        for d in d_vec:
            mean += 12.0 * math.exp( -(d - d_min) / tau )
        return mean / len(d_vec)





