# Force tf to switch to cpu instead of graphic card (but its slower...)
# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from sessions.singlerobotsession import *
from simulator.randomenvironment import *


def main():
    realtime_factor = 0

    # 1. create environment
    environment = RandomEnvironment(b2Vec2(0.0, 0.0), loop_frequency=30)

    # 2. create a session / test
    session = SingleRobotDQN(environment, "/home/python/fortiss/", 2000)
    session.set_mode( TRAIN_MODE )
    session.setup()

    # 3. go!!!
    session.start(total_episodes=150000)
    environment.start(True)

    while(environment.is_started):
        # 1. simulate step
        realtime_factor = environment.update()

        # 2. session update
        if session.update() == 1:
            environment.start(False)

        # 3. show graphics
        environment.update_graphics()

    # 4. finished, save everything
    session.save_all()

if __name__ == "__main__":
    main()