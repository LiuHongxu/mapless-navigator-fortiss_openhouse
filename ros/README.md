# Robot operating system

This directory contains the implementation of the DDQN on the Turtlebot in ros
* [lecture_code](lecture_code) contains the code verison of the lecture (based on MiniDNN)
* [fortiss_code](fortiss_code) contains the updated version 

## Delpendencies 

* [matio](https://github.com/tbeu/matio) (for lecture_code)
* [MiniDNN](https://github.com/yixuan/MiniDNN) (for lecture_code)
* [turtlebot packages](http://wiki.ros.org/turtlebot) (ros)
* [tensorflow](https://www.tensorflow.org/install/pip) for python 2.7

## Setup
* put the workspace folder [mapless_nav_turtlebot_ws](fortiss_code/mapless_nav_turtlebot_ws) on the turtlebot and build it
* put the workspace folder [mapless_nav_ws](fortiss_code/mapless_nav_ws) on a host computer that shares a roscore with the turtlebot
* make sure ROS_HOST_NAME and ROS_IP are configured correctly on turtlebot and host

## Run Project

1. launch on the turtlebot the system launch file to get access to all sensor topics and move base
    ```
    roslaunch mapless_navigation_bringup system.launch
    ```
2. launch on the host computer the system launch file for a single turtlebot with namespace robot_0
    ```
    roslaunch mapless_navigation_bringup system_robot_0.launch
    ```
    launch system_full launch file if multible turtlebots are online
    ```
    roslaunch mapless_navigation_bringup system_full.launch
    ```

3. open rviz and load the config file in [rviz config](fortiss_code/mapless_nav_ws/src/mapless_navigation_bringup/launch/config/rviz_config)
    ```
    rosrun rviz rviz
    ```

4. launch the goal scheduler to automatically assign goal positions to the turtlebots or use rviz to specify a goal
    ```
    rosrun goal_scheduler goal_scheduler.launch
    ```
    Open rqt_reconfigure to change between automatic or manual goal specification
    ```
    rosrun rqt_reconfigure rqt_reconfigure
    ```

## Networks
* Trained tensorflow networks can be placed in [models](fortiss_code/mapless_nav_ws/src/rl_tensorflow/models)
* change the [rl_tensorflow_node](fortiss_code/mapless_nav_ws/src/rl_tensorflow/scripts/rl_tensorflow_node.py) to load differnt models


## Debugging
* make sure the namespace (ex. robot_0, robot_1) in launch file on turtlebot matches the namespace used in launch files of host computer
* use rqt_graph to detect missing topics between nodes 

