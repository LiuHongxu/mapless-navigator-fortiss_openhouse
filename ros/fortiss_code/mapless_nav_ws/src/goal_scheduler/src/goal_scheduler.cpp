#include "goal_scheduler/goal_scheduler.h"

GoalScheduler::GoalScheduler()
    : is_automatic(false)
{
    // setup input variables

    // load ros parameters (private ns)
    ros::param::get("~robot_name", robot_name);
    ros::param::get("~max_running_time", max_running_time);
    ros::param::get("~num_of_robots", num_of_robots);

    // field frame
    field_pts = { {-0.8, -1.0}, {3.3, -0.9}, {3.57, 1.9}, {-0.4, 1.8} };
    current_robot = 0;

    for(size_t i = 0; i < field_pts.size(); ++i) {
        bound.x_min = std::min( bound.x_min, field_pts[i].x );
        bound.x_max = std::max( bound.x_max, field_pts[i].x );
        bound.y_min = std::min( bound.y_min, field_pts[i].y );
        bound.y_max = std::max( bound.y_max, field_pts[i].y );
    }

    done_subs.resize(num_of_robots); 
    goal_pubs.resize(num_of_robots);
    goal_timers.resize(num_of_robots);
    is_running.resize(num_of_robots);

    // ros services
    reconfig_srv.setCallback(boost::bind(&GoalScheduler::reconfig, this, _1, _2));

    // ros substriber
    rviz_goal_sub = nh.subscribe("/move_base_simple/goal", 10, &GoalScheduler::rviz_goal_callback, this);
    for(size_t i = 0; i < num_of_robots; ++i) {
        is_running[i] = false;
        done_subs[i] = nh.subscribe<std_msgs::Bool>(robot_name + std::to_string(i) + "/turtlebot_env/done", 10, boost::bind(&GoalScheduler::done_callback, this, _1, i) );
        goal_pubs[i] = nh.advertise<geometry_msgs::PoseStamped>(robot_name + std::to_string(i) + "/move_base_simple/goal", 10);
        goal_timers[i] = nh.createTimer(ros::Duration(max_running_time),  boost::bind(&GoalScheduler::goal_timer_callback, this, _1, i), true);
    }

    // setup random num generators
    rng = std::mt19937(rd());
    real_uni_x = std::uniform_real_distribution<float>(bound.x_min + 0.5, bound.x_max - 0.5);
    real_uni_y = std::uniform_real_distribution<float>(bound.y_min + 0.5, bound.y_max - 0.5);

    ROS_INFO_STREAM("num_of_robots=" << num_of_robots);
    ROS_INFO_STREAM("max_running_time=" << max_running_time);
    ROS_INFO_STREAM("num_of_robots=" << num_of_robots);
    ROS_INFO_STREAM("Random Sampling form x=[" << bound.x_min + 0.5 << ", " << bound.x_max - 0.5 << "]");
    ROS_INFO_STREAM("Random Sampling form y=[" << bound.y_min + 0.5 << ", " << bound.y_max - 0.5 << "]");
}

GoalScheduler::~GoalScheduler() {}

void GoalScheduler::update()
{
    if(is_automatic) {
        for(int i = 0; i < is_running.size(); ++i) {
            //if not running give it a new goal
            if( !is_running[i] )
                set_goal(i);
        }
    }
}

void GoalScheduler::set_goal(int id, float x, float y)
{
    // mark robot as running and set timer
    is_running[id] = true;
    goal_timers[id].start();

    // randomly sample a new goal
    if( x==0.0f && y==0.0f) {
        x = real_uni_x(rng);
        y = real_uni_y(rng);
    }

    // publish message
    geometry_msgs::PoseStamped posestamped;
    posestamped.pose.position.x = x;
    posestamped.pose.position.y = y;
    posestamped.pose.orientation.z = 0.0;
    goal_pubs[id].publish(posestamped);
    ROS_INFO_STREAM("Schedule: robot=" << id << " x=" << x << " y=" << y);
}

void GoalScheduler::rviz_goal_callback(const geometry_msgs::PoseStamped::ConstPtr& posestamped)
{
    // set goal flag
    rviz_goal.x = posestamped->pose.position.x;
    rviz_goal.y = posestamped->pose.position.y;

    /* publish goal to current robot
    for(int i = 0; i < is_running.size(); ++i) {
        set_goal(i, rviz_goal.x, rviz_goal.y);
        ROS_INFO_STREAM("Rviz Goal: robot=" << i << " x=" <<  rviz_goal.x << " y=" << rviz_goal.y);
    }*/
    set_goal(current_robot, rviz_goal.x, rviz_goal.y);
    ROS_INFO_STREAM("Rviz Goal: robot=" << current_robot << " x=" <<  rviz_goal.x << " y=" << rviz_goal.y);
}

void GoalScheduler::done_callback(const std_msgs::Bool::ConstPtr& msg, size_t id)
{
    // mark is_running flag as false and reset timer
    is_running[id] = false;
    goal_timers[id].stop();
    goal_timers[id].setPeriod( ros::Duration(max_running_time) );

    ROS_INFO_STREAM("Done: robot=" << id);
}

void GoalScheduler::goal_timer_callback(const ros::TimerEvent& timerevent, size_t id)
{
    ROS_INFO_STREAM("Time Limit: robot=" << id);
    if( is_automatic )
        set_goal(id);
}

void GoalScheduler::reconfig(goal_scheduler::gsConfig &newconfig, uint32_t level)
{
    is_automatic = newconfig.automatic_goal;
    current_robot = newconfig.robot_select;

    ROS_INFO_STREAM("is_automatic=" << is_automatic);
    ROS_INFO_STREAM("current_robot=" << current_robot);    
}