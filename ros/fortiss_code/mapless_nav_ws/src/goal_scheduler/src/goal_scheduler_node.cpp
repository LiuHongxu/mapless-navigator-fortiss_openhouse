#include "ros/ros.h"
#include "goal_scheduler/goal_scheduler.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "goal_scheduler");
    ROS_INFO_STREAM("Starting goal scheduler");

    GoalScheduler goal_scheduler;

    ros::Rate rate(5);
    while (ros::ok())
    {
        goal_scheduler.update();
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}