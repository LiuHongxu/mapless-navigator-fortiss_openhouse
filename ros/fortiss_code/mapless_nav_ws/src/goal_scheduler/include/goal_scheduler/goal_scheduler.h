#ifndef GOALSCHEDULER_H_
#define GOALSCHEDULER_H_

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <std_msgs/Bool.h>
#include <dynamic_reconfigure/server.h>

#include <random>
#include <vector>

#include "goal_scheduler/utilities.h"
#include "goal_scheduler/gsConfig.h"

class GoalScheduler
{
public:
    GoalScheduler();
    ~GoalScheduler();

    /* call this in main loop */
    void update();

    /* reconfiguration callback */
    void reconfig(goal_scheduler::gsConfig &newconfig, uint32_t level);

private:
    ros::NodeHandle nh;

    ros::Subscriber rviz_goal_sub;              // take input form rviz
    dynamic_reconfigure::Server<goal_scheduler::gsConfig> reconfig_srv;

    std::vector<ros::Subscriber> done_subs;     // report if goal reached
    std::vector<ros::Publisher> goal_pubs;      // send new goal
    std::vector<ros::Timer> goal_timers;        // run timer to reset goal some timer
    std::vector<bool> is_running;               // true if robot still on its way

    geometry_msgs::Pose2D rviz_goal;            // robot goal

    bool is_automatic;                          // true if goal are selected automatically
    int num_of_robots;                          // number of robots online
    int current_robot;                          // controlled robot
    int max_running_time;                       // maximum time allowed for reaching goal
    std::string robot_name;

    std::vector<vec2d::Vec2d> field_pts;        // corner field wrt fixed_frame
    vec2d::BoundingRect bound;

    std::random_device rd;  //  (seed) engine
    std::mt19937 rng;       // random-number engine
    std::uniform_real_distribution<float> real_uni_x, real_uni_y;

    void set_goal(int id, float x = 0.0f, float y = 0.0f);

    // callbacks
    void rviz_goal_callback(const geometry_msgs::PoseStamped::ConstPtr& posestamped);
    void done_callback(const std_msgs::Bool::ConstPtr& msg, size_t id);
    void goal_timer_callback(const ros::TimerEvent& timerevent, size_t id);
};

#endif