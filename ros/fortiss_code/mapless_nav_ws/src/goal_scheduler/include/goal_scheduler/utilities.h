#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <math.h>

#define PI M_PI
#define PI_2 2 * M_PI

/* define some functions for vector operations */
namespace vec2d 
{
	struct Vec2d {
		float x;
		float y;
		Vec2d(float x = 0.0, float y = 0.0)
			: x(x), y(y) {}

		friend Vec2d operator+(Vec2d a, const Vec2d& b){ a.x += b.x; a.y += b.y; return a; }
		friend Vec2d operator-(Vec2d a, const Vec2d& b){ a.x -= b.x; a.y -= b.y; return a; }
		friend Vec2d operator*(Vec2d a, float k){ a.x *= k; a.y *= k; return a; }
        friend Vec2d operator*(float k, Vec2d a){ a.x *= k; a.y *= k; return a; }
	};

    inline float distance(Vec2d& a) {
        return sqrtf(a.x * a.x + a.y * a.y);
    }

    inline float distance_sq(Vec2d& a) {
        return a.x * a.x + a.y * a.y;
    }

	struct BoundingRect {
		float x_min, y_min;
		float x_max, y_max;

		BoundingRect(float x_min = 1000.0, float x_max = -1000.0, float y_min = 1000.0, float y_max = -1000.0)
			: x_min(x_min), x_max(x_max), y_min(y_min), y_max(y_max) {}
	};
}

#endif