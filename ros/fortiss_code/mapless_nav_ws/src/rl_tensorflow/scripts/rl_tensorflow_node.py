#!/usr/bin/env python

import rospy
import numpy as np
import tensorflow as tf

from std_msgs.msg import *
from turtlebot_env.msg import *
from turtlebot_env.srv import *

from networks import DQNNetwork
from networks import DRQNNetwork

# Settings
STATE_SIZE = 20     # Number of states
ACTION_SIZE = 3     # Number of actions
CHECK_POINT_DIR_DDQN = '/home/simon/workspaces/mapless_navigation/src/rl_tensorflow/models/ddqn'
CHECK_POINT_DIR_DRQN = '/home/simon/workspaces/mapless_navigation/src/rl_tensorflow/models/drqn'

# Base Class for executing of tf graph
class ModelExecutor(object):
    def __init__(self, check_point_dir):
        # 1. get ros parameters
        robot_name = rospy.get_param('~robot_name')    

        # 2. setup ros services and subsriber
        self.execute_srv = rospy.ServiceProxy(robot_name + '/turtlebot_env/execute', execute, persistent=True)
        self.reset_srv = rospy.ServiceProxy(robot_name + '/turtlebot_env/reset', reset, persistent=True)
        self.recovery_srv = rospy.ServiceProxy(robot_name + '/turtlebot_env/recover', recovery, persistent=True)

        self.env_state_sub = rospy.Subscriber(robot_name + '/turtlebot_env/env_state', env_state, self.env_state_callback)
        self.start_sub = rospy.Subscriber(robot_name + '/turtlebot_env/start', Empty, self.start_callback)

        # 3. setup tensorflow
        self.tf_sess = tf.Session()

        # 4. setup model
        self._do_setup_model()

        # 5. load last checkpoint
        self.tf_saver = tf.train.Saver()
        latest_checkpoint = tf.train.latest_checkpoint(check_point_dir)
        if latest_checkpoint:
            rospy.loginfo("Loading model checkpoint {}...\n".format(latest_checkpoint))
            self.tf_saver.restore(self.tf_sess, latest_checkpoint)
        else:
            rospy.logerr("Loading model checkpoint failed!\nFile: {}".format(check_point_dir))

        # setup variables
        self.env_state = []
        self.action = 0
        self.is_observation_available = False
        self.is_started = False

        # wait till services are available
        rospy.loginfo('Wait for env services to become available...')
        rospy.wait_for_service(robot_name + '/turtlebot_env/execute')
        rospy.wait_for_service(robot_name + '/turtlebot_env/reset')
        rospy.wait_for_service(robot_name + '/turtlebot_env/recover')
        rospy.loginfo('Env available, start rl session')

    def _do_setup_model(self):
        '''
        reimplement this function:
        construct the tf network graph by initializing the network
        '''
        raise NotImplementedError

    def _do_generate_action(self, state):
        '''
        reimplement this function:
        use the state to generate a new action 
        return: action to take [0, n_action]
        '''
        raise NotImplementedError

    def _do_reset(self):
        '''
        reimplement this function:
        reset the model if needed
        '''
        pass

    def update(self):
        # 0. check if taks is started
        if not self.is_started:
            return False

        # 1. check if new observation available, repeat last action if not
        if not self.is_observation_available:
            self.execute(self.action)
            return False
        self.is_observation_available = False
        
        # 2. execute next action
        self.action = self._do_generate_action( self.env_state.state )
        self.execute( self.action )

        # 3. check if task finished
        if self.env_state.done:
            self.is_started = False
            self._do_reset()
            if self.env_state.success:
                rospy.loginfo('goal position reached, task finished, call: reset srv')
                self.execute( -1 )
                self.reset()
            else:
                rospy.loginfo('crashed, call recover srv')
                self.recovery()

        return True

    def execute(self, action):
        try:
            res = self.execute_srv(action)
        except rospy.ServiceException, e:
            return None

    def reset(self):
        try:
            res = self.reset_srv()
            self.is_observation_available = False
        except rospy.ServiceException, e:
            return None

    def recovery(self):
        try:
            res = self.recovery_srv()
            self.is_observation_available = False
        except rospy.ServiceException, e:
            return None

    def env_state_callback(self, env_state_msg):
        self.is_observation_available = True
        self.env_state = env_state_msg

    def start_callback(self, empty_msg):
        rospy.loginfo('rl control started')
        rospy.sleep(1.0)
        self.is_started = True

# Use DDQN as Network
class ModelExecutorDDQN(ModelExecutor):
    def __init__(self):
        # 1. baseclass constructor
        super(ModelExecutorDDQN, self).__init__(CHECK_POINT_DIR_DDQN)

    def _do_setup_model(self):
        self.model = DQNNetwork(self.tf_sess, STATE_SIZE, ACTION_SIZE, 0.0004, "q")
        self.target_model = DQNNetwork(self.tf_sess, STATE_SIZE, ACTION_SIZE, 0.0004, "target_q")

    def _do_generate_action(self, state):
        state_np = np.array(state, dtype=np.float).reshape((1, STATE_SIZE))
        q_values = self.model.predict(state_np)
        max_idx = np.argmax( q_values )

        print(q_values)

        if max_idx != 0:
            if abs(q_values[0,1] - q_values[0,2]) < 1.5: # 2.5
                return self.action
        return max_idx

# Use DRQN as Network
class ModelExecutorDRQN(ModelExecutor):
    def __init__(self):
        # 1. baseclass constructor
        super(ModelExecutorDRQN, self).__init__(CHECK_POINT_DIR_DRQN)
        self.rnn_state_online = (np.zeros([1, 256]), np.zeros([1, 256]))
        self.rnn_state_reset = (np.zeros([1, 256]), np.zeros([1, 256]))

    def _do_setup_model(self):
        self.model = DRQNNetwork(self.tf_sess, STATE_SIZE, ACTION_SIZE, 0.0004, "q")
        self.target_model = DRQNNetwork(self.tf_sess, STATE_SIZE, ACTION_SIZE, 0.0004, "target_q")

    def _do_generate_action(self, state):
        state_np = np.array(state, dtype=np.float32).reshape((1, STATE_SIZE))
        q_values, self.rnn_state_online = self.model.predict(state_np, self.rnn_state_online, 1, 1)
        return np.argmax(q_values)

    def _do_reset(self):
        #self.rnn_state_online = self.rnn_state_reset
        pass


if __name__ == '__main__':
    rospy.init_node('rl_tensorflow_node')

    executor = ModelExecutorDDQN() #ModelExecutorDDQN()
    rospy.loginfo('tensorflow session started')

    rate = rospy.Rate(50) # 50hz
    while not rospy.is_shutdown():
        executor.update()
        rate.sleep()