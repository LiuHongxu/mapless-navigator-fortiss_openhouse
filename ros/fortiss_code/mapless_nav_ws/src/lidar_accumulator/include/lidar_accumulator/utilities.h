#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <math.h>

#define PI M_PI
#define PI_2 2 * M_PI

/* define some functions for vector operations */
namespace vec2d 
{
	struct Vec2d {
		float x;
		float y;
		Vec2d(float x = 0.0, float y = 0.0)
			: x(x), y(y) {}

		friend Vec2d operator+(Vec2d a, const Vec2d& b){ a.x += b.x; a.y += b.y; return a; }
		friend Vec2d operator-(Vec2d a, const Vec2d& b){ a.x -= b.x; a.y -= b.y; return a; }
		friend Vec2d operator*(Vec2d a, float k){ a.x *= k; a.y *= k; return a; }
        friend Vec2d operator*(float k, Vec2d a){ a.x *= k; a.y *= k; return a; }
	};

    struct Pose2d {
        Vec2d p;
        float theta;
    };

    inline float distance(Vec2d& a) {
        return sqrtf(a.x * a.x + a.y * a.y);
    }

    inline float distance_sq(Vec2d& a) {
        return a.x * a.x + a.y * a.y;
    }

    inline float rotation_angle(float src, float dst) {
        return (src > dst) ? -PI + std::fmod(src - dst + PI, PI_2)
                        :  PI - std::fmod(dst - src + PI, PI_2);
    }
}

/* Implements a queue */
template<typename T>
class Queue : public std::deque<T>
{
public:
    Queue(size_t max_size) : max_size(max_size)
    {}

    /* filo buffer behaviour 
	 * add element in the end remove, first element
	 * oldest [0] --- newest [size-1]
	 */
    void add(const T& val) {
        this->push_back(val);
		if(this->size() >= max_size)
        	this->pop_front();
    }
private:
	size_t max_size;
};

#endif