#include "ros/ros.h"
#include "lidar_accumulator/lidar_accumulator.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "lidar_accumulator");
    ROS_INFO_STREAM("Starting lidar filter");

    LidarAccumulator lidar_accumulator;

    ros::Rate rate(30);
    while (ros::ok())
    {
        lidar_accumulator.update();
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}