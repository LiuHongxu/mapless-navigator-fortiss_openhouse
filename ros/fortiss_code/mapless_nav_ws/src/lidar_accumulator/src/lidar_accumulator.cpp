#include "lidar_accumulator/lidar_accumulator.h"
#include "lidar_accumulator/utilities.h"
#include <math.h>

using namespace vec2d;

LidarAccumulator::LidarAccumulator()
    : do_insert_scan(false), is_modified(false), cur_idx(0), cnt(0)
{
    // load ros parameters (private ns)
    ros::param::get("~robot_name", robot_name);
    ros::param::get("~tf_prefix", tf_prefix);
    ros::param::get("~buffer_size", buffer_size);
    ros::param::get("~angle_min", angle_min);
    ros::param::get("~angle_max", angle_max);
    ros::param::get("~range_min", range_min);
    ros::param::get("~range_max", range_max);

    // setup input variables
    fixed_frame = tf_prefix + "/odom";
    robot_frame = tf_prefix + "/base_link";

    theta_th = 0.2;
    d_th = 0.2;
    clouds.resize(buffer_size);
    angle_increment = 0.0174532f;

    ds_step = 10;
    pub_freq = 30.f;
    pub_period = 1.f / pub_freq;

    // setup publisher and subscriber for each robot
    scan_sub = nh.subscribe<sensor_msgs::LaserScan>(robot_name + "/kinectscan", 10, &LidarAccumulator::scan_callback, this);
    scan_pub = nh.advertise<sensor_msgs::LaserScan>(robot_name + "/kinectscan_accu", 10);
    cloud_pub = nh.advertise<sensor_msgs::PointCloud2>(robot_name + "/kinectscan_points_accu", 10);
    pub_timer = nh.createTimer(ros::Duration(pub_period),  &LidarAccumulator::pub_timer_callback, this);

    /*buffer_size = 6;
    theta_th = 0.2;
    d_th = 0.2;
    clouds.resize(buffer_size);

    angle_min = -1.3962f;
    angle_max = 1.3962f;
    angle_increment = 0.0174532f;
    range_min = 0.1f;
    range_max = 2.5f;*/

    // print parameters
    ROS_INFO_STREAM("LidarAccumulator: robot_name=" << robot_name);
    ROS_INFO_STREAM("LidarAccumulator: tf_prefix=" << tf_prefix);
    ROS_INFO_STREAM("LidarAccumulator: buffer_size=" << buffer_size);
    ROS_INFO_STREAM("LidarAccumulator: angle_min=" << angle_min);
    ROS_INFO_STREAM("LidarAccumulator: angle_max=" << angle_max);
    ROS_INFO_STREAM("LidarAccumulator: range_min=" << range_min);
    ROS_INFO_STREAM("LidarAccumulator: range_min=" << range_max);
}

LidarAccumulator::~LidarAccumulator()
{
}

/* call this function in the main loop */
void LidarAccumulator::update()
{
    if( update_pose(fixed_frame, robot_frame, robot_pose) )
        do_insert_scan = motion(robot_pose, prev_robot_pose, theta_th, d_th);
}

/* get robot pose */
bool LidarAccumulator::update_pose(const std::string& fixed_frame, const std::string& robot_frame, Pose2d& pose)
{
    tf::StampedTransform transform;
    bool available = false;

    // 1. get transformation from tf tree
    available = tfListener.waitForTransform(fixed_frame, robot_frame, ros::Time(0), ros::Duration(0.1));
    try {
      tfListener.lookupTransform(fixed_frame, robot_frame, ros::Time(0), transform);      
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s", ex.what());
      return false;
    }
    if(!available)
        return false;

    // 2. extract robot pose from transformation
    double roll, pitch, yaw;
    transform.getBasis().getRPY(roll, pitch, yaw);

    pose.p.x = transform.getOrigin().x();
    pose.p.y = transform.getOrigin().y();
    pose.theta = yaw;

    return true;
}

bool LidarAccumulator::motion(const vec2d::Pose2d& pose, vec2d::Pose2d& prev_robot_pose, float theta_th, float d_th)
{   
    Vec2d dx = robot_pose.p - prev_robot_pose.p;
    float dtheta = robot_pose.theta - prev_robot_pose.theta;
    float d = distance(dx);

    //ROS_INFO_STREAM("dtheta=" << dtheta << " d=" << d);
    prev_robot_pose = robot_pose;
    return true;
}

/* scan for all laser range finder */
void LidarAccumulator::scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    if(!do_insert_scan)
        return;
    do_insert_scan = false;

	sensor_msgs::PointCloud2 tmpCloud1, tmpCloud2;

    // downsample input by a factor of ds_step
    downsample_laserscan(scan, ds_scan, ds_step);

    // filter noise points
    int h = 5;
    sensor_msgs::LaserScan::_ranges_type temp = ds_scan.ranges;
    for(int i = h; i < temp.size() - h; ++i) {
        auto start =  temp.begin()+i-h;
        auto end =  temp.begin()+i+h+1;
        auto subvec = std::vector<float>(start, end);
        float median_val = median( subvec );
        start =  ds_scan.ranges.begin()+i-h;
        end =  ds_scan.ranges.begin()+i+h+1;
        filter(start, end, median_val - 0.4, median_val + 0.4, median_val);
    }

    // Verify that TF knows how to transform from the received scan to the destination scan frame
	tfListener.waitForTransform(ds_scan.header.frame_id.c_str(), fixed_frame.c_str(), ds_scan.header.stamp, ros::Duration(1));
    // transform points to destination frame ( the fixed frame )
	projector.transformLaserScanToPointCloud(ds_scan.header.frame_id, ds_scan, tmpCloud1, tfListener);
	try {
	    pcl_ros::transformPointCloud(fixed_frame.c_str(), tmpCloud1, tmpCloud2, tfListener);
	}
	catch (tf::TransformException ex) {
		ROS_ERROR("%s",ex.what());
		return;
	}

    scan_time = ds_scan.scan_time;

    // convert to pointcloud and save in buffer
    pcl_conversions::toPCL(tmpCloud2, clouds[cur_idx]);
    is_modified = true;
    cur_idx++; cnt++;
    cur_idx = cur_idx % buffer_size;

    //ROS_INFO_STREAM("scan_callback: insert n=" << ds_scan.ranges.size() << " at i=" << cur_idx);
}

void LidarAccumulator::pub_timer_callback(const ros::TimerEvent& timerevent)
{
    // wait until clouds buffer is completlly filled
    if(cnt < buffer_size)
        return;

    tf::StampedTransform transform;
    sensor_msgs::PointCloud2 tmpCloud1, tmpCloud2;
    
    // only rebuild accumulated cloud if something has changed
    if(is_modified) {
        is_modified = false;
        
        // merge current clouds into a single cloud
        acc_cloud = clouds[0];
        for(int i=1; i < buffer_size; ++i)
            pcl::concatenatePointCloud(acc_cloud, clouds[i], acc_cloud);

        pcl_conversions::fromPCL(acc_cloud, tmpCloud1);
        tmpCloud1.header.stamp = ros::Time::now();

        //tfListener.waitForTransform(fixed_frame.c_str(), robot_frame.c_str(), ros::Time::now()-ros::Duration(0.5), ros::Duration(3.0));
        tfListener.lookupTransform(robot_frame.c_str(), fixed_frame.c_str(), ros::Time(0), transform);
        try {
            pcl_ros::transformPointCloud(robot_frame.c_str(), transform, tmpCloud1, tmpCloud2 );
        }
        catch (tf::TransformException ex) {
            ROS_ERROR("%s",ex.what());
            return;
        }
        pcl_conversions::toPCL(tmpCloud2, acc_cloud);
        cloud_pub.publish(tmpCloud2);
    }

    // convert accumulated pointclouds into new lidar scan
    Eigen::MatrixXf points;
    getPointCloudAsEigen(acc_cloud, points);
    pointcloud_to_laserscan(points, &acc_cloud);
}

void LidarAccumulator::downsample_laserscan(const sensor_msgs::LaserScan::ConstPtr& src, sensor_msgs::LaserScan& dst, int step)
{
    dst.header          = src->header;
    dst.range_min       = src->range_min;
    dst.range_max       = src->range_max;
    dst.angle_min       = src->angle_min;
    dst.angle_increment = src->angle_increment * step;
    dst.time_increment  = src->time_increment;
    dst.scan_time       = src->scan_time;

    size_t size_sparse = src->ranges.size() / step;
    dst.ranges.resize(size_sparse);
    dst.angle_max = dst.angle_min + (dst.angle_increment * (size_sparse - 1));

    // take the mean of step elements
    for (unsigned int i = 0; i < src->ranges.size(); ++i) {
        dst.ranges[i / step] += src->ranges[i];
    }
    std::transform(dst.ranges.begin(), dst.ranges.end(), dst.ranges.begin(), std::bind2nd(std::multiplies<float>(), 1.0f/step));

    /* only take every step-th element
    for (unsigned int i = 0; i < size_sparse; i++) {
        dst.ranges[i] = src->ranges[i * step];
    }*/
}

void LidarAccumulator::pointcloud_to_laserscan(Eigen::MatrixXf points, pcl::PCLPointCloud2 *merged_cloud)
{
	sensor_msgs::LaserScanPtr output(new sensor_msgs::LaserScan());
	output->header = pcl_conversions::fromPCL(merged_cloud->header);
    output->header.frame_id = robot_frame.c_str();
	output->header.stamp = ros::Time(0);
	output->angle_min = angle_min;
	output->angle_max = angle_max;
	output->angle_increment = angle_increment;
	output->scan_time = scan_time;
	output->range_min = range_min;
	output->range_max = range_max;

	uint32_t ranges_size = std::ceil((output->angle_max - output->angle_min) / output->angle_increment);
	output->ranges.assign(ranges_size, output->range_max + 1.0);

    // create laser scan
	for(int i=0; i<points.cols(); i++)
	{
		const float &x = points(0,i);
		const float &y = points(1,i);
		const float &z = points(2,i);

		if ( std::isnan(x) || std::isnan(y) || std::isnan(z) )
		{
			ROS_DEBUG("rejected for nan in point(%f, %f, %f)\n", x, y, z);
			continue;
		}

		double range_sq = y*y+x*x;
		double range_min_sq_ = output->range_min * output->range_min;
		if (range_sq < range_min_sq_) {
			ROS_DEBUG("rejected for range %f below minimum value %f. Point: (%f, %f, %f)", range_sq, range_min_sq_, x, y, z);
			continue;
		}

		double angle = atan2(y, x);
		if (angle < output->angle_min || angle > output->angle_max)
		{
			ROS_DEBUG("rejected for angle %f not in range (%f, %f)\n", angle, output->angle_min, output->angle_max);
			continue;
		}
		int index = (angle - output->angle_min) / output->angle_increment;

		if (output->ranges[index] * output->ranges[index] > range_sq)
			output->ranges[index] = sqrt(range_sq);
	}
    scan_pub.publish(output); 
}

float LidarAccumulator::median(std::vector<float> &vec)
{
    const auto median_it = vec.begin() + vec.size() / 2;
    std::nth_element(vec.begin(), median_it , vec.end());
    return *median_it;
}

float LidarAccumulator::filter(std::vector<float>::iterator start, std::vector<float>::iterator end, float lower, float upper, float replace) {
    for(auto it = start; it != end; ++it) {
        if(*it < lower || *it > upper)
            *it = replace;
    }
}

float LidarAccumulator::mean(std::vector<float>::iterator start, std::vector<float>::iterator end) {
    int length = std::distance(start, end);
    return std::accumulate(start, end, 0.0f) / (double)length;
}