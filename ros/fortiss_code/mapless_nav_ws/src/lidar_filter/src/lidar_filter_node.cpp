#include "ros/ros.h"
#include "lidar_filter/lidar_filter.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "lidar_filter");
    ROS_INFO_STREAM("Starting lidar filter");

    LidarFilter lidar_filter;

    ros::Rate rate(50);
    while (ros::ok())
    {
        lidar_filter.update();
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}