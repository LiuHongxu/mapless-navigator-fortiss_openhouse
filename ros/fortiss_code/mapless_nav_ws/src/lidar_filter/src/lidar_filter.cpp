#include "lidar_filter/lidar_filter.h"
#include "lidar_filter/utilities.h"
#include <math.h>

using namespace vec2d;

LidarFilter::LidarFilter()
{
    // load ros parameters (private ns)
    ros::param::get("~num_of_robots", num_of_robots);
    ros::param::get("~robot_radius", robot_radius);
    ros::param::get("~robot_name", robot_name);
    ros::param::get("~tf_prefix", tf_prefix); 

    // setup input variables
    fixed_frame = "/map";

    // setup publisher and subscriber for each robot
    scan_subs.resize(num_of_robots);
    filterd_scan_pubs.resize(num_of_robots);
    robot_frames.resize(num_of_robots);
    robot_poses.resize(num_of_robots);          
    is_alive.resize(num_of_robots);

    for(size_t i = 0; i < num_of_robots; ++i) {
        scan_subs[i] = nh.subscribe<sensor_msgs::LaserScan>(robot_name + std::to_string(i) + "/merged_scan", 10, boost::bind(&LidarFilter::scan_callback, this, _1, i) );
        filterd_scan_pubs[i] = nh.advertise<sensor_msgs::LaserScan>(robot_name + std::to_string(i) + "/lidarscan_filtered", 10);
        robot_frames[i] = tf_prefix + std::to_string(i) + "/base_link";
        is_alive[i] = false;
        ROS_INFO_STREAM("Looking for scan: " << robot_name << std::to_string(i) << "/lidarscan" << " and tf:" << robot_frames[i]);
    }

    // setup field dimensions and filed boarder lines
    //field_pts = { {3.33, 1.59}, {-0.87, 1.67}, {-1.21, -1.67}, {3.38, -1.59} };
    field_pts = { {3.63, -3.12}, {3.76, 1.8}, {-1.58, 2.0}, {-1.58, -2.01} };
    field_lines = { field_pts[1]-field_pts[0], field_pts[2]-field_pts[1], field_pts[3]-field_pts[2], field_pts[0]-field_pts[3] };

    // print parameters
    ROS_INFO_STREAM("lidar_filter: num_of_robots=" << num_of_robots);
    ROS_INFO_STREAM("lidar_filter: robot_radius=" << robot_radius);
    ROS_INFO_STREAM("lidar_filter: robot_name=" << robot_name);
    ROS_INFO_STREAM("lidar_filter: tf_prefix=" << tf_prefix);
}

LidarFilter::~LidarFilter()
{
}

/* call this function in the main loop */
void LidarFilter::update()
{
    // get pose of all robots wrt fixed_frame
    for(size_t i = 0; i < num_of_robots; ++i) {
        is_alive[i] = update_pose(fixed_frame, robot_frames[i], robot_poses[i]);
    }
}

/* get robot pose */
bool LidarFilter::update_pose(const std::string& fixed_frame, const std::string& robot_frame, Pose2d& pose)
{
    tf::StampedTransform transform;
    bool available = false;

    // 1. get transformation from tf tree
    available = listener.waitForTransform(fixed_frame, robot_frame, ros::Time(0), ros::Duration(0.1));
    try {
      listener.lookupTransform(fixed_frame, robot_frame, ros::Time(0), transform);      
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s", ex.what());
      return false;
    }
    if(!available)
        return false;

    // 2. extract robot pose from transformation
    double roll, pitch, yaw;
    transform.getBasis().getRPY(roll, pitch, yaw);

    pose.p.x = transform.getOrigin().x();
    pose.p.y = transform.getOrigin().y();
    pose.theta = yaw;

    return true;
}

/* scan for all laser range finder */
void LidarFilter::scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan, size_t id)
{
    // transformation availabel
    if(!is_alive[id])
        return;

    // 1. create header of new filtered scan
    filtered_scan.header.frame_id = scan->header.frame_id;
    filtered_scan.header.stamp = scan->header.stamp;
    filtered_scan.angle_min = scan->angle_min;
    filtered_scan.angle_max = scan->angle_max;
    filtered_scan.angle_increment = scan->angle_increment;
    filtered_scan.scan_time = scan->scan_time;
    filtered_scan.range_min = scan->range_min;
    filtered_scan.range_max = scan->range_max;
    filtered_scan.ranges = scan->ranges;
    filtered_scan.intensities = scan->intensities;

    // get reference to filterd and unfilterd ranges
    sensor_msgs::LaserScan::_ranges_type& ranges = filtered_scan.ranges;

    // 2. get robot position and orientation
    Vec2d r = robot_poses[id].p;
    float r_angle = robot_poses[id].theta; // add pi since robot lidar is facing backward

    // 3. compute angle sectors used for each outer field line
    std::vector<float> field_angle_interval;
    Vec2d d = field_pts[0] - r;
    float angle = -rotation_angle(r_angle, atan2f(d.y, d.x));
    field_angle_interval.push_back(angle);
    size_t flip_idx = 0;
    for(size_t i = 1; i < 4; ++i) {
        d = field_pts[i] - r;
        angle = -rotation_angle(r_angle, atan2f(d.y, d.x));
        field_angle_interval.push_back(angle);
        if(field_angle_interval[i-1] * angle < 0 && field_angle_interval[i-1] > angle)
            flip_idx = i-1;
    }

    //ROS_INFO_STREAM("x:" << d.x << " y: " << d.y);
    /*ROS_INFO_STREAM(" r  :" << r_angle <<
                    " [0]:" << field_angle_interval[0] << 
                    " [1]:" << field_angle_interval[1] <<
                    " [2]:" << field_angle_interval[2] <<
                    " [3]:" << field_angle_interval[3] <<
                    " flip:" << flip_idx );*/

    // 4. compute index of lidar rays that might hit other robots
    std::vector<size_t> obstacle_idx_interval;
    std::vector<Vec2d> obstacle;
    for(size_t i = 0; i < num_of_robots; ++i) {
        if(i == id || !is_alive[i])
            continue; // no intersection with itself or a dead robot

        d = robot_poses[i].p - r;
        float dist = distance(d);
        float delta_angle = tanf(robot_radius / dist );
        float angle1 = -rotation_angle(r_angle, atan2f(d.y, d.x) - delta_angle);
        float angle2 = -rotation_angle(r_angle, atan2f(d.y, d.x) + delta_angle);

        obstacle.push_back( robot_poses[i].p );
        obstacle_idx_interval.push_back( (angle1 - filtered_scan.angle_min) / filtered_scan.angle_increment );
        obstacle_idx_interval.push_back( (angle2 - filtered_scan.angle_min) / filtered_scan.angle_increment );
    }

    // 5. compute hitpoint between lidar ray and outer field lines
    for(size_t i = 0; i <ranges.size(); ++i) {
        // handle exceptions
        if(std::isnan( scan->ranges[i] ))
            continue;
        else if(std::isinf( scan->ranges[i]))
            ranges[i] = 10;

        float angle = filtered_scan.angle_min + i * filtered_scan.angle_increment;

        Vec2d d = ranges[i] * Vec2d( cosf( angle + r_angle ), sinf( angle + r_angle ));     // point wrt base_frame

        // check intersection with lines
        if(angle >= field_angle_interval[0] && angle < field_angle_interval[1]){
            float det = d.x * field_lines[0].y - field_lines[0].x * d.y;
            float lamda = ( field_lines[0].y * (field_pts[1].x - r.x) - field_lines[0].x * (field_pts[1].y - r.y) ) / det;
            if(lamda >= 0 && lamda <= 1)
                ranges[i] = lamda * ranges[i];
        }
        else if(angle >= field_angle_interval[1] && angle < field_angle_interval[2]) {
            float det = d.x * field_lines[1].y - field_lines[1].x * d.y;
            float lamda = ( field_lines[1].y * (field_pts[2].x - r.x) - field_lines[1].x * (field_pts[2].y - r.y) ) / det;
            if(lamda >= 0 && lamda <= 1)
                ranges[i] = lamda * ranges[i];
        }
        else if(angle >= field_angle_interval[2] && angle < field_angle_interval[3]) {
            float det = d.x * field_lines[2].y - field_lines[2].x * d.y;
            float lamda = ( field_lines[2].y * (field_pts[3].x - r.x) - field_lines[2].x * (field_pts[3].y - r.y) ) / det;
            if(lamda >= 0 && lamda <= 1)
                ranges[i] = lamda * ranges[i];
        }
        else if(angle >= field_angle_interval[3] && angle < field_angle_interval[0]) {
            float det = d.x * field_lines[3].y - field_lines[3].x * d.y;
            float lamda = ( field_lines[3].y * (field_pts[0].x - r.x) - field_lines[3].x * (field_pts[0].y - r.y) ) / det;
            if(lamda >= 0 && lamda <= 1)
                ranges[i] = lamda * ranges[i];
        }
        else {
            float det = d.x * field_lines[flip_idx].y - field_lines[flip_idx].x * d.y;
            float lamda = ( field_lines[flip_idx].y * (field_pts[flip_idx].x - r.x) - field_lines[flip_idx].x * (field_pts[flip_idx].y - r.y) ) / det;
            if(lamda >= 0 && lamda <= 1)
                ranges[i] = lamda * ranges[i];
        }
    }

    /* 6. check intersection with obstacles
    for(size_t j = 0; j < obstacle_idx_interval.size(); j+=2) {  // iterate over all obstace intervalls
        Vec2d ro = r - obstacle[j / 2]; // robot->obst,
        float c = ro.x * ro.x + ro.y * ro.y - robot_radius * robot_radius;

        int detla_idx = abs(obstacle_idx_interval[j] - obstacle_idx_interval[j+1]);
        int min_idx = obstacle_idx_interval[j];
        int max_idx = min_idx + detla_idx;

        for(size_t i = min_idx; i <= max_idx; ++i) {    // iterate over all rays that might intersect
            int idx = i % 360;

            float angle = filtered_scan.angle_min + idx * filtered_scan.angle_increment; // wrt robot
            Vec2d d = ranges[idx] * Vec2d( cosf( angle + r_angle ), sinf( angle + r_angle )); // point wrt world

            float a = d.x * d.x + d.y * d.y;       
            float b = 2 * (ro.x * d.x + ro.y * d.y);

            float discriminant = b*b-4*a*c;
            if(discriminant >= 0) {
                float lamda = (-b - sqrtf(discriminant)) / (2 * a);
                if(lamda >= 0 && lamda <= 1)
                    ranges[idx] = lamda * ranges[idx];
            }
        }
    }*/

    // 7. publish filtered data
    filterd_scan_pubs[id].publish(filtered_scan);
}