#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/Marker.h> 
#include "lidar_filter/utilities.h"

class LidarFilter
{
public:
    LidarFilter();
    virtual ~LidarFilter();

    /* call this function in the main loop */
    void update();

private:
    ros::NodeHandle nh;
    tf::TransformListener listener;

    int num_of_robots;
    std::string robot_name;
    std::string tf_prefix;
    float robot_radius;                         // radius of robot

    std::vector<vec2d::Vec2d> field_pts;        // corner field wrt fixed_frame
    std::vector<vec2d::Vec2d> field_lines;      // outer lines of field wrt fixed_frame

    std::vector<ros::Subscriber> scan_subs;             // input unfilterd scans
    std::vector<ros::Publisher> filterd_scan_pubs;      // ouput filterd scans
    std::vector<bool> is_alive;                         // inidcate if robot is still online
    std::vector<vec2d::Pose2d> robot_poses;     // pose of each robot wrt fixed_frame

    std::vector<std::string> robot_frames;              // robot reference frames
    std::string fixed_frame;                            // fixed reference frame

    sensor_msgs::LaserScan filtered_scan;               // holds msg

    /* update robot pose */
    bool update_pose(const std::string& fixed_frame, const std::string& robot_frame, vec2d::Pose2d& pose);

    /* scan for all laser range finder */
    void scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan, size_t id);
};