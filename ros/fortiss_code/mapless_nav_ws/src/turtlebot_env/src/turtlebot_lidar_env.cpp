#include <tf/tf.h>
#include <std_srvs/Empty.h> 
#include <std_msgs/Empty.h> 
#include <std_msgs/Bool.h>
#include <kobuki_msgs/Led.h>

#include <cmath>
#include <stdio.h>

#include "turtlebot_env/turtlebot_lidar_env.h"

const float ALPHA = 0.4;                      // exp() moving average for LIDAR
const float RAD2DEG = 57.2957795131;
const int REVERSE = -1;

TurtlebotLidarEnv::TurtlebotLidarEnv()
    : AgentEnvironment(3, 20), is_scan_updated(false), is_pose_updated(false), is_goal_set(false),
      prev_action(0), distance(-1.0f), move_forward_flag(false), prev_distance(-1.0f),
      action_history(10), do_recovery(false)
{
    // load ros parameters (private ns)
    ros::param::get("~robot_name", robot_name);
    ros::param::get("~tf_prefix", tf_prefix);
    ros::param::get("~velo_lin_forward", velo_lin_forward);
    ros::param::get("~velo_lin_turn", velo_lin_turn); 
    ros::param::get("~velo_ang_turn", velo_ang_turn); 
    ros::param::get("~velo_smoothing_inc", velo_smoothing_inc);

    // set parameters (relative ns)
    nh.setParam("state_size", AgentEnvironment::get_dim_states());
    nh.setParam("action_size", AgentEnvironment::get_n_actions());

    // ros publisher
    vel_pub = nh.advertise<geometry_msgs::Twist>(robot_name + "/cmd_vel_mux/input/teleop", 10);
    lidar_rays_marker_pub = nh.advertise<visualization_msgs::Marker>(robot_name + "/turtlebot_env/lidar_rays_marker", 10);
    goal_marker_pub = nh.advertise<visualization_msgs::Marker>(robot_name + "/turtlebot_env/goal_marker", 10);
    env_state_pub = nh.advertise<turtlebot_env::env_state>(robot_name + "/turtlebot_env/env_state", 10);
    start_pub = nh.advertise<std_msgs::Empty>(robot_name + "/turtlebot_env/start", 10);
    done_pub = nh.advertise<std_msgs::Bool>(robot_name + "/turtlebot_env/done", 10);
    led_pub = nh.advertise<kobuki_msgs::Led>(robot_name + "/mobile_base/commands/led1", 10);
    sound_pub = nh.advertise<kobuki_msgs::Sound>(robot_name + "/mobile_base/commands/sound", 10);

    // ros subscriber (i dont  know why lidarscan only works without the '/')
    scan_sub = nh.subscribe(robot_name + "/merged_scan", 10, &TurtlebotLidarEnv::scan_callback, this); // "/lidarscan_filtered"
    goal_sub = nh.subscribe(robot_name + "/move_base_simple/goal", 10, &TurtlebotLidarEnv::goal_callback, this);
    bumper_sub = nh.subscribe(robot_name + "/mobile_base/events/bumper", 10, &TurtlebotLidarEnv::bumper_callback, this);

    // ros services
    execute_srv = nh.advertiseService(robot_name + "/turtlebot_env/execute", &TurtlebotLidarEnv::run_srv_execute, this);
    reset_srv = nh.advertiseService(robot_name + "/turtlebot_env/reset", &TurtlebotLidarEnv::run_srv_reset, this);
    recovery_srv = nh.advertiseService(robot_name + "/turtlebot_env/recover", &TurtlebotLidarEnv::run_srv_recovery, this);

    // tf transformation frames used for position calculations
    fixed_frame = "/map";
    robot_frame = tf_prefix + "/base_link";

    // setup hyper parameters
    min_goal_dist = 0.25;               // distance around goal position
    min_obstical_dist = 0.22;           // distance around obstical
    robot_radius = 0.18;                // radius of the robot
    
    // lidar parameters
    ray_num = 18;                       // number of laser range finder rays
    visual_angle = 3 * M_PI / 4.0f;     // visual angle to pick laser rays
    max_range = 2.0f;                   // maximum view distance

    // lidar ray visualization
    lidar_rays_marker = create_visualization_marker(robot_frame, visualization_msgs::Marker::LINE_LIST, 0, 1.0, 0.0, 0.0, 0.01);
    lidar_rays_marker.points.resize(2*ray_num);
    filtered_ranges.resize(ray_num, 0);
    // goal visualizer
    goal_marker = create_visualization_marker(fixed_frame, visualization_msgs::Marker::SPHERE, 1, 0.0, 1.0, 0.0, 0.1);
   
    // reset
    goal.x = 0;
    goal.y = 0;
    goal.theta = 0;
    hit_obstacle = false; reached_goal = false; hit_bumper = false;

    hw_cmd_vel.linear.x = des_cmd_vel.linear.x = 0;
    hw_cmd_vel.linear.y = des_cmd_vel.linear.y = 0;
    hw_cmd_vel.angular.z = des_cmd_vel.angular.z = 0;

    // print out info
    ROS_INFO_STREAM("Got robot name:" << robot_name);
    ROS_INFO_STREAM("Using fixed frame:" << fixed_frame);
    ROS_INFO_STREAM("Using robot frame:" << robot_frame);
    ROS_INFO_STREAM("velo_lin_forward:" << velo_lin_forward);
    ROS_INFO_STREAM("velo_lin_turn:" << velo_lin_turn);
    ROS_INFO_STREAM("velo_ang_turn:" << velo_ang_turn);

    // environment setup
    setup();
}

TurtlebotLidarEnv::~TurtlebotLidarEnv() 
{
}

/* precompute the inidces of the laserrange scan array */
void TurtlebotLidarEnv::setup() {
    float delta_phi = M_PI / 180.0f;
    int idx = visual_angle / delta_phi;

    float delta_phi_discret = 2 * visual_angle / (ray_num - 1);
    int inc = delta_phi_discret / delta_phi;

    raw_ranges.resize(360, 0.0f);
    ranges.resize(ray_num, 0.0f);

    for(int i = -idx + inc/2.0f; i < idx; i += inc) {
        indicies.push_back(180 + i);
        ROS_INFO_STREAM("[" << i << "]= " << indicies.back());
    }
}

void TurtlebotLidarEnv::update()
{
    // 1. check if new observations are available
    bool observation_done = observe(env_state);

    // 2. recovery service was called execute action in reversed order
    if(do_recovery && observation_done)
        recovery();

    // 3. update hardware smoothing (much faster than observation freq)
    action_smoothing();
}

/* set desired action command */
void TurtlebotLidarEnv::execute(action_type action, int reverse)
{
    if(!is_goal_set)
        return;  // not available, do nothing

    // 0. check if action must be reversed
    float velo_lin_forward_ = reverse * velo_lin_forward;
    float velo_lin_turn_ = reverse * velo_lin_turn;
    float velo_ang_turn_ = reverse * velo_ang_turn;

    // 1. remember last action
    if(reverse > 0) {
        prev_action = action;
        action_history.add(action);
    }
    
    // 2. assemble action (set desired /des_cmd_vel)
    switch(action) {
    case 0:
        des_cmd_vel.linear.x = velo_lin_forward_;
        des_cmd_vel.angular.z = 0.0;
        break;
    case 1:
        des_cmd_vel.linear.x = velo_lin_turn_;
        des_cmd_vel.angular.z = velo_ang_turn_;
        break;
    case 2:
        des_cmd_vel.linear.x = velo_lin_turn_;
        des_cmd_vel.angular.z = -velo_ang_turn_;
        break;
    case -1:
        des_cmd_vel.linear.x = 0.0f;
        des_cmd_vel.angular.z = 0.0f;
    }

    //ROS_INFO_STREAM("execute:" << action);
    //ROS_INFO_STREAM("velo_lin_forward:" << velo_lin_forward);
    //ROS_INFO_STREAM("velo_lin_turn:" << velo_lin_turn);
}

/* match desired action to hardware action (smoothing) */
void TurtlebotLidarEnv::action_smoothing()
{
    if(!is_goal_set)
        return;  // not available

    // set angular velocity directly
    hw_cmd_vel.angular.z = des_cmd_vel.angular.z;

    /* assert smooth start and stop of robot*/
    double diff = des_cmd_vel.linear.x - hw_cmd_vel.linear.x;

    if(diff > 0) {      // we need to accelerate
        if(diff > velo_smoothing_inc)
            hw_cmd_vel.linear.x += velo_smoothing_inc;
        else
            hw_cmd_vel.linear.x = des_cmd_vel.linear.x;
    } else if (diff < 0) {
        if(diff < -velo_smoothing_inc)
            hw_cmd_vel.linear.x -= velo_smoothing_inc;
        else
            hw_cmd_vel.linear.x = des_cmd_vel.linear.x;
    }
    // 3. publish "/cmd_vel"
    vel_pub.publish(hw_cmd_vel);
}

/* do observation */
bool TurtlebotLidarEnv::observe(TypedEnvState<float>& env_state)
{   
    // 1. check if new observation available
    if(!is_scan_updated)
        return false;

    // 2. get position relative to goal
    reached_goal = update_pose();
    is_scan_updated = false;
    is_pose_updated = false;

    // 3. reduce laser ranges to ray_num
    hit_obstacle = reduce_ranges(raw_ranges, ranges, max_range);

    // 4. normalize ranges to [0, 1]
    normalize_ranges(ranges, normalized_ranges, min_obstical_dist);

    // init
    if(prev_distance < 0.0)
        prev_distance = distance;

    // 5. compute reward
    float reward;
    if(hit_obstacle)
        reward = -150.0f;
    else if(reached_goal)
        reward = 100.0f;
    else {
        reward = std::min(1.5f, std::max(-1.5f, 15.0f * (prev_distance - distance) ) );
    }

    // 6. combine into a single state vector ( scale state to range [0, 1] )
    env_state.state = normalized_ranges;
    env_state.state.push_back( 1.0f / distance * min_goal_dist );
    env_state.state.push_back( 0.5f * (theta / M_PI + 1.0f) ); // CHANGED: 0.5f *
    //env_state.state.push_back( 0.5f * (cos_theta + 1.0f) );
    //env_state.state.push_back( 0.5f * (cos_theta + 1.0f) );

    env_state.reward = reward;
    env_state.success = reached_goal;
    env_state.done = reached_goal | hit_bumper | hit_obstacle;

    prev_distance = distance;

    //ROS_INFO_STREAM("d:" << distance);
    //ROS_INFO_STREAM("th:" << theta * 180.0f / M_PI);

    // publish ros env_state message
    turtlebot_env::env_state env_state_msg;
    env_state_msg.state = env_state.state;
    env_state_msg.reward = env_state.reward;
    env_state_msg.success = (int8_t)env_state.success;
    env_state_msg.done = (int8_t)env_state.done;
    env_state_pub.publish(env_state_msg);

    return true;
}

void TurtlebotLidarEnv::reset(bool success)
{
    // send the done flag
    std_msgs::Bool bool_msg; bool_msg.data = success;
    done_pub.publish(bool_msg);
    kobuki_msgs::Sound sound_msg;
    sound_msg.value = (success == true) ? 6 : 1;
    sound_pub.publish(sound_msg);

    // generate an inital observation ( wait for  topic as inital observation )
    const sensor_msgs::LaserScan::ConstPtr& scan = ros::topic::waitForMessage<sensor_msgs::LaserScan>(robot_name + "/lidarscan", nh, ros::Duration(1.0));
    raw_ranges = scan->ranges;
    update_pose();

    // reset
    is_scan_updated = true;
    is_pose_updated = true;
    is_goal_set = false;
    hit_bumper = false;
}

void TurtlebotLidarEnv::recovery()
{
    // as long as history contains action execture them in reverse
    if( action_history.size() > 0 ) {
        execute( action_history.front(), REVERSE );
        action_history.pop_front();
    }
    else {
        do_recovery = false;
        reset(false);
    }
}

bool TurtlebotLidarEnv::reduce_ranges(const std::vector<float>& raw_ranges, std::vector<float>& ranges, float max_range)
{
    bool done = false;
    int inf_cnt;
    float min_ray;

    // points for ray visualization
    geometry_msgs::Point p0, p1;
    p0.x = p0.y = 0;
    p0.z = p1.z = 0.24;

    for(int k = 0; k < indicies.size(); ++k) {
        // mean range
        float r = 0.0f;
        min_ray = INFINITY;
        inf_cnt = 0;

        // search for minimum laser distance in a range of +- 2° to indicies[k]
        for(int j = -4; j <= 4; ++j ) {              
            int idx = (360 + indicies[k] + j - 4) % 360;
            // don't consider nan and inf laser rays
            if(!std::isinf(raw_ranges[idx] && !std::isnan(raw_ranges[idx])))
                if(raw_ranges[idx] < min_ray)
                    min_ray = raw_ranges[idx];
        }
        ranges[k] = std::min(min_ray, max_range);

        // apply exponential average 
        r = filtered_ranges[k] = ALPHA * filtered_ranges[k] + (1 - ALPHA) * ranges[k] ;    
        ranges[k] = filtered_ranges[k];

        // check if obsticle was hit
        if( ranges[k] > robot_radius && ranges[k] < min_obstical_dist - 0.04) {
            ROS_INFO_STREAM("Lidar hit!!!");
            done = true;
        }

        // lidar ray visualization
        float ray_angle = (-visual_angle + 2 * k * visual_angle / (ray_num-1.0));
        p1.x = cos(ray_angle) * r;
        p1.y = sin(ray_angle) * r;
        lidar_rays_marker.points[2*k] = p0;                // start point of ray
        lidar_rays_marker.points[2*k+1] = p1;              // end point of ray
    }

    // publish rays 
    lidar_rays_marker_pub.publish(lidar_rays_marker);
    return done;
}

bool TurtlebotLidarEnv::update_pose()
{
    bool done = false;
    tf::StampedTransform transform;

    // 1. extract position and orientation of robot_frame wrt fixed_frame
    listener.waitForTransform(fixed_frame, robot_frame, ros::Time(0), ros::Duration(0.1));
    try{
      listener.lookupTransform(fixed_frame, robot_frame, ros::Time(0), transform);      
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s", ex.what());
    }
    is_pose_updated = true;

    double roll, pitch, yaw;
    transform.getBasis().getRPY(roll, pitch, yaw);

    // 2. compute distance to goal
    geometry_msgs::Pose2D pos;
    pos.x = transform.getOrigin().x();
    pos.y = transform.getOrigin().y();
    distance = geom2d::distance_point(pos, goal);

    // 3. compute angle to goal
    theta = rotation_angle(atan2(goal.y - pos.y, goal.x - pos.x), yaw );
    rotation_angle_cos_sin(atan2(goal.y - pos.y, goal.x - pos.x), yaw, cos_theta, sin_theta);

    // 4. goal reached
    if( distance <  min_goal_dist)
        done = true;

    return done;
}

void TurtlebotLidarEnv::normalize_ranges(const std::vector<float>& ranges, std::vector<float>& normalized_ranges, float min_obstical_dist)
{
    normalized_ranges.resize(ranges.size());
    for( int i = 0; i < ranges.size(); ++i) {
        normalized_ranges[i] = 1.0f / ranges[i] * min_obstical_dist;
    }
}

float TurtlebotLidarEnv::rotation_angle(float src, float dst)
{
    float val = (src > dst) ? -M_PI + std::fmod(src - dst + M_PI, M_PI * 2) 
                             :  M_PI - std::fmod(dst - src + M_PI, M_PI * 2);
    return  val;
}

void TurtlebotLidarEnv::rotation_angle_cos_sin(float src, float dst, float& angle_cos, float& angle_sin)
{
    angle_cos = cosf(src - dst);
    angle_sin = sinf(src - dst);
}

// ---------
// Callbacks
// ---------
void TurtlebotLidarEnv::scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    raw_ranges = scan->ranges;
    is_scan_updated = true;
}

void TurtlebotLidarEnv::goal_callback(const geometry_msgs::PoseStamped::ConstPtr& posestamped)
{
    // set goal flag
    goal.x = posestamped->pose.position.x;
    goal.y = posestamped->pose.position.y;
    ROS_INFO_STREAM("New Goal set to: x=" <<  goal.x << " y=" << goal.y);
    is_goal_set = true;
    is_scan_updated = false;
    hit_bumper = false;

    // publish start msg
    std_msgs::Empty empty;
    start_pub.publish(empty);

    // publish goal marker
    goal_marker.pose = posestamped->pose;
    goal_marker_pub.publish(goal_marker);
}

void TurtlebotLidarEnv::bumper_callback(const kobuki_msgs::BumperEventConstPtr& msg)
{
    if (msg->state == kobuki_msgs::BumperEvent::PRESSED) {
        hit_bumper = true;
        ROS_INFO_STREAM("Hit bumper: true");
    }
}

// ---------
// Services
// ---------
bool TurtlebotLidarEnv::run_srv_execute(turtlebot_env::execute::Request& req, turtlebot_env::execute::Response& res)
{
    execute( (action_type)req.action );
    return true;
}
bool TurtlebotLidarEnv::run_srv_reset(turtlebot_env::reset::Request& req, turtlebot_env::reset::Response& res)
{
    reset(true);
    return true;
}

bool TurtlebotLidarEnv::run_srv_recovery(turtlebot_env::recovery::Request& req, turtlebot_env::recovery::Response& res)
{
    do_recovery = true;
    return true;
}

/* create a visualization marker */
visualization_msgs::Marker TurtlebotLidarEnv::create_visualization_marker(std::string frame, int type, int id, double r, double g, double b, double scale)
{
    visualization_msgs::Marker marker;

    marker.header.frame_id = frame;
    marker.header.stamp = ros::Time();
    marker.ns = "turtlebot_env_node";
    marker.id = id;
    marker.type = type;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = scale;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0;
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;

    return marker;
}