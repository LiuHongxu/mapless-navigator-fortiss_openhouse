#include "turtlebot_env/turtlebot_lidar_env.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_env_node");

  ROS_INFO_STREAM("Starting turtlebot lidar environment... \n");
  TurtlebotLidarEnv env;
  ros::Rate loop_rate(50);

  while(ros::ok())
  {
    // update action smoothing
    env.update();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}