#ifndef TURTLEBOTLIDARENV_H_
#define TURTLEBOTLIDARENV_H_

#include <string>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/LaserScan.h>
#include <kobuki_msgs/BumperEvent.h>
#include <kobuki_msgs/Sound.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/Marker.h> 

#include "turtlebot_env/execute.h"
#include "turtlebot_env/reset.h"
#include "turtlebot_env/recovery.h"
#include "turtlebot_env/env_state.h"

#include "turtlebot_env/utilities.h"
#include "turtlebot_env/agent_environment.h"

/*
 * Base classe for Turtlebot environment
 */
class TurtlebotLidarEnv : public AgentEnvironment
{
public:
    TurtlebotLidarEnv();
    virtual ~TurtlebotLidarEnv();

    /* call this function in the main loop */
    void update();

    /* perform one step (action) -> learner */
    void execute(action_type action, int reverse = 1);

    /* returns true if new observations was made*/
    bool observe(TypedEnvState<float>& env_state);

    /* hw velocity control of turtlebot */
    void action_smoothing();

    /* reset environment and return to inital observation */
    void reset(bool success = false);

    /* replay the last n actions to get robot back into useable position */
    void recovery();

    /* update current pose form tf */
    bool update_pose();

protected:
    bool hit_obstacle, hit_bumper, reached_goal;    // controll flags
    float distance;                                 // robot world position
    float theta, cos_theta, sin_theta;                                    // robot world angle
    std::vector<float> ranges;                      // reduced laser range data
    bool is_goal_set;                               // true if goal recived
    
    float min_goal_dist;                    // min distance to goal     (stop simulation)
    float min_obstical_dist;                // min distance to obsticle (stop simulation)
    float robot_radius;                     // radius of the robot

    action_type prev_action;                

private:
    ros::NodeHandle nh;

    // Publisher
    ros::Publisher vel_pub;                 // robot veclocity
    ros::Publisher lidar_rays_marker_pub;   // lidar rviz markers
    ros::Publisher goal_marker_pub;         // goal rviz markers
    ros::Publisher env_state_pub;           // environemnt state
    ros::Publisher start_pub;               // published if new was goal set
    ros::Publisher done_pub;                // published if goal reached or crash
    ros::Publisher led_pub;                 // change led mode
    ros::Publisher sound_pub;               // make a sound       

    // Subscriber
    ros::Subscriber scan_sub;
    ros::Subscriber goal_sub;
    ros::Subscriber bumper_sub;

    // Services
    ros::ServiceServer execute_srv;
    ros::ServiceServer reset_srv;
    ros::ServiceServer recovery_srv;

    // TFs
    tf::TransformListener listener;
    std::string fixed_frame;                // fixed reference frame
    std::string robot_frame;                // frame attached to roboto

    tf::TransformBroadcaster br;
    tf::Transform transform;
    tf::Quaternion qtf;

    // lidar ray visualization
    visualization_msgs::Marker lidar_rays_marker;  // ray visualization
    visualization_msgs::Marker goal_marker;

    // roslaunch file parameters
    std::string robot_name;
    std::string tf_prefix;
    float velo_lin_forward;
    float velo_lin_turn;
    float velo_ang_turn;
    float velo_smoothing_inc;

    // flags
    bool is_scan_updated;                   // true if laser scan updated
    bool is_pose_updated;                   // true if pose updated
    bool is_env_state_available;            // true if new env_state is available
    bool do_recovery;                       // true if recovery behaviour active

    // cmd smoother
    geometry_msgs::Twist des_cmd_vel;       // desired action
    geometry_msgs::Twist hw_cmd_vel;        // cmd_vel on hardware
    bool move_forward_flag;                 // flag for driving forward

    geometry_msgs::Pose2D goal;             // robot goal
    
    // lidar related
    int ray_num;                            // number of rays for state
    float visual_angle;
    float max_range;
    std::vector<int> indicies; 

    std::vector<float> raw_ranges;          // raw laser range data  
    std::vector<float> filtered_ranges;     // lowpass filtered ranges
    std::vector<float> normalized_ranges;   // normalized ranges

    TypedEnvState<float> env_state;         // combines everything into a environment state struct

    // history buffer
    float prev_distance;
    Queue<int> action_history;                   // saves last n actions ( for recovery )

    /* setup variables */
    void setup();

    /* lidar callbacks */
    void scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan);
    /* new gaol set */
    void goal_callback(const geometry_msgs::PoseStamped::ConstPtr& pose);
    /* bumber was pressed */
    void bumper_callback(const kobuki_msgs::BumperEventConstPtr& msg);
    
    /* service functions */
    bool run_srv_execute(turtlebot_env::execute::Request& req, turtlebot_env::execute::Response& res);
    bool run_srv_reset(turtlebot_env::reset::Request& req, turtlebot_env::reset::Response& res);
    bool run_srv_recovery(turtlebot_env::recovery::Request& req, turtlebot_env::recovery::Response& res);

    /* reduce laserrange finder dimensions */
    bool reduce_ranges(const std::vector<float>& raw_ranges, std::vector<float>& ranges, float max_range);
    /* scale ranges to [0, 1] */
    void normalize_ranges(const std::vector<float>& ranges, std::vector<float>& normalized_ranges, float min_obstical_dist);
    /* helper fnc to compute angel diff */
    static float rotation_angle(float src, float dst);

    static void rotation_angle_cos_sin(float src, float dst, float& angle_cos, float& angle_sin);

    /* create ros visualization marker */
    visualization_msgs::Marker create_visualization_marker(std::string frame, int type, int id, double r, double g, double b, double scale);
};

#endif