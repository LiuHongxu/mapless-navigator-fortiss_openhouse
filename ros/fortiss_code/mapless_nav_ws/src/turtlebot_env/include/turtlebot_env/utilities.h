#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <vector>
#include <deque>
#include <numeric>
#include <algorithm>
#include <math.h>
#include <geometry_msgs/Pose2D.h>

/* gemetric relations */
namespace geom2d 
{
	using namespace geometry_msgs;

	/* squared Distance point to point */
	inline double distance_point_sq(const Pose2D& p1, const Pose2D& p2) {
		return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
	}

	/* Distance point to point */
	inline double distance_point(const Pose2D& p1, const Pose2D& p2) {
		return sqrt( distance_point_sq(p1, p2) );
	}
}

/* Implements a queue */
template<typename T>
class Queue : public std::deque<T>
{
public:
    Queue(size_t max_size) : max_size(max_size)
    {}

    /* filo buffer behaviour 
	 * add element in the beginning remove, last element
	 * most recent [0] --- oldest [size-1]
	 */
    void add(const T& val) {
        this->push_front(val);
		if(this->size() >= max_size)
        	this->pop_back();
    }
private:
	size_t max_size;
};

#endif
