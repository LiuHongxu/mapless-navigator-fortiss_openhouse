#include <ros/ros.h>
#include <string.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <laser_geometry/laser_geometry.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h> 
#include "sensor_msgs/LaserScan.h"
#include "pcl_ros/point_cloud.h"
#include <Eigen/Dense>
#include <ira_laser_tools/laserscan_multi_mergerConfig.h>

using namespace std;
using namespace pcl;
using namespace laserscan_multi_merger;

class LaserscanMerger
{
public:
    LaserscanMerger();
    void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan, std::string topic);
    void pointcloud_to_laserscan(vector<pcl::PCLPointCloud2>& clouds);

private:
    ros::NodeHandle node_;
    laser_geometry::LaserProjection projector_;
    tf::TransformListener tfListener_;

    ros::Publisher point_cloud_publisher_;
    ros::Publisher laser_scan_publisher_;
    vector<ros::Subscriber> scan_subscribers;
    vector<bool> clouds_modified;

    vector<pcl::PCLPointCloud2> clouds;
    vector<string> input_topics;

    void laserscan_topic_parser();

    float angle_min;
    float angle_max;
    float angle_increment;
    float scan_time;
    float range_min;
    float range_max;

    string destination_frame;
    string cloud_destination_topic;
    string scan_destination_topic;
    string laserscan_topics;

	sensor_msgs::LaserScan merged_scan;
};

void LaserscanMerger::laserscan_topic_parser()
{
	// LaserScan topics to subscribe
	ros::master::V_TopicInfo topics;
	ros::master::getTopics(topics);

    istringstream iss(laserscan_topics);
	vector<string> tokens;
	copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter<vector<string> >(tokens));

	vector<string> tmp_input_topics;

	for(int i=0;i<tokens.size();++i)
	{
	    for(int j=0;j<topics.size();++j)
		{
			if( (tokens[i].compare(topics[j].name) == 0) && (topics[j].datatype.compare("sensor_msgs/LaserScan") == 0) )
			{
				tmp_input_topics.push_back(topics[j].name);
			}
		}
	}

	sort(tmp_input_topics.begin(),tmp_input_topics.end());
	std::vector<string>::iterator last = std::unique(tmp_input_topics.begin(), tmp_input_topics.end());
	tmp_input_topics.erase(last, tmp_input_topics.end());


	// Do not re-subscribe if the topics are the same
	if( (tmp_input_topics.size() != input_topics.size()) || !equal(tmp_input_topics.begin(),tmp_input_topics.end(),input_topics.begin()))
	{

		// Unsubscribe from previous topics
		for(int i=0; i<scan_subscribers.size(); ++i)
			scan_subscribers[i].shutdown();

		input_topics = tmp_input_topics;
		if(input_topics.size() > 0)
		{
            scan_subscribers.resize(input_topics.size());
			clouds_modified.resize(input_topics.size());
			clouds.resize(input_topics.size());
            ROS_INFO("Subscribing to topics\t%ld", scan_subscribers.size());
			for(int i=0; i<input_topics.size(); ++i)
			{
                scan_subscribers[i] = node_.subscribe<sensor_msgs::LaserScan> (input_topics[i].c_str(), 1, boost::bind(&LaserscanMerger::scanCallback,this, _1, input_topics[i]));
				clouds_modified[i] = false;
				cout << input_topics[i] << " ";
			}
		}
		else
            ROS_INFO("Not subscribed to any topic.");
	}
}

LaserscanMerger::LaserscanMerger()
{
	ros::NodeHandle nh("~");

	nh.getParam("destination_frame", destination_frame);
	nh.getParam("cloud_destination_topic", cloud_destination_topic);
	nh.getParam("scan_destination_topic", scan_destination_topic);
    nh.getParam("laserscan_topics", laserscan_topics);

	angle_min = 1000.0f;
    angle_max = -1000.0f;
    angle_increment = -1.0f;
    scan_time;
    range_min = 1000.0f;
    range_max = -1000.0f;

    this->laserscan_topic_parser();

	point_cloud_publisher_ = node_.advertise<sensor_msgs::PointCloud2> (cloud_destination_topic.c_str(), 1, false);
	laser_scan_publisher_ = node_.advertise<sensor_msgs::LaserScan> (scan_destination_topic.c_str(), 1, false);

	tfListener_.setExtrapolationLimit(ros::Duration(0.1));
}

void LaserscanMerger::scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan, std::string topic)
{
	sensor_msgs::PointCloud tmpCloud1, tmpCloud2;
	sensor_msgs::PointCloud2 tmpCloud3;

    // Verify that TF knows how to transform from the received scan to the destination scan frame
	tfListener_.waitForTransform(scan->header.frame_id.c_str(), destination_frame.c_str(), scan->header.stamp, ros::Duration(1));

	projector_.transformLaserScanToPointCloud(scan->header.frame_id, *scan, tmpCloud1, tfListener_);
	try {
		tfListener_.transformPointCloud(destination_frame.c_str(), tmpCloud1, tmpCloud2);
	}
	catch (tf::TransformException ex) {
		ROS_ERROR("%s",ex.what());
		return;
	}

	for(int i=0; i<input_topics.size(); ++i) {
		if(topic.compare(input_topics[i]) == 0) {
			sensor_msgs::convertPointCloudToPointCloud2(tmpCloud2, tmpCloud3);
			pcl_conversions::toPCL(tmpCloud3, clouds[i]);
			clouds_modified[i] = true;

			angle_min = std::min(angle_min, scan->angle_min);
			angle_max = std::max(angle_max, scan->angle_max);
			angle_increment = std::max(angle_increment, scan->angle_increment);
			scan_time = scan->scan_time;
			range_min = std::min(range_min, scan->range_min);
			range_max = std::max(range_max, scan->range_max);
		}
	}

    // Count how many scans we have
	int totalClouds = 0;
	for(int i=0; i<clouds_modified.size(); ++i)
		if(clouds_modified[i])
			++totalClouds;

	if(totalClouds == clouds_modified.size()) {
		/*for(int i=0; i<clouds_modified.size(); ++i)
			clouds_modified[i] = false;*/
		pointcloud_to_laserscan(clouds);
	}
}

void LaserscanMerger::pointcloud_to_laserscan(vector<pcl::PCLPointCloud2>& clouds)
{
	merged_scan.header.frame_id = destination_frame.c_str();
    merged_scan.header.stamp = ros::Time::now();
    merged_scan.angle_min = angle_min;
    merged_scan.angle_max = angle_max;
    merged_scan.angle_increment = angle_increment;
    merged_scan.scan_time = scan_time;
    merged_scan.range_min = range_min;
    merged_scan.range_max = range_max;

	// set everything to max range
	uint32_t ranges_size = std::ceil((merged_scan.angle_max - merged_scan.angle_min) / merged_scan.angle_increment);
	merged_scan.ranges.assign(ranges_size, merged_scan.range_max + 1.0);

	// for each of the transformed point clouds
	Eigen::MatrixXf points;
	for(int j=0; j<clouds.size(); j++) {
		getPointCloudAsEigen(clouds[j], points);	

		for(int i=0; i<points.cols(); i++)
		{
			const float &x = points(0,i);
			const float &y = points(1,i);

			if ( std::isnan(x) || std::isnan(y) ) {
				continue;
			}

			double range_sq = y*y+x*x;
			double range_min_sq_ = merged_scan.range_min * merged_scan.range_min;
			if (range_sq < range_min_sq_) {
				continue;
			}
			double angle = atan2(y, x);
			if (angle < merged_scan.angle_min || angle > merged_scan.angle_max) {
				continue;
			}
			int index = (angle - merged_scan.angle_min) / merged_scan.angle_increment;

			if (merged_scan.ranges[index] * merged_scan.ranges[index] > range_sq)
				merged_scan.ranges[index] = sqrt(range_sq);
		}
	}
	laser_scan_publisher_.publish(merged_scan);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "laser_multi_merger");

    LaserscanMerger _laser_merger;

	ros::spin();

	return 0;
}
